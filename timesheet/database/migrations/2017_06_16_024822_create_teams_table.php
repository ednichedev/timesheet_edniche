<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->string('title');
            $table->string('desc');
            $table->integer('departments_id')->unsigned()->index();
            $table->integer('leaders_id')->unsigned()->index();
            $table->string('avatar')->default('default.png');
            
            $table->boolean('is_active')->default(false);
            $table->timestamps();
            $table->foreign('departments_id')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('leaders_id')->references('id')->on('users')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
