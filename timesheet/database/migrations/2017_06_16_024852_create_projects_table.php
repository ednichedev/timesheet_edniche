<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('desc');
            $table->integer('teams_id')->unsigned()->index();
            $table->date('start_date');
            $table->date('duedate');
            $table->boolean('completed')->default(false);
            $table->boolean('is_active')->default(false);
            $table->timestamps();

            $table->foreign('teams_id')->references('id')->on('teams')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
