<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_projects', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('desc');
            $table->integer('assignment_id')->unsigned()->index();
            $table->dateTime('start_date');
            $table->time('start_time');
            $table->dateTime('duedate');
            $table->time('end_time');
            $table->boolean('completed')->default(false);
            $table->integer('users_id')->unsigned()->index();
            $table->integer('create_by')->unsigned()->index();
            $table->integer('completed_by')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('assignment_id')->references('id')->on('assignmentprojects')->onDelete('cascade');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('create_by')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('completed_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_projects');
    }
}
