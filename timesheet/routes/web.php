<?php


Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();

Route::post('/singin', 'Auth\AuthLoginController@login')->name('signin');

Route::post('/singin/change_password', 'Auth\AuthLoginController@change_password')->name('change_password');

//Route::resource('/admin', 'Dashboard\AdminController')->name('admin');


    
    Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {

    	Route::get('/', 'Dashboard\AdminController@index')->name('admin.dashboard');

        Route::get('/projectCalender', 'Dashboard\AdminController@projectCalender');


        //Departments
    	Route::get('/department', 'Dashboard\DepartmentController@index');
        Route::post('/department/store', 'Dashboard\DepartmentController@store')->name('department.create');
        Route::get('/department/getdepartment', 'Dashboard\DepartmentController@getdepartment');

        Route::post('/department/block', 'Dashboard\DepartmentController@block');

        Route::post('/department/enable', 'Dashboard\DepartmentController@enable');

        Route::post('/department/update/{id}', 'Dashboard\DepartmentController@update')->name('department.update');

        Route::get('/department/show_depart', 'Dashboard\DepartmentController@show_depart');

        Route::get('/department/search', 'Dashboard\DepartmentController@search');

        //Users
        Route::get('/users', 'Dashboard\UserController@index');
        
        Route::post('/users/store', 'Dashboard\UserController@store')->name('user.create');

        Route::post('/users/block', 'Dashboard\UserController@block')->name('user.block');

        Route::post('/users/enable', 'Dashboard\UserController@enable')->name('user.enable');
        
        Route::post('/users/resetpass/{id}', 'Dashboard\UserController@resetpass')->name('user.resetpass');

        Route::post('/users/account/{id}', 'Dashboard\UserController@account')->name('user.account');

        Route::get('/users/getuser', 'Dashboard\UserController@getUser');

        Route::get('/users/search', 'Dashboard\UserController@search');

        Route::get('/users/show_user', 'Dashboard\UserController@show_user');

        Route::get('/user/edit/{id}', 'Dashboard\UserController@edit')->name('user.edit');

        Route::post('/user/update/{id}', 'Dashboard\UserController@update')->name('user.update');

        //Team route 
        Route::get('/team', 'Dashboard\AdminTeamController@index');

        Route::post('/team/store', 'Dashboard\AdminTeamController@store')->name('team.create');
        
        Route::get('/team/getteam', 'Dashboard\AdminTeamController@getTeams');

        // Route::post('/team/addmember', 'Dashboard\AdminTeamController@addmember')->name('team.addmember');

        Route::get('/team/addmember/{id}', 'Dashboard\AdminTeamController@addmember')->name('team.addmember');
        Route::post('/team/addmemberpost/', 'Dashboard\AdminTeamController@addmemberPost')->name('team.addmember.post');

        Route::post('/team/addLeaderTomember/', 'Dashboard\AdminTeamController@addLeaderTomember')->name('team.addleadermember.post');


        Route::get('/team/removemember/{id}', 'Dashboard\AdminTeamController@removemember')->name('team.removemember');
        Route::post('/team/removememberpost/', 'Dashboard\AdminTeamController@removememberPost')->name('team.removemember.post');

        Route::post('/team/update/{id}', 'Dashboard\AdminTeamController@update');
        
        Route::get('/team/search/', 'Dashboard\AdminTeamController@search');

        Route::post('/team/block/', 'Dashboard\AdminTeamController@block');

        Route::post('/team/enable/', 'Dashboard\AdminTeamController@enable');

        Route::get('/team/show_team/', 'Dashboard\AdminTeamController@show_team');

        //Project Route

        Route::get('/project', 'Dashboard\AdminProjectController@index');

         Route::post('/project/store', 'Dashboard\AdminProjectController@store')->name('project.create');

        Route::get('/project/getproject', 'Dashboard\AdminProjectController@getProject');

        Route::post('/project/completed/project/{id}', 'Dashboard\AdminProjectController@projectComplete');

        Route::post('/project/uncompleted/project/{id}', 'Dashboard\AdminProjectController@projectunComplete');

        Route::get('/project/addmember/{id}/', 'Dashboard\AdminProjectController@addmember')->name('project.addmember');
        Route::post('/project/addmemberpost/', 'Dashboard\AdminProjectController@addmemberPost')->name('project.addmember.post');

        Route::get('/project/removemember/{id}/', 'Dashboard\AdminProjectController@removemember')->name('project.removemember');
        Route::post('/project/removememberpost/', 'Dashboard\AdminProjectController@removememberPost')->name('project.removemember.post');

        Route::get('/project/addassigntment/{id}/', 'Dashboard\AdminProjectController@addAssignment')->name('project.addassigntment');

        Route::post('/project/addassigntment/', 'Dashboard\AdminProjectController@addAssignmentPost')->name('project.addassigntment.post');

        Route::post('/project/assigntment/task/create', 'Dashboard\AdminProjectController@addTaskAssignmentPost')->name('admin.project.assignment.task.create');

        // Project Note

        Route::post('/project/assigntment/task/note/create', 'Dashboard\AdminProjectController@addNote')->name('admin.project.assignment.task.note.create');

        // Project Note

        //Route::get('/project/getassign/', 'Dashboard\AdminProjectController@getassign');

         Route::get('/project/getassignmentproject/{id}', 'Dashboard\AdminProjectController@getAssignment')->name('admin.get.assignmentproject');


         Route::get('/project/gettaskproject/{id}', 'Dashboard\AdminProjectController@getTask')->name('admin.get.taskproject');

         Route::post('/project/completed/{id}', 'Dashboard\AdminProjectController@assignComplete');

         Route::post('/project/uncompleted/{id}', 'Dashboard\AdminProjectController@assignunComplete');

        Route::post('/project/taskproject/completed/{id}', 'Dashboard\AdminProjectController@taskComplete');

        Route::post('/project/taskproject/uncompleted/{id}', 'Dashboard\AdminProjectController@taskunComplete');

        

        Route::post('/project/update/{id}', 'Dashboard\AdminProjectController@update');

        Route::post('/project/block/', 'Dashboard\AdminProjectController@block');

        Route::post('/project/enable/', 'Dashboard\AdminProjectController@enable');

        Route::get('/project/search/', 'Dashboard\AdminProjectController@search');

        Route::get('/project/show_project/', 'Dashboard\AdminProjectController@show_project');

        Route::post('/project/removeassignPost/', 'Dashboard\AdminProjectController@removeassignPost')->name('project.remove.post');

        //Assignment

        Route::get('/assignment', 'Dashboard\AdminAssignmentController@index');

        Route::post('/assignment/store/', 'Dashboard\AdminAssignmentController@store')->name('assignment.create');

        Route::get('/assignment/getassignment', 'Dashboard\AdminAssignmentController@getassignment');

        Route::post('/assignment/remove', 'Dashboard\AdminAssignmentController@remove')->name('assignmentsingle.remove.post');

        Route::get('/assignment/search', 'Dashboard\AdminAssignmentController@search');

        Route::get('/assignment/show_assignmentsingle', 'Dashboard\AdminAssignmentController@show_assignmentsingle');


        Route::post('/assignment/completed/{id}', 'Dashboard\AdminAssignmentController@assignComplete');

         Route::post('/assignment/uncompleted/{id}', 'Dashboard\AdminAssignmentController@assignunComplete');


        Route::get('/assignmentsingle/gettask/{id}', 'Dashboard\AdminAssignmentController@getTask')->name('assignmentsingle.tasksingle');

        Route::post('/assignmentsingle/tasks/create', 'Dashboard\AdminAssignmentController@addTaskAssignmentPost')->name('assignmentsingle.tasksingle.create');

        Route::post('/assignmentsingle/tasks/completed/{id}', 'Dashboard\AdminAssignmentController@taskComplete');

         Route::post('/assignmentsingle/tasks/uncompleted/{id}', 'Dashboard\AdminAssignmentController@taskunComplete');

         //Note 

          Route::post('/assignmentsingle/task/note/create', 'Dashboard\AdminAssignmentController@addNote')->name('admin.assignmentsingle.task.note.create');



        //Profile

        Route::get('/profile/{id}', 'Dashboard\AdminController@Profile')->name('admin.profile');

        Route::post('/profile/post/{id}', 'Dashboard\AdminController@ProfilePost')->name('admin.profile.post');

         Route::post('/profile/change_password/{id}', 'Dashboard\AdminController@ChangePassword')->name('admin.profile.changepassword');


        //Reporting

        Route::get('/report', 'Dashboard\AdminReportController@index');

        Route::post('/report/projectreport', 'Dashboard\AdminReportController@projectReport')->name('projectreport');





	});

Route::group(['prefix' => 'teamleader', 'middleware' => ['auth', 'teamleader']], function () {

    	Route::get('/', 'Dashboard\TeamController@index')->name('team.dashboard');

        Route::get('/team', 'Dashboard\TeamTeamController@team');

         Route::get('/project', 'Dashboard\TeamTeamController@project');

         Route::post('/project/addAssignment', 'Dashboard\TeamTeamController@store')->name('projec.addassignment');

        Route::post('/project/addassigntment/task', 'Dashboard\TeamTeamController@addTaskAssignmentPost')->name('project.assignmentproject.task.create');

        Route::post('/project/deleteassign', 'Dashboard\TeamTeamController@deleteassign')->name('projec.deleteassign');

        Route::post('/project/completed/{id}', 'Dashboard\TeamTeamController@assignComplete');

         Route::post('/project/uncompleted/{id}', 'Dashboard\TeamTeamController@assignunComplete');

         Route::get('/project/get_assignment/{id}', 'Dashboard\TeamTeamController@getAssignment')->name('project.get.assignments');

         Route::get('/project/get_taskproject/{id}', 'Dashboard\TeamTeamController@getTask')->name('assignmentproject.get.taskproject');

        Route::post('/project/assignmentproject/tasks/completed/{id}', 'Dashboard\TeamTeamController@taskComplete');

        Route::post('/project/assignmentproject/tasks/uncompleted/{id}', 'Dashboard\TeamTeamController@tasknunComplete');

        Route::post('/project/assignmentproject/tasks/note', 'Dashboard\TeamTeamController@addNote')->name('teamleader.project.task.note.create');




         //My Assignments

         Route::get('/myassignment', 'Dashboard\TeamMyassignmentController@index');

        Route::post('/myassignment/new', 'Dashboard\TeamMyassignmentController@store')->name('myassignment.add');

        Route::post('/myassignment/deleteassign', 'Dashboard\TeamMyassignmentController@deleteassign')->name('myassignment.deleteassign');

        Route::post('/myassignment/completed/{id}', 'Dashboard\TeamMyassignmentController@assignComplete');

        Route::post('/myassignment/uncompleted/{id}', 'Dashboard\TeamMyassignmentController@assignunComplete');

        Route::post('/myassignment/addtask', 'Dashboard\TeamMyassignmentController@addTaskAssignmentPost')->name('myassignment.task.create');

        Route::get('/myassignment/gettask/{id}', 'Dashboard\TeamMyassignmentController@getTask')->name('myassignment.tasksingle');

        Route::post('/myassignment/task/completed/{id}', 'Dashboard\TeamMyassignmentController@taskComplete');

         Route::post('/myassignment/task/uncompleted/{id}', 'Dashboard\TeamMyassignmentController@tasknunComplete');

         Route::post('/myassignment/task/note', 'Dashboard\TeamMyassignmentController@addNote')->name('teamleader.myassignment.task.create');

         //Profile

        Route::get('/profile/{id}', 'Dashboard\TeamController@Profile')->name('team.profile');

        Route::post('/profile/post/{id}', 'Dashboard\TeamController@ProfilePost')->name('team.profile.post');

         Route::post('/profile/change_password/{id}', 'Dashboard\TeamController@ChangePassword')->name('team.profile.changepassword');

        
	});


Route::group(['prefix' => 'employee', 'middleware' => ['auth', 'employee']], function () {

    	Route::get('/', 'Dashboard\EmployeeController@index')->name('employee.dashboard');

        Route::get('/team', 'Dashboard\EmployeeTeamController@index');

        Route::get('/project', 'Dashboard\EmployeeProjectController@index');

        Route::get('/project/get_assignment/{id}', 'Dashboard\EmployeeProjectController@getAssignment')->name('project.get.assignmentsproject');

        Route::post('/project/addAssignment', 'Dashboard\EmployeeProjectController@store')->name('projec.employee.addassignment');

        Route::post('/project/deleteassign', 'Dashboard\EmployeeProjectController@deleteassign')->name('projec.employee.deleteassign');

        Route::post('/projec/assignment/addtask', 'Dashboard\EmployeeProjectController@addTaskAssignmentPost')->name('project.assignment.task.create');

        Route::get('/project/gettaskproject/{id}', 'Dashboard\EmployeeProjectController@getTask')->name('assignments.get.taskproject');

        Route::post('/assignment/task/completed/{id}', 'Dashboard\EmployeeProjectController@taskComplete');

        Route::post('/assignment/task/uncompleted/{id}', 'Dashboard\EmployeeProjectController@tasknunComplete');

        //Note
        Route::post('/assignment/task/note/create', 'Dashboard\EmployeeProjectController@addNote')->name('employee.project.task.note.create');


        //My assignments

        Route::get('/myassignment_employee', 'Dashboard\EmployeeMyassignmentController@index');

        Route::post('/myassignment_employee/new', 'Dashboard\EmployeeMyassignmentController@store')->name('myassignment_employee.add');

        Route::post('/myassignment_employee/deleteassign', 'Dashboard\EmployeeMyassignmentController@deleteassign')->name('myassignment_employee.deleteassign');

        Route::post('/myassignment_employee/addtask', 'Dashboard\EmployeeMyassignmentController@addTaskAssignmentPost')->name('myassignment_employee.task.create');

         Route::get('/myassignment_employee/gettask/{id}', 'Dashboard\EmployeeMyassignmentController@getTask')->name('myassignment_employee.tasksingle');

         Route::post('/myassignment_employee/task/delete', 'Dashboard\EmployeeMyassignmentController@deleteTask')->name('myassignment_employee.deletetask');

         Route::post('/myassignment_employee/task/completed/{id}', 'Dashboard\EmployeeMyassignmentController@taskComplete');

         Route::post('/myassignment_employee/task/uncompleted/{id}', 'Dashboard\EmployeeMyassignmentController@tasknunComplete');

         //Notes 
         Route::post('/myassignment_employee/assignmentsingle/task/note', 'Dashboard\EmployeeMyassignmentController@addNote')->name('employee.assignmentsingle.note');

         //Profile

        Route::get('/profile/{id}', 'Dashboard\EmployeeController@Profile')->name('employee.profile');

        Route::post('/profile/post/{id}', 'Dashboard\EmployeeController@ProfilePost')->name('employee.profile.post');

         Route::post('/profile/change_password/{id}', 'Dashboard\EmployeeController@ChangePassword')->name('employee.profile.changepassword');



        
});





// Route::get('/home', 'HomeController@index');
