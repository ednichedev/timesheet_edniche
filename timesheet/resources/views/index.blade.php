@extends('layouts.home-app')

   @section('content')

	<!-- Navbar -->
	<nav class="navbar navbar-toggleable-md bg-primary fixed-top navbar-transparent " color-on-scroll="500">
		<div class="container">
			<div class="navbar-translate">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar bar1"></span>
					<span class="navbar-toggler-bar bar2"></span>
					<span class="navbar-toggler-bar bar3"></span>
				</button>
				<a class="navbar-brand" href="http://demos.creative-tim.com/now-ui-kit/index.html" rel="tooltip" data-placement="bottom" target="_blank">
					Edniche English School
				</a>
			</div>
			<div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
				<ul class="navbar-nav">
					<li class="nav-item">
						
					</li>
					<li class="nav-item">
						
					</li>
					<li class="nav-item">
						<a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
							<i class="fa fa-twitter"></i>
							<p class="hidden-lg-up">Twitter</p>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
							<i class="fa fa-facebook-square"></i>
							<p class="hidden-lg-up">Facebook</p>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
							<i class="fa fa-instagram"></i>
							<p class="hidden-lg-up">Instagram</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- End Navbar -->

	<div class="wrapper">
		<div class="page-header clear-filter" filter-color="orange">
			<div class="page-header-image" style="background-image: url('/assets/img/img-header.jpg');">
			</div>
			<div class="container">
				
				<div class="content-center brand" id="brand">
				<div class="hidden-sm">
					<img src="/assets/img/logoedniche.png" class="" alt="" style="width: 350px; margin-bottom: 45px;">
					</div>
					<h1 class="h1-seo">Timesheet  flow</h1>
					

					<button class="btn btn-primary btn-simple btn-round" type="button" style="font-size: 16px; color: #fff; border-color: #FFF;" id="btnShowLogin">Start Now</button>

				</div>

				<div class="content-center" id="showLogin" style="display: none;">
				<div class="card card-login card-plain col-md-4 ">
					<form class="form" method="" action="">
						<div class="header header-primary text-center">
							
						</div>
						<div class="content">
							<div class="input-group form-group-no-border input-lg">
								<span class="input-group-addon">
									<i class="now-ui-icons users_circle-08" style="color: #FFF;"></i>
								</span>
								<input type="email" class="form-control" placeholder="Email...">
							</div>
							<div class="input-group form-group-no-border input-lg">
								<span class="input-group-addon">
									<i class="now-ui-icons ui-1_lock-circle-open" style="color: #FFF;"></i>
								</span>
								<input type="password" placeholder="Password..." class="form-control">
							</div>
						</div>
						<div class="footer text-center">
							<button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Login</button>
						</div>

					   
					   <button class="btn btn-primary btn-icon  btn-icon-mini btn-round btn-simple" type="button" style="font-size: 14px; color: #fff; border-color: #FFF;" id="hideLogin">
						<i class="now-ui-icons arrows-1_minimal-down"></i>
					  </button>
						
					 
					</form>
				</div>
			</div>
			
				
				
			</div>
				
		</div>

		<footer class="footer" style=" margin-top: -70px;">
			<div class="container">
				

				<div class="copyright" style="color: #FFF; text-align: left;">
					&copy;
					<script>
						document.write(new Date().getFullYear())
					</script>
					| 
				Power by Edniche English School
				</div>

			</div>
		</footer>


	</div>


@stop


