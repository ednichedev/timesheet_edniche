@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li>
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
								</li>
							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h5 class="f18"> 
						Calender For Project
						</h5>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						
						<!-- BEGIN DASHBOARD STATS 1-->
						<div class="row">
								<div class="portlet light">

									<div id='calendar'></div>

								</div>
						</div>
						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="row">

						</div>
						
						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

@stop

@section('javascript')
			
			<script>

	$(document).ready(function() {

		<?php 

		$now = new DateTime();
		$current = $now->format('Y-m-d');

		?>

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay,listWeek,listDay'
			},

			views: {
				listDay: { buttonText: 'list day' },
				listWeek: { buttonText: 'list week' }
			},

			defaultDate: '<?php echo $current; ?>',
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
			<?php $i=1; ?>
			<?php foreach ($data as $datas) {?>
				{
					title: '<?php echo $datas->title ?>',
					start: '<?php echo $datas->start_date ?>',
					end: '<?php echo $datas->duedate ?>',			
					color: '<?php echo 'rgba('.rand(0,255).', '.rand(0,255).', '.rand(0,255).', 0.73)'; ?>',
				},
				<?php }?>
				],

		});
		
	});

</script>
	
	@parent
@stop