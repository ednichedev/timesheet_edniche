@extends('layouts.main-app')

   @section('content')


					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Users</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
						<div class="row">
											
							<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">
														
								
								<h2><i class="im-list2 s16" style="margin-top: 15px; color: #B0BEC5;"></i>&nbsp;<small style="color: #B0BEC5;">Lists</small>
								<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 8px;"></i></button>
										

											<p style="margin-top: -25px;">&nbsp;</p>

													

													<div class="input-icon input-small ico">

														<i class="icon-magnifier" id="search"></i>
														<i class="icon-magnifier hidden" id="search-1"></i>
														<input type="text" class="form-control search f12" name="search_users" id="search_users" placeholder="Search..." autofocus="true">

												
												</div>

														<select  class="form-control show-user" name="show_user" id="show_user">
															<option value="5">5</option>
															<option value="10">10</option>
															<option value="30">30</option>
															<option value="100">All</option>
														</select>


													<div class="portlet-body">
														<div class="box-scroll">											
																		<div class="table-display">
																											
																		</div>
														</div>

													</div>
									</div>

		@if(Auth::user()->role == 1)

			<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
						<div class="portlet-body">
																											
							<!-- 	Create New User -->
							<div class="show_form_create">

								<h2 style="color: #B0BEC5; margin-top: -10px;">&nbsp;<small style="color: #B0BEC5;">Create</small></h2>
								<p style="margin-top: -27px;">&nbsp;</p>
								
								<div class="panel-body">
									<form class="form form-horizontal" role="form" id="user_form" method="POST" action="{{  route('user.create') }}">

									{{ csrf_field() }}

							
										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
												<input type="text" class="form-control col-md-12 f12" placeholder="Full Name..." name="name" value="{{ old('name') }}" required autofocus id="name">
												@if ($errors->has('name'))
													<span class="help-block">
														<strong>{{ $errors->first('name') }}</strong>
													</span>
												@endif
											</div>
										</div>
								
										<!-- End .form-group  -->
										
										<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
												<input type="email" class="form-control col-md-12 f12" placeholder="Email Address..." name="email" value="{{ old('email') }}" required id="email">
														@if ($errors->has('email'))
															<span class="help-block">
																<strong>{{ $errors->first('email') }}</strong>
															</span>
														@endif

											</div>
										</div>

										<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
												<input type="password" class="form-control col-md-12 f12" placeholder="Password..." name="password" required id="password">

														@if ($errors->has('password'))
															<span class="help-block">
																<strong>{{ $errors->first('password') }}</strong>
															</span>
														@endif

											</div>
										</div>

										<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
												<input id="password-confirm" type="password" class="form-control col-md-12 f12" name="password_confirmation" placeholder="Confirm Password..." required id="confirm_password">
											</div>
										</div>

										
										<!-- End .form-group  -->


										<div class="form-group">
						
																<div class="col-md-12">

																	<span style="color: #999;" class="mt-radio-inline f12">Roles: </span>

																		<div class="mt-radio-inline">

																		<label class="mt-radio mt-radio-outline f12" style="color: #999;">
																					<input type="radio" name="role" value="1" id="optionsRadios1" value="1"> Admin
																					<span></span>
																				</label>

																				<label class="mt-radio mt-radio-outline f12" style="color: #999;">
																					<input type="radio" name="role" value="1" id="optionsRadios1" value="2"> Manager
																					<span></span>
																				</label>
																				<label class="mt-radio mt-radio-outline f12" style="color: #999;">
																					<input type="radio" name="role" value="3" id="optionsRadios2"> Team Leader
																					<span></span>
																				</label>

																				<label class="mt-radio mt-radio-outline f12" style="color: #999;">
																					<input type="radio" name="role" value="4" id="optionsRadios3"> Member
																					<span></span>
																				</label>
																				
																			</div>

																	</div>


										</div>

										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="submit">Save</button>
													&nbsp;
												<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>
								</div>
									
							</div>
							
						<!-- 	Reset Password -->
							<div class="show_form_pass hidden ">
								<h2 style="margin-top: -10px;">&nbsp;<small style="color: #B0BEC5;">Reset Password | </small><small id="user_avatar" class="hidden"></small>&nbsp; <small id="user_name" class="hidden s14"></small>
								</h2> 

									<p style="margin-top: -27px;">&nbsp;</p>


								<div class="panel-body">
									<form class="form-horizontal group-border hover-stripped form-style-4" role="form" id="user_resetpass" method="POST" action="">
									{{ csrf_field() }}

								
										<!-- End .form-group  -->

										<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
												<input type="password" class="col-md-12 form-control f12" placeholder="New Password..." name="password" required id="reset-password" autofocus>

														@if ($errors->has('password'))
															<span class="help-block">
																<strong>{{ $errors->first('password') }}</strong>
															</span>
														@endif

											</div>
										</div>

										<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
												<input id="password-confirm" type="password" class="col-md-12 form-control f12" name="password_confirmation" placeholder="Confirm Password..." required id="confirm_password">
											</div>
										</div>

										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
											<button type="button" class="btn btn-icon-only default" id="btn-back-1" style="display: none;"><i class="fa fa-angle-left f16"></i></button>
												<button class="btn blue f12" id="btn-reset">Reset Password</button>
												&nbsp;
												<button class="btn btn-default f12" type="reset" id="back-user">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


								</div>
									
							</div>

						</div>
				</div>

			@endif


			</div>


						
							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right" role="alert" id="success">
								  <i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>


							
							<div class="message success col-lg-4 pull-right" role="alert" id="resetpass">
								  <i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="error">
								  <i class="fa fa-warning"></i> &nbsp; Please fill all field   
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="resetpass_notmacth">
								  <i class="fa fa-warning"></i> &nbsp; Password not match and Password more than 6 character... !   
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="error_email">
								  <i class="fa fa-warning"></i> &nbsp; 
								  Invalid Email Address

								</div>
								


						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>
				

					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

		<!-- End #content -->



@stop

