

												<table class="table dataTable no-footer">
														<tbody>
															
															@foreach($users as $user)
															<tr id="showtable">
																
																<td><img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $user->avatar }}" style="max-width: 30px; height: 30px; border-radius: 100px;"></td>
																<td>{{ $user->name }}</td>
																<td>{{ $user->email }}</td>
																<td>
																	@if($user->role==1)
																			Admin
																			@elseif($user->role==2)
																			Manager
																			@elseif($user->role==3)
																				Team Leader
																				@elseif($user->role==4)
																					
																					Member
																
																@endif
																</td>
													
																<td>
																@if($user->is_active == 0)
																<span class="label label-danger s12" id="label">Blocked</span></td>
																@else
																		
																@endif

															@if(Auth::user()->role == 1)
																<td>
																
																	
																		<div class="dropdown dropdown-user">
																				<button class="btn btn-link dropdown-toggle pull-right" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class=" icon-settings"></i>
																				</button>

																			<ul class="dropdown-menu dropdown-menu-default pull-right"  id="dropdown-menu">

																			<li class="f12">
																				<a href="{{route('user.edit', $user->id)}}" class="resetpass">
																				<i class="icon-wrench f12"></i> &nbsp; Edit</a>
																											

																				</li>

																			
																				<li class="f12">
																				<a href="#" class="resetpass" data-value="{{ $user->id }}" data-name="{{ $user->name}}" data-emails="{{ $user->email}}" data-image="/dashboard/assets/img/avatars/{{ $user->avatar }}">
																				<i class="icon-key f12"></i> &nbsp; Reset Password</a>
																											

																				</li>

																				<li class="f12">
																					@if($user->is_active == 1)
																					<a href="#" class="block-btn" data-value="{{ $user->id }}"><i class="icon-power"></i> &nbsp; Block</a>
																					@else
																						<a href="#" data-value="{{ $user->id }}" class="btn-enable"><i class="icon-like"></i> &nbsp;Enable</a>
																					@endif
																				</li>
																			</ul>
																		</div>

																</td>

																	@endif


															</tr>
																											
														@endforeach
														</tbody>


												</table>






											

						