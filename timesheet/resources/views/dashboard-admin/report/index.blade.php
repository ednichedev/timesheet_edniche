@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li>
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
								</li>
							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h5 class="f18"> 
						Reports
						</h5>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						
						<!-- BEGIN DASHBOARD STATS 1-->
						<div class="row">
								<div class="portlet light">

<!-- 									<div class="col-md-12">
											<div class="panel panel-default">
												  <div class="panel-body">
														<form class="form-inline" action="{{route('projectreport')}}" method="POST">
														{{ csrf_field() }}
																	  <div class="form-group">
																		<div class="col-md-6">
																							<div class="input-icon">
																							<i class="fa fa-calendar"></i>
																							<input  type="text"  class="form-control" placeholder="Start Date..."  id="example1" name="start_date">
																							</div>
																							</div>
																				</div>
																	  <div class="form-group">
																		<div class="col-md-6">
																						<div class="input-icon">
																						<i class="fa fa-calendar"></i>
																							<input  type="text" class="form-control" placeholder="End Date..."  id="example2" name="duedate">
																							</div>
																					</div>
																	  </div>
																			
																			&nbsp;
																				&nbsp;

																	  <div class="form-group">
																		<div class="col-md-6">
																						<select class="form-control select2" name="complete">
																							<option value="1">Complete</option>
																							<option value="0">Processe</option>
																					</select>
																					</div>
																	  </div>

																				&nbsp;
																				&nbsp;
																		 <div class="form-group">
																		  <button type="submit" class="btn btn-default">Search</button>
																		  </div>

																		  &nbsp;
																				&nbsp;

																		  <div class="form-group">
																							<span class="f14 color-light">Report for Project sort by date</span>
																		  </div>


																	</form>
												  </div>
												</div>
									</div> -->


									<div class="col-md-12">
												
												<div class="panel panel-default">
													<div class="f16 color-light" style="margin-top: 10px; margin-left: 20px;">Project completed and need to complete to day </div>
													<div class="panel-body">
														<table class="table">

															<tbody>
															@if(count($projects))

																@foreach($projects as $project)
																<tr>
																	<td>{{ $project->title }}</td>
																	<td>{{$project->teams->title }}
																					|
																				<img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $project->teams->leaders->avatar }}" style="max-width: 15px; border-radius: 100px;">
																				
																				<span class="f11" style="color: #95a5a6;">{{ $project->teams->leaders->name }}
																				</span>


																	</td>

																	<td>
																		{{ Carbon\Carbon::parse($project->start_date)->format('d-M-Y') }}
																	</td>

																	<td>
																		{{ Carbon\Carbon::parse($project->duedate)->format('d-M-Y') }}
																	</td>

																	<td>
																		@if($project->completed == 0)

																			@else
																		<span class="badge" style="background-color: #2ecc71;">
																			 Completed
																			 </span>
																			 	@endif
																	</td>

																

																</tr>
																@endforeach
																@endif
															</tbody>
															
														</table>

													  </div>
													</div>

									</div>

								</div>
						</div>
						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="row">

						</div>
						
						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

@stop

