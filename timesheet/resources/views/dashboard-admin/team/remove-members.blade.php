@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Teams</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">
											
		<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">
														
								
								<h2><i class="im-list2 s16" style="margin-top: 12px; color: #B0BEC5;"></i>&nbsp;<small style="color: #B0BEC5;">Lists</small>
								<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 8px;"></i></button>
										

											<p style="margin-top: -25px;">&nbsp;</p>

													

													<div class="input-icon input-small ico">

														<i class="icon-magnifier" id="search" class=""></i>
														<i class="icon-magnifier" id="search-1" class="hidden"></i>
														<input type="text" class="form-control search f12" name="search_depart" id="search_depart" placeholder="Search..." autofocus="true">

												
												</div>

														<select  class="form-control show-user" name="show_depart" id="show_depart">
															<option value="5">5</option>
															<option value="10">10</option>
															<option value="30">30</option>
															<option value="100">All</option>
														</select>


													<div class="portlet-body">
														<div class="box-scroll">											
																		<div class="table-team-display">
																											
																		</div>
														</div>

													</div>
									</div>

	<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
	<div class="portlet-body">


							<!-- Add member --> 
							<div class="add_member_form">
								<h2>&nbsp;<small style="color: #B0BEC5;">Remove Members | 	
								@foreach($teams as $team)
								<small class="team_avatar">
								<img src="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}" style='max-width: 30px; border-radius:50px;'>
								</small>&nbsp;
								<small class="f18" style="color: #B0BEC5;">
								{{ $team->title }}
								</small>
								&nbsp; &nbsp;
									@endforeach

								<small class="team_avatar">
								</small>&nbsp;
								<small id="team_name" class="f18" style="color: #B0BEC5;">
								</small>
								&nbsp; &nbsp;
								
								</h2> 
										<table class="table">
										<thead>
											<tr>
												<th style="font-weight: normal;">Leader</th>
												<th></th>
												<th></th>
											</tr>
										</thead>

											<tbody>
											@foreach($teams as $team)
													<tr>
														<td width="5%">
														<img src="/dashboard/assets/img/avatars/{{ $team->leaders->avatar }}" style='max-width: 30px; border-radius:50px;'>
														</td>

														<td>	{{ $team->leaders->name }}</small></td>
														
														<td> <a href="#" class="pull-right f14 font-red-intense">Cancel</a></td>
													</tr>
													@endforeach
													<tr><td colspan="4"></td></tr>
											</tbody>
										</table>

								<p style="margin-top: -27px;">&nbsp;</p>

								<div class="panel-body">
									
									<table class="table">
										<thead>
												<tr>
														<th colspan="3">Members</th>
													</tr>
										</thead>
										<tbody>

											@foreach( $team->members as $member)
											<tr>
												<td width="10%">
												<img src="/dashboard/assets/img/avatars/{{ $member->avatar }}" style='max-width: 30px; border-radius:50px;'></td>
												<td>{{ $member->name }}</td>

												<td width="10%">
														<form method="POST" action="{{ route('team.removemember.post') }}">
														{{ csrf_field() }}
																<input type="hidden" name="team_id" value="{{ $team->id }}">
																<input type="hidden" name="members" value="{{ $member->id }}">

																<button class="btn btn-link"><i class="icon-close font-red-intense"></i></button>
															
														</form>

												</td>

											</tr>
											@endforeach
										</tbody>

									</table>


									<form class="form form-horizontal" role="form" method="POST" action="">

									{{ csrf_field() }}

							
										<div class="form-group">

											@foreach($teams as $team)
												<input type="hidden" name="team_id" value="{{ $team->id }}">
												@endforeach

										</div>

										<!-- End .form-group  -->
									</form>


				</div>




</div>

	</div>


</div>

						
							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right" role="alert" id="success">
								  <i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>


							
							<div class="message success col-lg-4 pull-right" role="alert" id="resetpass">
								  <i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="error">
								  <i class="fa fa-warning f16"></i> &nbsp; Please fill all field   
								</div>
								



						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>


						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

		<!-- End #content -->



@stop