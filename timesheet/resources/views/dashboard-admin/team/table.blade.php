
												<table class="table dataTable no-footer">
														<tbody>
															
															@foreach($teams as $team)
															<tr id="showtable">
																
																<td>
																<img class="user-avatar" src="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}" style="max-width: 30px; border-radius:50px;"></td>

																<td>
																	{{ $team->title }}<br>
																	<span class="f12 color-light">{{ $team->departments->title }}</span>
																</td>
																
																<td>
																<img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $team->leaders->avatar }}" style="max-width: 30px; border-radius:50px;"></td>
																<td>
																{{ $team->leaders->name }}
																<br>
																	<span class="f12 color-light">Members ( {{ $team->members->count() }} ) 
																	<a href="{{ route('team.addmember', $team->id) }}" class="btn btn-sm btn-link"><i class="fa fa-plus"></i> </a>
																	<!-- <button class="btn btn-sm btn-link btn-add-member" data-value="{{$team->id}}" data-image="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}" data-name="{{$team->title}}" data-leader-name="{{ $team->leaders->name }}" data-leader-image="/dashboard/assets/img/avatars/{{ $team->leaders->avatar }}" data-member="{{ $team->members->count() }}"><i class="fa fa-plus"></i></button> --></span>
																</td>
																<!-- <td>{{ $team->leaders->name }}</td> -->
																<td>
																@if($team->is_active == 1)
																<span class="label label-danger">Blocked</span>
																@else	

																@endif
																</td>

																	<td>
																		<div class="dropdown dropdown-user">
																				<button class="btn btn-link dropdown-toggle pull-right" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class=" icon-settings"></i>
																				</button>

																			<ul class="dropdown-menu dropdown-menu-default pull-right"  id="dropdown-menu">
																			
																				<li class="f12">
																				
																				<a href="#" class="edit" data-value="{{ $team->id }}" data-name="{{ $team->title }}" data-desc="{{ $team->desc }}" data-team-image="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}" data-leader="{{ $team->leaders->name }}" data-id="{{$team->leaders->id}}" data-image="/dashboard/assets/img/avatars/{{ $team->leaders->avatar }}"><i class="icon-pencil f12"></i> &nbsp; Edit</a>
																				</li>

																				<li class="f12">
																				@if($team->is_active == 0)
																					<a href="#" class="btn-block-team" data-value="{{ $team->id }}"><i class="icon-power"></i> &nbsp; Block
																					</a>
																					@else
																					<a href="#" class="btn-enable-team" data-value="{{ $team->id }}">
																					<i class="icon-like"></i> &nbsp;Enable
																					</a>
																					@endif
																				</li>
																			</ul>
																		</div>

																</td>

														

															</tr>
																											
														@endforeach
														</tbody>


												</table>






											

						