@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Teams</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">
											
		<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">
														
								
								<h2><small style="color: #B0BEC5;">Lists</small>
								<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 10px;"></i></button>
										
											<p style="margin-top: -25px;">&nbsp;</p>

													<div class="input-icon input-small ico">
													
														<i class="icon-magnifier" id="search"></i>
														<i class="icon-magnifier hidden" id="search-1"></i>
														<input type="text" class="form-control search f12" name="search_team" id="search_team" placeholder="Search..." autofocus="true">

												
												</div>

														<select  class="form-control show-user" name="show_team" id="show_team">
															<option value="5">5</option>
															<option value="10">10</option>
															<option value="30">30</option>
															<option value="100">All</option>
														</select>


													<div class="portlet-body">
														<div class="box-scroll">											
																		<div class="table-team-display">
																											
																		</div>
														</div>

													</div>

									</div>



	<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
	<div class="portlet-body">

							<!-- 	Create New User -->
							<div class="team_form">

								<h2 style="color: #B0BEC5; margin-top: -10px;">&nbsp;<small style="color: #B0BEC5;">Create</small></h2>
								<p style="margin-top: -27px;">&nbsp;</p>
								
								<div class="panel-body">

								<form class="form form-horizontal" role="form" id="team_form" method="POST" action="{{  route('team.create') }}">

									{{ csrf_field() }}

							
										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
												<input type="text" class="form-control col-md-12 f12" placeholder="Title..." name="title" value="{{ old('title') }}" required autofocus id="title">
												@if ($errors->has('title'))
													<span class="help-block">
														<strong>{{ $errors->first('title') }}</strong>
													</span>
												@endif
											</div>
										</div>
								
										<!-- End .form-group  -->
										
										<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
											<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..." id="desc"></textarea>
											</div>
										</div>
	
										<div class="form-group">
											
											<div class="col-lg-12">

													<select class="ui dropdown col-lg-12 f12" name="departments_id">
													  <option value="">Select Department</option>

														@foreach($departments as $department)
													  <option value="{{ $department->id }}">{{ $department->title }}</option>

														@endforeach
													 
													</select>

												
													  </div>
													</div>

										<div class="form-group">
											
											<div class="col-lg-12">

												<div class="ui fluid selection dropdown f12">
													  <input type="hidden" name="leaders_id">
													  <i class="dropdown icon"></i>
													  <div class="default text">Select Leader</div>
													  <div class="menu">
														@foreach($users as $user)
														
														<div class="item" data-value="{{ $user->id }}">
														  <img class="ui mini avatar image" src="/dashboard/assets/img/avatars/{{ $user->avatar }}">
														  {{ $user->name }}
														</div>
															@endforeach
													  </div>
													</div>

												
													  </div>
													</div>


								
											
										</div>

										
										<!-- End .form-group  -->

										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="team_submit">Save</button>
													&nbsp;&nbsp;
												<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>
								</div>
							</div>

									

							<!-- 	Edit form -->
								<div class="team_form_edit hidden">
								<h2>&nbsp;<small style="color: #B0BEC5;">Edit | <small id="team_avatar"></small>
								<small id="team_name" class="hidden f16" style="color: #B0BEC5;"></small>
								</h2> 
									<p style="margin-top: -27px;">&nbsp;</p>
								<div class="panel-body">

									<form class="form form-horizontal" role="form" id="team_form_edit" method="POST" action="">

									{{ csrf_field() }}

							
										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											
											<div class="col-lg-12 text-title">
												
												@if ($errors->has('title'))
													<span class="help-block">
														<strong>{{ $errors->first('title') }}</strong>
													</span>
												@endif
											</div>
										</div>
								
										<!-- End .form-group  -->
										
						<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12 text-desc">
											<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..."></textarea>
											</div>
										</div>
	
										<div class="form-group{{ $errors->has('manager_id') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
										
													

												<div class="leaders_id">
														

												</div>

													<div class="ui fluid selection dropdown hidden">
													  <input type="hidden" name="" class="att_name">
													  <i class="dropdown icon"></i>
													  <div class="default text">Select Manager</div>
													  <div class="menu">
													@foreach($users as $user)
														
														<div class="item" data-value="{{ $user->id }}">
														  <img class="ui mini avatar image" src="/dashboard/assets/img/avatars/{{ $user->avatar }}">
														  {{ $user->name }}
														</div>
															@endforeach
													  </div>
													</div>



											</div>
										</div>

										
										<!-- End .form-group  -->

										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="team_update">Update</button>
													&nbsp;&nbsp;
												<a href="#" class="btn btn-default f12" id="back">Cancel</a>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


					</div>

				</div>

							<!-- Add member --> 
							<div class="add_member_form hidden">
								<h2>&nbsp;<small style="color: #B0BEC5;">Add Members | &nbsp;

								<small class="team_avatar">
								</small>&nbsp;
								<small id="team_name" class="f18" style="color: #B0BEC5;">
								</small>
								&nbsp; &nbsp;
								
								</h2> 
										<table class="table">
										<thead>
											<tr>
												<th style="font-weight: normal;">Leader</th>
												<th></th>
												<th style="font-weight: normal;" colspan="2">Members</th>
											</tr>
										</thead>

											<tbody>
													<tr>
														<td width="5%"><small class="leader_avatar"></small></td>

														<td style="border-right: 1px solid #ecf0f1;"><small class="leader_name f16" style="color: #B0BEC5;"></small></td>
														<td>
															<span class="member_name font-red-intense f18"></span>
														</td>
														<td> <a href="#" class="pull-right f14 font-red-intense">Remove</a></td>
													</tr>
													<tr><td colspan="4"></td></tr>
											</tbody>
										</table>

								<p style="margin-top: -27px;">&nbsp;</p>

								<div class="panel-body">

									<form class="form form-horizontal" role="form" id="team_form_member" method="POST" action="">

									{{ csrf_field() }}

							
										<div class="form-group">
										
										<div class="team_id"></div>
									

										<div class="col-lg-12">
											<label class="f14">Members</label>
											<select class="form-control selectMembers f12" multiple="multiple" name="members[]">
															@foreach($members as $member)
																	<option value="{{ $member->id }}" data-image="/dashboard/assets/img/avatars/{{ $member->avatar }}">{{ $member->name }}</option>
															@endforeach
											</select>
												<!-- <div class="ui fluid multiple search selection dropdown">
											  <input type="hidden" multiple="multiple" name="members[]">
											  <i class="dropdown icon"></i>
											  <div class="default text">Select Country</div>
											  <div class="menu">
												@foreach($members as $member)
											  <div class="item" data-value="{{ $member->id }}"><i class="af flag"></i>{{ $member->name }}</div>
														@endforeach
											 </div>
											 </div> -->

										</div>
										</div>

									<div class="form-group"></div>
										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="team_addmember">Save</button>
													&nbsp;&nbsp;
												<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


				</div>




</div>

	</div>


</div>

						
							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right f14" role="alert" id="success">
								  <i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>


							
							<div class="message success col-lg-4 pull-right f14" role="alert" id="resetpass">
								  <i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
								</div>

								<div class="message error col-lg-4 pull-right f14" role="alert" id="error">
								  <i class="fa fa-warning f16"></i> &nbsp; Please fill all field   
								</div>
								



						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>


						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

		<!-- End #content -->



@stop
