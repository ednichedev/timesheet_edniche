@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin/team')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Teams</span>
									</a>
								</li>

									&nbsp;
									&nbsp;
									&nbsp;

								<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Add Members</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">
											
		<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">
														
								
								<h2><i class="im-list2 s16" style="margin-top: 12px; color: #B0BEC5;"></i>&nbsp;<small style="color: #B0BEC5;">Lists</small>
								<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 8px;"></i></button>
										

											<p style="margin-top: -25px;">&nbsp;</p>

													

													<div class="input-icon input-small ico">

														<i class="icon-magnifier" id="search" class=""></i>
														<i class="icon-magnifier" id="search-1" class="hidden"></i>
														<input type="text" class="form-control search f12" name="search_depart" id="search_depart" placeholder="Search..." autofocus="true">

												
												</div>

														<select  class="form-control show-user" name="show_depart" id="show_depart">
															<option value="5">5</option>
															<option value="10">10</option>
															<option value="30">30</option>
															<option value="100">All</option>
														</select>


													<div class="portlet-body">
														<div class="box-scroll">                                            
																		<div class="table-team-display">
																											
																		</div>
														</div>

													</div>
									</div>

	<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
	<div class="portlet-body">


							<!-- Add member --> 
							<div class="add_member_form">
								<h2>&nbsp;<small style="color: #B0BEC5;">Add Members | &nbsp;
								@foreach($teams as $team)
								<small class="team_avatar">
								<img src="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}" style='max-width: 30px; border-radius:50px;'>
								</small>&nbsp;
								<small class="f18" style="color: #B0BEC5;">
								{{ $team->title }}
								</small>
								&nbsp; &nbsp;
									@endforeach
								</h2> 
										<table class="table">
										<thead>
											<tr>
												<th style="font-weight: normal;">Leader</th>
												<th></th>
												<th style="font-weight: normal;" colspan="2">Members</th>
											</tr>
										</thead>

											<tbody>
											@foreach($teams as $team)
													<tr>
														<td width="5%">
														<img src="/dashboard/assets/img/avatars/{{ $team->leaders->avatar }}" style='max-width: 30px; border-radius:50px;'>
														</td>

														<td style="border-right: 1px solid #ecf0f1;"> {{ $team->leaders->name }}</td>
														<td>
															@foreach( $team->members->slice(0, 3) as $member)
															<div class="label label-default" style="padding-bottom: 5px;">
															<img src="/dashboard/assets/img/avatars/{{ $member->avatar }}" style='max-width: 20px; border-radius:50px;'>
															<span class="f12">{{ $member->name }}</span>
															</div>
																&nbsp;


															@endforeach
															@if($team->members->count() > 3)
																	<div class="label label-info"><span class="f12">All ( {{ $team->members->count() }} )</span></div>
																				
																			@else
																		
																@endif
														</td>
														<td> <a href="{{ route('team.removemember', $team->id) }}" class="pull-right f14 font-red-intense">Remove</a></td>
													</tr>
													@endforeach
													<tr><td colspan="4"></td></tr>
											</tbody>
										</table>

								<p style="margin-top: -27px;">&nbsp;</p>

								<div class="panel-body">

									<form class="form form-horizontal" role="form" method="POST" action="{{route('team.addmember.post')}}">

									{{ csrf_field() }}

							
										<div class="form-group">

											@foreach($teams as $team)
										<input type="hidden" name="team_id" value="{{ $team->id }}">
												@endforeach
										<div class="col-lg-12">

											<label class="f14">Members</label>
											<select class="form-control selectMembers f12" multiple="multiple" name="members[]">
												
																@foreach($members_t as $member_t)

																	@if($member_t->is_active == 1)
																		
																	<option value="{{ $member_t->id }}" data-image="/dashboard/assets/img/avatars/{{ $member_t->avatar }}">{{ $member_t->name }}</option>

																	
																	@endif
																	
																	@endforeach
													
											</select>
											

										</div>
										</div>

									<div class="form-group"></div>
										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="team_addmember">Save</button>
													&nbsp;&nbsp;
												<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


				</div>




</div>

	</div>


</div>

						
							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right" role="alert" id="success">
								  <i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>


							
							<div class="message success col-lg-4 pull-right" role="alert" id="resetpass">
								  <i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="error">
								  <i class="fa fa-warning f16"></i> &nbsp; Please fill all field   
								</div>
								



						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>


						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->        

		<!-- End #content -->



@stop