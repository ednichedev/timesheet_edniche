
												<table class="table dataTable no-footer">
														<tbody class="list">
															
															@foreach($departments as $department)
															<tr id="showtable">

																<td>{{ $department->title }}</td>
																<td>
																<img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $department->managers->avatar }}" style="max-width: 30px; height: 30px; border-radius: 100px;"></td>

																<td>{{ $department->managers->name }}</td>
																<td>
																@if($department->is_active == 1)
																<span class="label label-danger">Blocked</span>
																@else	
																@endif
																</td>
																<td>
																		<div class="dropdown dropdown-user">
																				<button class="btn btn-link dropdown-toggle pull-right" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class=" icon-settings"></i>
																				</button>

																			<ul class="dropdown-menu dropdown-menu-default pull-right"  id="dropdown-menu">
																			
																				<li class="f12">
																				
																				<a href="#" class="edit" data-value="{{ $department->id }}" data-name="{{ $department->title }}" data-desc="{{ $department->desc }}" data-manager="{{ $department->managers->name }}" data-id="{{$department->managers->id}}" data-image="/dashboard/assets/img/avatars/{{ $department->managers->avatar }}"><i class="icon-pencil f12"></i> &nbsp; Edit</a>
																				</li>

																				<li class="f12">
																				@if($department->is_active == 0)
																					<a href="#" class="btn-block-depart" data-value="{{ $department->id }}"><i class="icon-power"></i> &nbsp; Block
																					</a>
																					@else
																					<a href="#" class="btn-enable-depart" data-value="{{ $department->id }}">
																					<i class="icon-like"></i> &nbsp;Enable
																					</a>
																					@endif
																				</li>
																			</ul>
																		</div>

																</td>


															</tr>
																											
														@endforeach
														</tbody>


												</table>

	<ul class="pagination"></ul>




											

						