@extends('layouts.main-app')

			@section('content')


																				<!-- BEGIN CONTENT -->
																<div class="page-content-wrapper">
																				<!-- BEGIN CONTENT BODY -->
																				<div class="page-content">
																								<!-- BEGIN PAGE HEADER-->
								
																								<!-- BEGIN PAGE BAR -->
																								<div class="page-bar">
																												<ul class="page-breadcrumb">
																																
																																<li class="page-breadcrumb-deactive">
																																<a href="{{ url('/admin')}}">
																																				<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
																																				</a>
																																</li>
																																				&nbsp;
																																				&nbsp;
																																				&nbsp;
																																<li class="">
																																				<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Department</span>
																																</li>

																												</ul>

																								</div>
																								<!-- END PAGE BAR -->
																								<!-- END PAGE HEADER-->
																								<!-- BEGIN DASHBOARD STATS 1-->
																								<div class="row">
																																												
																												<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">
																																																								
																																
																																<h2><i class="im-list2 s16" style="margin-top: 10px; color: #B0BEC5;"></i>&nbsp;<small style="color: #B0BEC5;">Lists</small>
																																<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 8px;"></i></button>
																																								

																																												<p style="margin-top: -25px;">&nbsp;</p>

																																																				

																																																				<div class="input-icon input-small ico">

																																																								<i class="icon-magnifier" id="search"></i>
																																																								<i class="icon-magnifier hidden" id="search-1"></i>
																																																								<input type="text" class="form-control search f12" name="search_depart" id="search_depart" placeholder="Search..." autofocus="true">

																																																
																																																</div>

																																																								<select  class="form-control show-user" name="show_depart" id="show_depart">
																																																												<option value="5">5</option>
																																																												<option value="10">10</option>
																																																												<option value="30">30</option>
																																																												<option value="100">All</option>
																																																								</select>


																																																				<div class="portlet-body">
																																																								<div class="box-scroll">                                            
																																																													<div class="table-depart-display">
																																																																																																												
																																																													</div>
																																																								</div>

																																																				</div>
																																				</div>

																												<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
																																				
																								<div class="portlet-body">                                                                              
																												<!--    Create New User -->
																												<div class="department_form">

																																<h2 style="color: #B0BEC5; margin-top: -10px;">&nbsp;<small style="color: #B0BEC5;">Create</small></h2>
																																<p style="margin-top: -27px;">&nbsp;</p>
																																
																																<div class="panel-body">

																																<form class="form form-horizontal" role="form" id="department_form" method="POST" action="{{  route('department.create') }}">

																																				{{ csrf_field() }}

																												
																																								<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
																																												
																																												<div class="col-lg-12">
																																																<input type="text" class="form-control col-md-12 f12" placeholder="Title..." name="title" value="{{ old('title') }}" required autofocus id="title">
																																																@if ($errors->has('title'))
																																																				<span class="help-block">
																																																								<strong>{{ $errors->first('title') }}</strong>
																																																				</span>
																																																@endif
																																												</div>
																																								</div>
																																
																																								<!-- End .form-group  -->
																																								
																																								<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
																																												
																																												<div class="col-lg-12">
																																												<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..." id="desc"></textarea>
																																												</div>
																																								</div>
				
																																								<div class="form-group{{ $errors->has('manager_id') ? ' has-error' : '' }}">
																																												
																																												<div class="col-lg-12">

																																																				<div class="ui fluid selection dropdown f12">
																																																						<input type="hidden" name="managers_id">
																																																						<i class="dropdown icon"></i>
																																																						<div class="default text">Select Manager</div>
																																																						<div class="menu">
																																																								@foreach($users as $user)
																																																								<div class="item" data-value="{{ $user->id }}">
																																																										<img class="ui mini avatar image" src="/dashboard/assets/img/avatars/{{ $user->avatar }}">
																																																										{{ $user->name }}
																																																								</div>
																																																												@endforeach
																																																						</div>
																																																				</div>

																																
																																												</div>
																																								</div>

																																								
																																								<!-- End .form-group  -->

																																								<!-- End .form-group  -->
																																								<div class="form-group">
																																												<div class="col-lg-12">
																																																<button class="btn blue f12" id="department_submit">Save</button>
																																																				&nbsp;&nbsp;
																																																<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

																																												</div>
																																								</div>
																																								<!-- End .form-group  -->
																																				</form>
																																</div>
																																				
																												</div>

																								<!--    Reset Password -->
																												<div class="department_form_edit hidden">
																																<h2 style="color: #B0BEC5; margin-top: -10px;">&nbsp;<small style="color: #B0BEC5;">Edit | <small id="depart_name" class="hidden f16" style="color: #B0BEC5;"></small>
																																</h2> 
																																				<p style="margin-top: -27px;">&nbsp;</p>
																																<div class="panel-body">

																																				<form class="form form-horizontal" role="form" id="department_form_edit" method="POST" action="">

																																				{{ csrf_field() }}

																												
																																								<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
																																												
																																												<div class="col-lg-12 text-title">
																																																
																																																@if ($errors->has('title'))
																																																				<span class="help-block">
																																																								<strong>{{ $errors->first('title') }}</strong>
																																																				</span>
																																																@endif
																																												</div>
																																								</div>
																																
																																								<!-- End .form-group  -->
																																								
																																								<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
																																												
																																												<div class="col-lg-12 text-desc">
																																												<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..."></textarea>
																																												</div>
																																								</div>
				
																																								<div class="form-group{{ $errors->has('manager_id') ? ' has-error' : '' }}">
																																												
																																												<div class="col-lg-12">
																																								
																																																				

																																																<div class="managers_id">
																																																								

																																																</div>

																																																				<div class="ui fluid selection dropdown hidden">
																																																						<input type="hidden" name="" class="att_name">
																																																						<i class="dropdown icon"></i>
																																																						<div class="default text">Select Manager</div>
																																																						<div class="menu">
																																																								@foreach($users as $user)
																																																								
																																																								<div class="item" data-value="{{ $user->id }}">
																																																										<img class="ui mini avatar image" src="/dashboard/assets/img/avatars/{{ $user->avatar }}">
																																																										{{ $user->name }}
																																																								</div>
																																																												@endforeach
																																																						</div>
																																																				</div>



																																
																																												</div>
																																								</div>

																																								
																																								<!-- End .form-group  -->

																																								<!-- End .form-group  -->
																																								<div class="form-group">
																																												<div class="col-lg-12">
																																																<button class="btn blue f12" id="department_update">Update</button>
																																																				&nbsp;&nbsp;
																																																<a href="#" class="btn btn-default f12" id="back-depart">Cancel</a>

																																												</div>
																																								</div>
																																								<!-- End .form-group  -->
																																				</form>


																																</div>
																																				
																												</div>

																								</div>
																</div>



												</div>
																								
																												<!-- Notification  -->
																												<div class="message success col-lg-6 pull-right" role="alert" id="success">
																																		<i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
																																</div>


																												
																												<div class="message success col-lg-4 pull-right" role="alert" id="resetpass">
																																		<i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
																																</div>

																																<div class="message error col-lg-4 pull-right" role="alert" id="error">
																																		<i class="fa fa-warning"></i> &nbsp; Please fill all field   
																																</div>
																																


																								<div class="clearfix"></div>
																								<!-- END DASHBOARD STATS 1-->
																								<div class="border-bottom-2"></div>
																								
																								
																				</div>
																				<!-- END CONTENT BODY -->
																</div>
																<!-- END CONTENT -->        

								<!-- End #content -->



@stop