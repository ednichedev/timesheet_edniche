<div class="assignments hidden">
								
								<button class="btn btn-link btn-close-assign pull-right"><i class="fa fa-close f16" style="margin-top: 72px;"></i></button>
								
							
								<div class="col-md-8" style="margin-top: 80px;">
								<small style="color: #B0BEC5;" class="f18"></small>
								<small id="project_name" class="f18" style="color: #B0BEC5;"></small>
						
							<section style="margin-top: 8px;">	
								<span class="f12">Started</span>
									<span id="project_start_date" class="f12"></span> <span class="f13">&nbsp; | &nbsp;</span>
									<span class="f12">Ends</span>
									<span id="project_date" class="f12"></span>
						</section><br>
						<section style="margin-top: -10px;">	
									<span class="f12"><strong class="f14 assign_left" style="color: #e74c3c; margin-bottom: 15px;"></strong> Assignments Left</span><br>
									<div class="spacing"></div>
									<span class="f12 assign_complet"><strong class="f14" style="color: #2ecc71;">2</strong> Assignments Completed</span>
						</section>
					</div>

									<div class="col-md-4" style="margin-top: 80px; margin-bottom: 0px;">
											<small class="f16" style="text-align: center;">

													<div class="circle">
														<span id="project_assign_count"></span>
													</div>

													<div class="circle-complet hidden">
														<i class="fa fa-check f20" style="color:#2ecc71;"></i>
													</div>

														<h5 class="f14" style="margin-top: 5px; margin-left: 15px;">Assignments</h5>
														<h5 class="f16 hidden" style="margin-top: 5px; color:#2ecc71;">Complected</h5>
											</small>
									</div>


								<p style="margin-top: -47px;">&nbsp;</p>

								<div class="border-bottom-3"></div>

								<div class="panel-body">
									<div class="box-scroll-2">
										<div class="table-assign-display">

										</div>
									</div>
								</div>

								</div>

