	<script type="text/javascript">

 $(document).ready(function() {

		<?php for($i=1;$i<150;$i++){ ?>

				$(document).on('click', '#p_user<?php echo $i; ?>' , function(){

							$("#ui-user<?php echo $i; ?>").toggle();

							});

		<?php }?>
	

});

		

$(document).ready(function(){

	<?php for($i=1;$i<150;$i++){ ?>

	$('#myModal<?php echo $i; ?>').on('shown.bs.modal', function () {
 
	
});


<?php }?>

});



</script>

	<table class="table">

		<tbody>
			@php
			$i=1
			@endphp
			@foreach($projects as $project)
				@foreach($project->assignmentprojects as $assign)
					<tr>
						<td width="5%">						 
							<input type="checkbox" id="check">
						</td>
						<td width="5%">
					
						<a href="#" id="p_user{{ $i }}">
						<img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $assign->user->avatar }}" style="max-width: 18px;">
						</a>
							<div class="ui-user" id="ui-user{{ $i }}" style="display: none;">
							{{ $assign->user->name }}
								<div class="polyon"></div>
							</div>
						
						</td>
							
						<td>{{ Str::limit($assign->title, 50,'...') }}</td>

						<td width="15%" class="color-light">
						{{ Carbon\Carbon::parse($assign->start_date)->format('d-M-Y') }}
						</td>

						<td width="15%" class="color-light">
						{{ Carbon\Carbon::parse($assign->duedate)->format('d-M-Y') }}
						</td>

						<td width="5%">
											
									<!-- Modal -->
										<div class="modal fade  bs-example-modal-sm" id="myModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
										  <div class="modal-dialog modal-sm" role="document">
											<div class="modal-content">
											  <div class="modal-header" style="background-color: #e74c3c; color: #FFFFFF; border-top-left-radius: 5px; border-top-right-radius: 5px;">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #FFFFFF;"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="mySmallModalLabel">Delete Confirmation</h4>
											  </div>
											  <div class="modal-body">

												   <form method="POST" action="{{route('project.remove.post')}}">
																{{ csrf_field() }}

																		<input type="hidden" name="assign" value="{{$assign->id}}" id="assign{{$i}}">

																				
																	<button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 140px;">Close</button>
																	&nbsp;
																	<button class="btn btn-primary pull-right" type="submit">Yes</button>
															

														</form>

											  </div>
											  
											  
											</div>
										  </div>
										</div>

										<button class="btn btn-link" style="margin-top: -10px;" data-toggle="modal" data-target="#myModal{{$i}}"><i class="fa fa-close f11"></i></button>
							

						</td>

					</tr>
					@php
					$i++
					@endphp

			@endforeach
		@endforeach
		</tbody>
</table>

