@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;

								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin/project')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Project</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Remove Members</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">
											
		<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">
														
								
								<h2><i class="im-list2 s16" style="margin-top: 12px; color: #B0BEC5;"></i>&nbsp;<small style="color: #B0BEC5;">Lists</small>
								<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 8px;"></i></button>
										

											<p style="margin-top: -25px;">&nbsp;</p>

													

													<div class="input-icon input-small ico">

														<i class="icon-magnifier" id="search"></i>
														<i class="icon-magnifier hidden" id="search-1"></i>
														<input type="text" class="form-control search f12" name="search_project" id="search_project" placeholder="Search..." autofocus="true">


												</div>

														<select  class="form-control show-user" name="show_depart" id="show_depart">
															<option value="5">5</option>
															<option value="10">10</option>
															<option value="30">30</option>
															<option value="100">All</option>
														</select>


													<div class="portlet-body">
														<div class="box-scroll">											
																		<div class="table-project-display">
																											
																		</div>
														</div>

													</div>
									</div>

	<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
	<div class="portlet-body">


							<!-- Add member --> 
							<div class="remove_member_form">
								<h2>&nbsp;<small style="color: #B0BEC5;">Remove Members | 	
								@foreach($projects as $project)

								<small class="f18" style="color: #B0BEC5;">
								{{ $project->title }}
								</small>
								&nbsp; &nbsp;
									@endforeach
								
								</h2> 
										<table class="table">
										<thead>
											<tr>
												<th style="font-weight: normal;">Leader</th>
												<th></th>
												<th></th>
											</tr>
										</thead>

											<tbody>
											@foreach($teams as $team)
													<tr>
														<td width="5%">
														<img src="/dashboard/assets/img/avatars/{{ $team->leaders->avatar }}" style='max-width: 30px; border-radius:50px;'>
														</td>

														<td>	{{ $team->leaders->name }}</small></td>
														
														<td> <a href="{{url('admin/project')}}" class="pull-right f14 font-red-intense">Cancel</a></td>
													</tr>
													@endforeach
													<tr><td colspan="4"></td></tr>
											</tbody>
										</table>

								<p style="margin-top: -47px;">&nbsp;</p>

								<div class="panel-body">
									
									<table class="table">
										<thead>
												<tr>
														<th colspan="3" style="font-weight: normal;">Members</th>
														
													</tr>
										</thead>
										<tbody>

											@foreach( $projects as $project)

												@foreach( $project->user as $users)
											<tr>
												<td width="10%">
												<img src="/dashboard/assets/img/avatars/{{ $users->avatar }}" style='max-width: 25px; border-radius:50px;'></td>
												<td>{{ $users->name }}</td>

												<td width="10%">
														<form method="POST" action="{{ route('project.removemember.post') }}">
														{{ csrf_field() }}

																@foreach($projects as $project)

																<input type="hidden" name="project_id" value="{{ $project->id }}">
																@endforeach
																<input type="hidden" name="members" value="{{ $users->id }}">

																<button class="btn btn-link"><i class="fa fa-close font-red-intense"></i></button>
															
														</form>

												</td>

											</tr>
											@endforeach
											@endforeach
										</tbody>

									</table>




				</div>

</div>

@include('dashboard-admin.project.show-assignments')

@include('dashboard-admin.project.edit-project')

	</div>


</div>

						
							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right" role="alert" id="success">
								  <i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>


							
							<div class="message success col-lg-4 pull-right" role="alert" id="resetpass">
								  <i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="error">
								  <i class="fa fa-warning f16"></i> &nbsp; Please fill all field   
								</div>
								



						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>


						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

		<!-- End #content -->



@stop