<div class="project_form_edit hidden">
								
								<button class="btn btn-link btn-close-edit pull-right"><i class="fa fa-close f16" style="margin-top:0px;"></i></button>

								<h2>&nbsp;<small style="color: #B0BEC5;">Edit |</small>
								<small class="project_name" style="color: #B0BEC5;"></small>
								</h2> 
									<p style="margin-top: -27px;">&nbsp;</p>
								<div class="panel-body">

									<form class="form form-horizontal" role="form" id="project_form_edit" method="POST" action="">

									{{ csrf_field() }}

							
										<div class="form-group">
											
											<div class="col-lg-12">
												
												<input type='text' class="form-control f12" name="title" id="title" placeholder="Title..." required autofocus>

											</div>
										</div>
								
										<!-- End .form-group  -->
										
						<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12 text-desc">
											
											</div>
										</div>
	
										<div class="form-group">
											
											<div class="col-lg-12">
										
													

												<div class="teams_id">
														
												</div>

													<div class="ui fluid selection dropdown hidden f12">
													  <input type="hidden" name="" class="att_name">
													  <i class="dropdown icon"></i>
													  <div class="default text">Select Team</div>
													  <div class="menu">
													@foreach($teams as $team)
														
														<div class="item" data-value="{{ $team->id }}">
														  <img class="ui mini avatar image" src="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}">
														  {{ $team->title }}
														</div>
															@endforeach
													  </div>
													</div>

											</div>
										</div>

									<div class="form-group">

												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text"  class="form-control" placeholder="Start Date..."  id="example3" name="start_date">
												</div>
												</div>


												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text" class="form-control" placeholder="End Date..."  id="example4" name="duedate">
												</div>
												</div>


											</div>


										
										<!-- End .form-group  -->

										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="project_update">Update</button>
													&nbsp;&nbsp;
												<a href="#" class="btn btn-default f12" id="back">Cancel</a>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


					</div>

				</div>
						