
												<table class="table">
														<tbody>
															@php
																	$i=1
															@endphp

															@foreach($projects as $project)
															<tr>
																	
																	<td>

																	@if($project->completed == 0)

																	<form method="POST" action="" id="project_admin_complet_form" class="">
																	{{ csrf_field() }}
																 <input type="hidden" name="completed" value="1">
																 

																	<input type="checkbox" id="checkadminproject{{$i}}" data-id="{{ $project->id }}">
																	</form>


																	@else

																	<form method="POST" action="" id="project_admin_uncomplet_form" class="">
																	{{ csrf_field() }}
																 <input type="hidden" name="uncompleted" value="0">
																 

																 <input type="checkbox" id="uncheckadminproject{{$i}}" data-id="{{ $project->id }}" checked="checked">
																	</form>

																	</td>

																	@endif

																	<td><a href="{{ route('admin.get.assignmentproject', $project->id )}}" class="color-light">{{ $project->title }}</a>
																<br>
																			<span class="f11">
																					<i class="fa fa-clock-o"></i>
																					{{ Carbon\Carbon::parse($project->duedate)->format('d-M-Y') }}																			
																			</span>
																<br>
																	<span class="f12 color-light">Assignments( {{ $project->assignmentprojects->count() }} ) 
																	<a href="{{ route('project.addassigntment', $project->id) }}" class="btn btn-sm btn-link"><i class="fa fa-plus"></i> </a>

																	</td>
															
																<td width="8%">
																<img class="user-avatar" src="/dashboard/assets/img/teams/avatar/{{ $project->teams->avatar }}" style="max-width: 30px; border-radius: 50px; border-radius: 100px;"></td>

																<td>{{ $project->teams->title }}
																<br>
																
																				<img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $project->teams->leaders->avatar }}" style="max-width: 15px; border-radius: 100px;">
																				
																				<span class="f11" style="color: #95a5a6;">{{ $project->teams->leaders->name }}
																				</span>

																	<br>
																	<span class="f12 color-light">Members ( {{ $project->user->count() }} ) 
																	<a href="{{ route('project.addmember', $project->id) }}" class="btn btn-sm btn-link"><i class="fa fa-plus"></i> </a>

																</td>

															


																<td>
																@if($project->is_active == 1)
																<span class="label label-danger">Blocked</span>
																@else	
																@endif
																</td>

																<td>
																	@if($project->completed == 1)
																			<span class="badge" style="background-color: #2ecc71;">
																			 Completed
																			 </span>

																			@else


																			 <?php
																							 $countAssignCompleted = App\Assignmentproject::where('projects_id', $project->id)->where('completed', 1)->get();
																			 
																				$current = 0;
																				$percent=0;
																				$assignment = $countAssignCompleted->count();
																				$total = $project->assignmentprojects->count() * 1000;
																				$current = $assignment * 1000;
																				if ($total!=0) {				 	
																					$percent = ($current /	$total) *100;
																					settype($percent, "integer");	
																				}
																			 
																			 ?>

																			 <span class="badge" style="background-color: #2ecc71;">
																				{{$percent}} % 
																			 </span>


																	@endif
																</td>


																<td width="10%">
																
																		<div class="dropdown dropdown-user">
																				<button class="btn btn-link dropdown-toggle pull-right" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class=" icon-settings"></i>
																				</button>

																			<ul class="dropdown-menu dropdown-menu-default pull-right"  id="dropdown-menu">
																			
																				<li class="f12">
																				
																				<a href="#" class="project_edit" data-value="{{ $project->id }}" data-name="{{ $project->title }}" data-desc="{{ $project->desc }}" data-team="{{ $project->teams->title }}" data-id="{{$project->teams->id}}" data-image="/dashboard/assets/img/teams/avatar/{{ $project->teams->avatar }}" data-start-date="{{ $project->start_date }}" data-duedate="{{ $project->duedate }}"><i class="icon-pencil f12"></i> &nbsp; Edit</a>
																				</li>

																				<li class="f12">
																				@if($project->is_active == 0)
																					<a href="#" class="btn-block-project" data-value="{{ $project->id }}"><i class="icon-power"></i> &nbsp; Block
																					</a>
																					@else
																					<a href="#" class="btn-enable-project" data-value="{{ $project->id }}">
																					<i class="icon-like"></i> &nbsp;Enable
																					</a>
																					@endif
																				</li>
																			</ul>
																		</div>

																</td>


															</tr>
																@php
																		$i++
																@endphp							
														@endforeach
														</tbody>


												</table>





											

						