@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin/project')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Project</span>
									</a>
								</li>

									&nbsp;
									&nbsp;
									&nbsp;

								<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Add Assignment</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">
											
		<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
														
								
								<h2><i class="im-list2 s16" style="margin-top: 12px; color: #B0BEC5;"></i>&nbsp;<small class="f18" style="color: #B0BEC5;">Lists</small>
								<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 8px;"></i></button>
										

				<p style="margin-top: -25px;">&nbsp;</p>
			@foreach($projects as $project)
													
				 <span class="f11 block-project color-light">
					Started &nbsp;{{ Carbon\Carbon::parse($project->start_date)->format('d-M-Y') }}&nbsp; | &nbsp; 
				 Ends &nbsp;{{ Carbon\Carbon::parse($project->duedate)->format('d-M-Y') }}
				 </span>
@endforeach
												


<div class="portlet-body">
	<div class="box-scroll">											
					<table class="table">

		<tbody>
			@php
			$i=1
			@endphp
			@foreach($projects as $project)

				@foreach($project->assignmentprojects as $assign)
					<tr>
						<td width="5%">						 
							<input type="checkbox" id="check">
						</td>
						<td width="5%">
						
						<a href="#" class="popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="{{ $assign->user->name }}">
						<img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $assign->user->avatar }}" style="max-width: 18px;">
						</a>

						</td>
							
						<td>
						<a href="{{route('admin.get.taskproject', $assign->id)}}">
						{{ Str::limit($assign->title, 50,'...') }}
						</a>
							<br>
							<span class="f12 color-light">Tasks ({{ $assign->tasks->count()}})</span>

						</td>

						<td>
						<button class="btn btn-link f11 popovers" data-container="body" data-trigger="hover" data-placement="top" data-content="New Task" aria-expanded="false" data-toggle="modal" data-target="#myTaskModal"  value="{{ $assign->id }}" data-user-id="{{ $assign->user->id }}" data-ass-date="{{$assign->duedate}}" id="admin-add-task{{$i}}"><i class="fa fa-plus"></i></button>
						
						</td>

						<td width="18%" class="color-light">
						{{ Carbon\Carbon::parse($assign->start_date)->format('d-M-Y') }}
						</td>

						<td width="18%" class="color-light">
						{{ Carbon\Carbon::parse($assign->duedate)->format('d-M-Y') }}
						</td>

						<td style="color: #e67e22;">
							
							<?php 
							$currentDateTime = date('Y-m-d');
							$difference = strtotime($assign->duedate) - strtotime($assign->start_date);

								$tdifference = strtotime($assign->end_time) - strtotime($assign->start_time);

						?>

						@if(floor($difference / (60*60)) < 24)

							{{ $days = floor($tdifference / (60*60) )}} Hours

							@elseif(floor($difference / (60*60)) >= 24)
									{{ $days = floor($difference / (60*60*24) )}} Days

						@endif

						</td>

						<td width="5%">
											
									<!-- Modal -->
										<div class="modal fade  bs-example-modal-sm" id="myModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
										  <div class="modal-dialog modal-sm" role="document">
											<div class="modal-content">
											  <div class="modal-header" style="background-color: #e74c3c; color: #FFFFFF; border-top-left-radius: 5px; border-top-right-radius: 5px;">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #FFFFFF;"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="mySmallModalLabel">Delete Confirmation</h4>
											  </div>
											  <div class="modal-body">

												   <form method="POST" action="{{route('project.remove.post')}}">
																{{ csrf_field() }}

																		<input type="hidden" name="assign" value="{{$assign->id}}" id="assign{{$i}}">

																				
																	<button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 140px;">Close</button>
																	&nbsp;
																	<button class="btn btn-primary pull-right" type="submit">Yes</button>
															

														</form>

											  </div>
											  
											  
											</div>
										  </div>
										</div>

										<button class="btn btn-link" style="margin-top: -10px;" data-toggle="modal" data-target="#myModal{{$i}}"><i class="fa fa-close f11"></i></button>
							

						</td>

					</tr>
					@php
					$i++
					@endphp



			@endforeach
		@endforeach
		</tbody>
</table>
					</div>

				</div>
</div>

<div class="portlet light border-left col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
	<div class="portlet-body">


							<!-- Add member --> 
							<div class="add_assignt_form">
								<h2>&nbsp;<small class="f18" style="color: #B0BEC5;">Add Assignments |
								@foreach($projects as $project)

								<small class="f18" style="color: #B0BEC5;">
								{{ $project->title }}
								</small>
								&nbsp; &nbsp;
									@endforeach
								</h2> 

								<table class="table">
										<thead>
											<tr>
												<th style="font-weight: normal;" colspan="2">Leader of Project</th>
												
												<th style="font-weight: normal;" colspan="2">Assignments of Project</th>
											</tr>
										</thead>

											<tbody>
											@foreach($teams as $team)
													<tr>
														<td width="5%">
														<img src="/dashboard/assets/img/avatars/{{ $team->leaders->avatar }}" style='max-width: 20px; border-radius:50px;'>
														</td>

														<td style="border-right: 1px solid #ecf0f1;">	{{ $team->leaders->name }}</small></td>
														<td>

														@foreach($projects as $project)
															<div class="chart-peace">
															@if(count($project->assignmentprojects)>=90)
															<div class="plus"><i class="fa fa-plus"></i></div>
															
															@endif
															<span class="f12">{{ $project->assignmentprojects->count() }}
															</span>
															</div>

															@endforeach

															@if($project->user->count() > 3)
																	<div class="label label-info">

																	<span class="f12">All ( {{ $project->user->count() }} )</span>

																	</div>
																				
																			@else
																		
																@endif
														</td>
														
													</tr>
													@endforeach

													<tr>
														<td colspan="4"></td>
													</tr>
													
											</tbody>
										</table>
							

								<p style="margin-top: -67px;">&nbsp;</p>

								<div class="panel-body">

									<form class="form form-horizontal" role="form" method="POST" action="{{route('project.addassigntment.post')}}">

									{{ csrf_field() }}

									<div class="form-group">
											<div class="col-lg-12">

											<input type="hidden" name="auth_id" class="form-control f12" value="{{ Auth::user()->id }}">

												<input type="text" name="title" class="form-control f12" required autofocus placeholder="Title...">
											</div>
									</div>

									<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
											<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..." id="desc"></textarea>
											</div>
										</div>


										

										<div class="form-group">

											@foreach($projects as $project)
										<input type="hidden" name="project_id" value="{{ $project->id }}">

										<input type="hidden" name="project_duedate" value="{{ $project->duedate }}">

												@endforeach
										<div class="col-lg-12">

											<label class="color-light f12">Assign to</label>
											<select class="form-control selectMembers f12" name="user_id">
											 <option></option>
												@foreach($projects as $project)
												
																@foreach($project->user as $users)
																																	
																	<option value="{{ $users->id }}" data-image="/dashboard/assets/img/avatars/{{ $users->avatar }}">{{ $users->name }}</option>

																	@endforeach

																	<option value="{{ $project->teams->leaders->id }}" data-image="/dashboard/assets/img/avatars/{{ $project->teams->leaders->avatar }}">{{ $project->teams->leaders->name }}</option>

														@endforeach
											</select>
											

										</div>
										</div>

										<div class="form-group">

												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text"  class="form-control" placeholder="Start Date..."  id="example1" name="start_date">
												</div>
												</div>


												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
			<input  type="text" class="form-control" placeholder="End Date..."  id="example2" name="duedate">
			</div>
			</div>


											</div>


										<div class="form-group">

													<div id="log" class="col-md-3 f18" style=" text-align: center; color: #e67e22;"></div>	
												
														
									</div>

											@if ($errors->has('duedate'))
													<span class="col-lg-6 help-block alert alert-danger alert-dismissible" role="alert">
																	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																	</button>
														<strong class="f12">{{ $errors->first('duedate') }}</strong>
														</span>
												@endif

									<div class="form-group"></div>
										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="team_addmember">Save</button>
													&nbsp;&nbsp;
												<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


				</div>


</div>


@include('dashboard-admin.project.add-task')	


</div>
							
								

</div>

						
							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right" role="alert" id="success">
								  <i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>


							
							<div class="message success col-lg-4 pull-right" role="alert" id="resetpass">
								  <i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="error">
								  <i class="fa fa-warning f16"></i> &nbsp; Please fill all field   
								</div>
								



						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>


						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

		<!-- End #content -->



@stop

@section('javascript')

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 -->
<script type="text/javascript">
	
	$(document).ready(function () {
				
			$(document).on('change', '#example2', function () {


				fromDate = Date.parse($('#example1').val());

				toDate = Date.parse($('#example2').val());

				var dateDiff = (toDate - fromDate);

				var DD = Math.floor(dateDiff / 3600 / 24);
				var formatdate = DD / 1000;
			

					$( "#log" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

					
				});

	$(document).on('change', '#end_time', function () {

				// var date = moment($("#duedate").val());

				// var momemt = moment(date).endOf('day').fromNow();
				// fromDate = parseInt(new Date($("#example1").val())*1000); 
				fromDate = Date.parse($('#example5').val());

	// toDate = parseInt(new Date($("#example2").val())*1000);

	toDate = Date.parse($('#example6').val());

				fromtime = parseInt($("#start_time").val()); 
	totime = parseInt($("#end_time").val());
				
				var timeDiff = (totime - fromtime);
				var dateDiff = (toDate - fromDate);

	var DD = Math.floor(dateDiff / 3600 / 24);
		var formatdate = DD / 1000;
				var formatted = ((timeDiff < 10) ? ("0" + timeDiff + " Hours") : timeDiff);

				if (toDate > fromDate) {

					$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

				}else{

						$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatted+"</div>" );

				}

					
				});

	});




	$(document).ready(function(){

	<?php
		foreach($projects as $project){

			$count = $project->assignmentprojects->count();

			} 
	?>

<?php for($i=1;$i<=$count;$i++){ ?>

$(document).on('click', '#admin-add-task<?php echo $i ?>', function(){

			var id = $('#admin-add-task<?php echo $i ?>').val();

			var user_id = $('#admin-add-task<?php echo $i ?>').attr("data-user-id");
			var ass_date = $('#admin-add-task<?php echo $i ?>').attr("data-ass-date");

		$('#myTaskModal').on('shown.bs.modal', function () {

				$( "input[name~='assignment_id']" ).val(id);

				$( "input[name~='user_id']" ).val(user_id);

				$( "input[name~='assignmentpro_duedate']" ).val(ass_date);
				

		 });



});

<?php }?>

});

</script>

@stop