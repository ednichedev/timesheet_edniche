@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->

						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">

								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Project</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">

		<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">


								<h2><small style="color: #B0BEC5;">Lists</small>
							

											<p style="margin-top: -25px;">&nbsp;</p>

													<div class="input-icon input-small ico">

														<i class="icon-magnifier" id="search"></i>
														<i class="icon-magnifier hidden" id="search-1"></i>
														<input type="text" class="form-control search f12" name="search_project" id="search_project" placeholder="Search..." autofocus="true">


												</div>

														<select  class="form-control show-user" name="show_project" id="show_project">
															<option value="5">5</option>
															<option value="10">10</option>
															<option value="30">30</option>
															<option value="100">All</option>
														</select>


													<div class="portlet-body">

															<div class="box-scroll">
																			<div class="table-project-display">

																			</div>
															</div>

													</div>

						</div>



<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">

	<div class="portlet-body">

							<!-- 	Create New User -->
							<div class="project_form">

								<h2 style="color: #B0BEC5; margin-top: -10px;">&nbsp;<small style="color: #B0BEC5;">Create</small></h2>
								<p style="margin-top: -27px;">&nbsp;</p>

				<div class="panel-body">

								<form class="form form-horizontal" role="form" id="project_form" method="POST" action="{{  route('project.create') }}">

									{{ csrf_field() }}


										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

											<div class="col-lg-12">
												<input type="text" class="form-control col-md-12 f12" placeholder="Title..." name="title" required autofocus id="title">
												@if ($errors->has('title'))
													<span class="help-block">
														<strong>{{ $errors->first('title') }}</strong>
													</span>
												@endif
											</div>
										</div>

										<!-- End .form-group  -->

										<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">

											<div class="col-lg-12">
											<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..." id="desc"></textarea>
											</div>
										</div>

										<div class="form-group">

											<div class="col-lg-12">

												<div class="ui fluid selection dropdown f12">
													  <input type="hidden" name="teams_id">
													  <i class="dropdown icon"></i>
													  <div class="default text">Select Team</div>
													  <div class="menu">
														@foreach($teams as $team)

														<div class="item" data-value="{{ $team->id }}">
														  <img class="ui mini avatar image" src="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}">
														  {{ $team->title }}
														</div>
															@endforeach
													  </div>
													</div>


													  </div>
													</div>

								<div class="form-group">

												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text"  class="form-control" placeholder="Start Date..."  id="example1" name="start_date">
												</div>
												</div>


												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
			<input  type="text" class="form-control" placeholder="End Date..."  id="example2" name="duedate">
			</div>
			</div>


											</div>


										<div class="form-group">

													<div id="log" class="col-md-3 f18" style=" text-align: center; color: #e67e22;"></div>	
												
														
									</div>




										<!-- End .form-group  -->

										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="project_submit">Save</button>
													&nbsp;&nbsp;
												<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>

				</div>

</div>


				<!-- 	Edit form -->


				<!-- 	Edit form -->
					<div class="project_form_edit hidden">
								
								<button class="btn btn-link btn-close-edit pull-right"><i class="fa fa-close f16" style="margin-top:0px;"></i></button>

								<h2>&nbsp;<small style="color: #B0BEC5;">Edit |</small>
								<small class="project_name" style="color: #B0BEC5;"></small>
								</h2> 
									<p style="margin-top: -27px;">&nbsp;</p>
								<div class="panel-body">

									<form class="form form-horizontal" role="form" id="project_form_edit" method="POST" action="">

									{{ csrf_field() }}

							
										<div class="form-group">
											
											<div class="col-lg-12">
												<input type='text' class="form-control f12" name="title" id="title" placeholder="Title..." required autofocus>
											
											</div>
										</div>
								
										<!-- End .form-group  -->
										
						<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12 text-desc">
											
											</div>
						</div>
	
										<div class="form-group">
											
											<div class="col-lg-12">
										
													

												<div class="teams_id">
														
												</div>

													<div class="ui fluid selection dropdown hidden f12">
													  <input type="hidden" name="" class="att_name">
													  <i class="dropdown icon"></i>
													  <div class="default text">Select Team</div>
													  <div class="menu">
													@foreach($teams as $team)
														
														<div class="item" data-value="{{ $team->id }}">
														  <img class="ui mini avatar image" src="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}">
														  {{ $team->title }}
														</div>
															@endforeach
													  </div>
													</div>

											</div>
										</div>

									<div class="form-group">

												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text"  class="form-control" placeholder="Start Date..."  id="example3" name="start_date">
												</div>
												</div>


												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text" class="form-control" placeholder="End Date..."  id="example4" name="duedate">
												</div>
												</div>


											</div>


										
										<!-- End .form-group  -->

										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="project_update">Update</button>
													&nbsp;&nbsp;
												<a href="#" class="btn btn-default f12" id="back">Cancel</a>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


					</div>

				</div>
						





	</div>


</div>
							

							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right f14" role="alert" id="success">
								  <i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>



							<div class="message success col-lg-4 pull-right f14" role="alert" id="resetpass">
								  <i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password
								</div>

								<div class="message error col-lg-4 pull-right f14" role="alert" id="error">
								  <i class="fa fa-warning f16"></i> &nbsp; Please fill all field
								</div>




						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>




					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->

		<!-- End #content -->



@stop

@section('javascript')

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 -->
<script type="text/javascript">
	
	$(document).ready(function () {
				
				$(document).on('change', '#example2', function () {


				fromDate = Date.parse($('#example1').val());

				toDate = Date.parse($('#example2').val());

				var dateDiff = (toDate - fromDate);

	var DD = Math.floor(dateDiff / 3600 / 24);
		var formatdate = DD / 1000;
			

					$( "#log" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

					
				});
	});



$(document).ready(function() {

	 $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}}); 
	 
	 $.ajaxSetup({cache: false });

});

	
	$(document).ready(function () {
	
	<?php for($i=1;$i<150;$i++){ ?>

	$(document).on('change', '#checkadminproject<?php echo $i; ?>', function() {

		var id = $("#checkadminproject<?php echo $i; ?>").attr("data-id");

				$.ajax({
					
						url: "/admin/project/completed/project/"+id,
						type: 'POST',
						data: $('#project_admin_complet_form').serialize(), 
						dataType: 'json',
						cache: false,
						success: function(data){

			
							window.location.reload(true);

						}

				});

				event.preventDefault();


	});

	<?php }?>

});


// Uncompleted assignment

$(document).ready(function () {
	
		<?php for($i=1;$i<150;$i++){ ?>

	$(document).on('change', '#uncheckadminproject<?php echo $i; ?>', function() {

		var id = $("#uncheckadminproject<?php echo $i; ?>").attr("data-id");
		
				$.ajax({
					
						url: "/admin/project/uncompleted/project/"+id,
						type: 'POST',
						data: $('#project_admin_uncomplet_form').serialize(), 
						dataType: 'json',
						cache: false,
						success: function(data){
							

							window.location.reload(true);

								 

						}
				});

				event.preventDefault();


	});

	<?php }?>



});



</script>

@stop