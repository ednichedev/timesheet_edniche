@extends('layouts.main-app')

			@section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li class="page-breadcrumb-deactive">
								<a href="{{ url('/admin')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
									</a>
								</li>
									&nbsp;
									&nbsp;
									&nbsp;
								<li class="page-breadcrumb-active">
							
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Assignment</span>
								
								</li>
							

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">
											
		<div class="portlet light border-right col-xs-12 col-sm-12 col-md-6 col-lg-6">
														
								
								<h2><i class="im-list2 s16" style="margin-top: 12px; color: #B0BEC5;"></i>&nbsp;<small class="f18" style="color: #B0BEC5;">Lists</small>
								<button class="btn btn-link pull-right add-new"><i class="fa fa-plus f16" style="margin-top: 8px;"></i></button>
										

											<p style="margin-top: -25px;">&nbsp;</p>

													


													<div class="input-icon input-small ico">

														<i class="icon-magnifier" id="search"></i>
														<i class="icon-magnifier hidden" id="search-1"></i>
														<input type="text" class="form-control search f12" name="search_assignmentsingle" id="search_assignmentsingle" placeholder="Search..." autofocus="true">


												</div>

														<select  class="form-control show-user" name="show_assignmentsingle" id="show_assignmentsingle">
															<option value="100">All</option>
															<option value="5">5</option>
															<option value="10">10</option>
															<option value="30">30</option>
														</select>


													<div class="portlet-body">
														<div class="box-scroll">											
																		<div class="table-assignmentsingle-display">
																											
																		</div>
														</div>

													</div>
									</div>

	<div class="portlet light col-xs-12 col-sm-12 col-md-6 col-lg-6">
									
	<div class="portlet-body">


							<!-- Add member --> 
							<div class="add_assignt_form">

								<h2>&nbsp;<small class="f18" style="color: #B0BEC5;">Create</small>
				
								</h2> 

								<p style="margin-top: -47px;">&nbsp;</p>

								<div class="panel-body">

									<form class="form form-horizontal" role="form" method="POST" action="{{ route('assignment.create' )}}" id="submit_assignt_form">

									{{ csrf_field() }}

									<div class="form-group">
									<input type="hidden" name="create_by" value="{{Auth::user()->id}}">
									</div>

									<div class="form-group">
											<div class="col-lg-12">

												<input type="text" name="title" class="form-control f12" required autofocus placeholder="Title..." id="title">
											</div>
									</div>

									<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
											<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..." id="desc"></textarea>
											</div>
										</div>


										<div class="form-group">

				
										<div class="col-lg-12">

											<label class="color-light f12">Assign to</label>
											<select class="form-control selectMembers f12" name="users_id" id="user">

															<option></option>
											
																@foreach($users as $user)
																																	
																	<option value="{{ $user->id }}" data-image="/dashboard/assets/img/avatars/{{ $user->avatar }}">{{ $user->name }}</option>

																	@endforeach
												
											</select>
											

										</div>
										</div>
										
											<div class="form-group">

												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text"  class="form-control" placeholder="Start Date..."  id="example1" name="start_date">
												</div>
												</div>

													<!-- <div class="col-md-3">
														<div class="input-icon">
															<i class="fa fa-clock-o"></i>
															<input type="text" class="form-control timepicker timepicker-24" name="start_time" id="start_time"> </div>
													</div> -->

											</div>


										<div class="form-group">

													
												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text" class="form-control" placeholder="End Date..."  id="example2" name="duedate">
												</div>
												</div>

														<!-- <div class="col-md-3">
														<div class="input-icon">
															<i class="fa fa-clock-o"></i>
															<input type="text" class="form-control timepicker timepicker-24" name="end_time" id="end_time"> </div>
													</div> -->

													<div id="log" class="col-md-3 f18" style="position: absolute; margin-top: -40px; margin-left: 440px; text-align: center; color: #e67e22;"></div>	
														
									</div>

										

											@if ($errors->has('duedate'))
													<span class="help-block alert alert-danger alert-dismissible" role="alert">
																	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																	</button>
														<strong>{{ $errors->first('duedate') }}</strong>
														</span>
												@endif

									<div class="form-group"></div>
										<!-- End .form-group  -->
										<div class="form-group">
											<div class="col-lg-12">
												<button class="btn blue f12" id="assign_submit">Save</button>
													&nbsp;&nbsp;
												<button class="btn btn-default f12" type="reset" id="clear">Cancel</button>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>


				</div>


</div>


	</div>
							
								

</div>

						
							<!-- Notification  -->
							<div class="message success col-lg-6 pull-right" role="alert" id="success">
										<i class="fa fa-check"></i> &nbsp; &nbsp; Successfuly
								</div>


							
							<div class="message success col-lg-4 pull-right" role="alert" id="resetpass">
										<i class="im-checkmark2 s16"></i> &nbsp; Successfuly reset password    
								</div>

								<div class="message error col-lg-4 pull-right" role="alert" id="error">
										<i class="fa fa-warning f16"></i> &nbsp; Please fill all field   
								</div>
								



						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="border-bottom-2"></div>


						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

		<!-- End #content -->



@stop


@section('javascript')

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 -->
<script type="text/javascript">
	
	$(document).ready(function () {
				
				$(document).on('change', '#example2', function () {

				// var date = moment($("#duedate").val());

				// var momemt = moment(date).endOf('day').fromNow();
				// fromDate = parseInt(new Date($("#example1").val())*1000); 
				fromDate = Date.parse($('#example1').val());

	// toDate = parseInt(new Date($("#example2").val())*1000);

			toDate = Date.parse($('#example2').val());

			// 	fromtime = parseInt($("#start_time").val()); 
			// totime = parseInt($("#end_time").val());
				
				// var timeDiff = (totime - fromtime);
				var dateDiff = (toDate - fromDate);

	var DD = Math.floor(dateDiff / 3600 / 24);
	var formatdate = DD / 1000;
				// var formatted = ((timeDiff < 10) ? ("0" + timeDiff + " Hours") : timeDiff);

					$( "#log" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

				

					
				});
	});

</script>

@stop