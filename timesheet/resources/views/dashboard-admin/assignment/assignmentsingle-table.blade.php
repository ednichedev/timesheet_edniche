	<script type="text/javascript">



$(document).ready(function(){

	<?php for($i=1;$i<150;$i++){ ?>

	$('#myModal<?php echo $i; ?>').on('shown.bs.modal', function () {
 
	
});


<?php }?>

});



</script>

	<table class="table">

		<tbody>
			@php
			$i=1
			@endphp
			@foreach($assignmentsingles as $assignmentsingle)




				<tr>

						<td width="5%">						 
							
							@if($assignmentsingle->completed == 0)

						<form method="POST" action="" id="assign_single_complet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="completed" value="1">
					 <input type="hidden" name="completed_by" value="{{Auth::user()->id }}">

						<input type="checkbox" id="checksingle{{$i}}" data-id="{{ $assignmentsingle->id }}">
						</form>


						@else

						<form method="POST" action="" id="assign_single_uncomplet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="uncompleted" value="0">
					 

					 <input type="checkbox" id="unchecksingle{{$i}}" data-id="{{ $assignmentsingle->id }}" checked="checked">
						</form>


						@endif
						
						</td>

							
						<td>

							<a href="{{route('assignmentsingle.tasksingle', $assignmentsingle->id)}}">

						@if($assignmentsingle->completed == 1)
						<del>
						{{ Str::limit($assignmentsingle->title, 15,'...') }}
						</del>
						@else
						
							{{ Str::limit($assignmentsingle->title, 15,'...') }}

						@endif
						<br>
						</a>
						<span class="f11 color-light">
						Tasks ( {{ $assignmentsingle->tasks->count()}} )
						</span>
						</td>
							
						<td width="4%">
					
					
						<img class="user-avatar popovers" src="/dashboard/assets/img/avatars/{{ $assignmentsingle->user->avatar }}" style="max-width: 18px; border-radius: 100px;"  data-container="body" data-trigger="hover" data-placement="top" data-content="{{ $assignmentsingle->user->name }}">
						
						</td>


						<td width="18%" class="color-light">
						{{ Carbon\Carbon::parse($assignmentsingle->start_date)->format('d-M-Y') }}
						</td>

						<td width="18%" class="color-light">
						{{ Carbon\Carbon::parse($assignmentsingle->duedate)->format('d-M-Y') }}
						</td>

						<td style="color: #e67e22;">
							

						<?php 
							$currentDateTime = date('Y-m-d');
							$difference = strtotime($assignmentsingle->duedate) - strtotime($assignmentsingle->start_date);

								$tdifference = strtotime($assignmentsingle->end_time) - strtotime($assignmentsingle->start_time);

						?>

						@if(floor($difference / (60*60)) < 24)

							{{ $days = floor($tdifference / (60*60) )}} Hours

							@elseif(floor($difference / (60*60)) >= 24)
									{{ $days = floor($difference / (60*60*24) )}} Days

						@endif
					
						</td>

						<td>
																	@if($assignmentsingle->completed == 1)
																			<span class="badge" style="background-color: #2ecc71;">
																			 Completed
																			 </span>

																			@else


																			 <?php
																							 $countAssignCompleted = App\Tasksingle::where('assignment_id', $assignmentsingle->id)->where('completed', 1)->get();
																			 
																				$current = 0;
																				$percent=0;
																				$assignment = $countAssignCompleted->count();
																				$total = $assignmentsingle->tasks->count() * 1000;
																				$current = $assignment * 1000;

																				if ($total!=0) {				 	
																					$percent = ($current /	$total) *100;
																					settype($percent, "integer");	
																				}
																			 
																			 ?>

																			 <span class="badge" style="background-color: #2ecc71;">
																				{{$percent}} % 
																			 </span>


																	@endif
																</td>


						<td width="2%">
									
									@if($assignmentsingle->completed == 0)

									<!-- Modal -->
										<div class="modal fade  bs-example-modal-sm" id="myModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
										  <div class="modal-dialog modal-sm" role="document">
											<div class="modal-content">
											  <div class="modal-header" style="background-color: #e74c3c; color: #FFFFFF; border-top-left-radius: 5px; border-top-right-radius: 5px;">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #FFFFFF;"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="mySmallModalLabel">Delete Confirmation</h4>
											  </div>
											  <div class="modal-body">

												   <form method="POST" action="{{route('assignmentsingle.remove.post')}}">
																{{ csrf_field() }}

																		<input type="hidden" name="assignmentsingle" value="{{$assignmentsingle->id}}" id="assignmentsingle{{$i}}">

																				
																	<button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 140px;">Close</button>
																	&nbsp;
																	<button class="btn btn-primary pull-right" type="submit">Yes</button>
															

														</form>

											  </div>
											  
											  
											</div>
										  </div>
										</div>

										<button class="btn btn-link" style="margin-top: -10px;" data-toggle="modal" data-target="#myModal{{$i}}"><i class="fa fa-close f11"></i></button>
							@else

							@endif

						</td>


					</tr>
					@php
					$i++
					@endphp


		@endforeach
		</tbody>
</table>

