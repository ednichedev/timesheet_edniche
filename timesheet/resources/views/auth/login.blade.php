@extends('layouts.home-app')

   @section('content')

	<!-- Navbar -->
	<nav class="navbar navbar-toggleable-md bg-primary fixed-top navbar-transparent " color-on-scroll="500">
		<div class="container">
			<div class="navbar-translate">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar bar1"></span>
					<span class="navbar-toggler-bar bar2"></span>
					<span class="navbar-toggler-bar bar3"></span>
				</button>
				<a class="navbar-brand" href="http://school.edniche.com" rel="tooltip" data-placement="bottom" target="_blank">
					Edniche English School
				</a>
			</div>
			<div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
				<ul class="navbar-nav">
					<li class="nav-item">
						
					</li>
					<li class="nav-item">
						
					</li>
					<li class="nav-item">
						<a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
							<i class="fa fa-twitter"></i>
							<p class="hidden-lg-up">Twitter</p>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
							<i class="fa fa-facebook-square"></i>
							<p class="hidden-lg-up">Facebook</p>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
							<i class="fa fa-instagram"></i>
							<p class="hidden-lg-up">Instagram</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- End Navbar -->

	<div class="wrapper">
		<div class="page-header clear-filter" filter-color="orange">
			<div class="page-header-image" style="background-image: url('/assets/img/img-header.jpg');">
			</div>
			<div class="container">
				
				<div class="content-center brand" id="brand">
				<div class="hidden-xs">
					<img src="/assets/img/logoedniche.png" alt="" style="width: 350px; margin-bottom: 45px;">

					</div>
					<h1 class="h1-seo">Project Managerment System</h1>
					

					<button class="btn btn-simple btn-round" type="button" id="btnShowLogin">Start Now</button>
					
					<p>&nbsp;</p>
					@if ($errors->has('email'))
								 <span class="help-block" style="padding:10px; border:1px solid #FFF; border-radius: 50px;">
								   <i class="now-ui-icons travel_info" style="color: #F4511E; line-height: 35px;"></i> &nbsp; &nbsp;{{ $errors->first('email') }}
								 </span>
					@endif

					@if ($errors->has('password'))
									<span class="help-block" >
										
									 <strong >{{ $errors->first('password') }}</strong>
									
									</span>
					@endif


				</div>

				<div class="content-center" id="showLogin" style="display: none;">
				<div class="card card-login card-plain col-md-4 ">

					<form class="form-horizontal" role="form" method="POST" action="{{ route('signin') }}">
	  {{ csrf_field() }}

						<div class="header header-primary text-center">
							
						</div>
						<div class="content">
							<div class="input-group form-group-no-border input-lg{{ $errors->has('email') ? ' has-error' : '' }}">
								<span class="input-group-addon">
									<i class="now-ui-icons users_circle-08" style="color: #FFF;"></i>
								</span>
								<input id="email" type="email" class="form-control" placeholder="Email..." name="email" value="{{ old('email') }}" required autofocus style="color: #FFF;">
								
							</div>

							

							<div class="input-group form-group-no-border input-lg{{ $errors->has('password') ? ' has-error' : '' }}">
								<span class="input-group-addon">
									<i class="now-ui-icons ui-1_lock-circle-open" style="color: #FFF;"></i>
								</span>
								<input type="password" id="password" placeholder="Password..." class="form-control" name="password" required style="color: #FFF;">
									
							</div>

						</div>
						
						<div class="footer text-center">
							<button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Login</button>
						</div>

					   
					   <button class="btn btn-icon  btn-icon-mini btn-round btn-simple" type="button" style="font-size: 14px; color: #fff; border-color: #FFF;" id="hideLogin">
						<i class="now-ui-icons arrows-1_minimal-down"></i>
					  </button>
						
					 
					</form>
				</div>
			</div>
			
				
				
			</div>
				
		</div>

		<footer class="footer" style=" margin-top: -70px;">
			<div class="container">
				

				<div class="copyright" style="color: #FFF; text-align: left;">
					&copy;
					<script>
						document.write(new Date().getFullYear())
					</script>
					|  Edniche English School
				</div>

			</div>
		</footer>


	</div>


@stop


