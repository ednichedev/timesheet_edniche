
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">New Assignment</h4>
	  </div>
	  <div class="modal-body">

							<form class="form form-horizontal" role="form" method="POST" action="{{route('projec.addassignment')}}">

									{{ csrf_field() }}


										<div id="project_id"></div>
										<div id="project_duedate"></div>

									<div class="form-group">
											<div class="col-lg-12">
												<input type="text" name="title" class="form-control f12" required autofocus placeholder="Title...">
											</div>
									</div>

									<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
											<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..." id="desc"></textarea>
											</div>
										</div>


										

							<div class="form-group">

									@foreach($teams as $team)

											@foreach($team->projects as $project)
										<input type="hidden" name="project_id" value="{{ $project->id }}">

										<input type="hidden" name="project_duedate" value="{{ $project->duedate }}">

												@endforeach
										@endforeach
										<div class="col-lg-12">

											<label class="color-light f12">Assign to</label>
											<select class="form-control selectMembers f12" name="user_id">
												@foreach($teams as $team)
												@foreach($team->projects as $project)
																@foreach($project->user as $users)
																																	
																	<option value="{{ $users->id }}" data-image="/dashboard/assets/img/avatars/{{ $users->avatar }}">{{ $users->name }}</option>

																	@endforeach
														@endforeach
													@endforeach
											</select>
											

										</div>

										

										</div>

										<div class="form-group">
											
											<div class="col-lg-12">

													<label class="color-light f12">Assignment datline</label>
														<div class="input-group input-medium">
															<input type="date" class="form-control f12" name="start_date" placeholder="Start Date..." required>
															<span class="input-group-addon"> To </span>
															<input type="date" class="form-control f12" name="duedate" placeholder="Duedate..." required> </div>
														
													
											</div>

											</div>
											@if ($errors->has('duedate'))
													<span class="help-block alert alert-danger alert-dismissible" role="alert">
																	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																	</button>
														<strong>{{ $errors->first('duedate') }}</strong>
														</span>
												@endif

									<div class="form-group"></div>
										<!-- End .form-group  -->
										<div class="form-group" style="margin-bottom: 0;">
											<div class="col-lg-12">

											<div class="modal-footer">
							<button type="button" class="btn btn-default f12" data-dismiss="modal">Close</button>
							<button type="submit" class="btn blue f12">Save</button>
						  </div>

											</div>
										</div>
										<!-- End .form-group  -->
									</form>

	  </div>
	  
	</div>
  </div>
</div>