@extends('layouts.main-app')

	 @section('content')



			<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
		 <!-- BEGIN CONTENT BODY -->
		 <div class="page-content">
			<!-- BEGIN PAGE HEADER-->
	
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
			 <ul class="page-breadcrumb">
				
				<li class="page-breadcrumb-deactive">
				 <a href="{{ url('/employee')}}"><span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
				 </a>
				</li>
				&nbsp;
				&nbsp;
				&nbsp;
				<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Project</span>
				</li>

			 </ul>

			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title">
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS 1-->
			<div class="row">

			<div class="portlet light col-xs-12 col-sm-12 col-md-12 col-lg-12">
		 <!--  <p style="margin-top: -25px;">&nbsp;</p> -->


				<table class="table">
					
					@if(count($projects))

					@foreach($projects as $project)

							@foreach($project->user as $users)

								@if($users->id == Auth::user()->id )

								@if($project->completed == 1)

							<tr bgcolor="#E1F5FE">
										@else
							<tr>
						@endif
						<td>
						<span class="f18 block-project">
						<a href="{{ route('project.get.assignmentsproject', $project->id )}}" class="color-light">{{ $project->title}}</a>
						</span>
						<span class="f11 block-project color-light">
						Assignments &nbsp;
									<span class="badge" style="background-color: #2ecc71;">
		   {{ $project->assignmentprojects->count()}}
		  </span>
						</span>
						</td>
						<td>
						<span class="project_image">
		   <img class="user-avatar" src="/dashboard/assets/img/teams/avatar/{{ $project->teams->avatar }}" style="max-width: 20px; border-radius:50px;">
		   <span class="f12 color-light">{{ $project->teams->title}}
		 </span>
		 </td>

					<td width="25%">Started &nbsp;{{ Carbon\Carbon::parse($project->start_date)->format('d-M-Y') }}&nbsp; | &nbsp; 
		 Ends &nbsp;{{ Carbon\Carbon::parse($project->duedate)->format('d-M-Y') }}
	  </td>

	  <td>

	  @if($project->completed == 1)
				
				<span class="badge" style="background-color: #2ecc71;">
					 Completed
					 </span>

	  @else
		
		<?php

		 $countAssignCompleted = App\Assignmentproject::where('projects_id', $project->id)->where('completed', 1)->get();
				 
					$current = 0;
					$percent=0;
					$assignment = $countAssignCompleted->count();
					$total = $project->assignmentprojects->count() * 1000;
					$current = $assignment * 1000;
					if ($total!=0) {				 	
						$percent = ($current /	$total) *100;
						settype($percent, "integer");	
					}
				 
				 ?>


				<div class="col-md-12">
				 <div class="progress">
									
											<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" style="width:{{	$percent }}%">
												<span class="sr-only"></span>
												{{	$percent }} %
											</div>
								</div>
					</div>

					@endif

	  </td>

					</tr>

					@endif



					@endforeach

					@endforeach

					
				</table>


<div class="pagination pull-right">
	{{ $projects->links() }}
</div>

@else

@endif

			 </div>


			</div>


			<div class="clearfix"></div>
			<!-- END DASHBOARD STATS 1-->
			<div class="row">
			</div>
			
		 
		 </div>
		 <!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->  


@stop