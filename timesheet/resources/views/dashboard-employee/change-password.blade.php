<!DOCTYPE html>

<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title>Metronic Admin Theme #1 | User Lock Screen 2</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
		<meta content="" name="author" />
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->

		<!-- END THEME LAYOUT STYLES -->
		<link rel="shortcut icon" href="favicon.ico" /> </head>
	<!-- END HEAD -->
		<style type="text/css">
			body{
				background-color: #ecf0f1;
			background-image: url('/assets/img/img-header.jpg');
				background-repeat:no-repeat;
			}

			.bd-panel{
				margin-top: 220px;
			}
			.thumbnail{
				margin: 0 auto;
				border-radius: 100px;
				width: 120px;
				height: 120px;
			}
			.form-inline{
				margin-top: 10px;
			}

		.style-4 input[type="password"] {
  padding: 10px;
  border: none;
  border-bottom: solid 2px #c9c9c9;
  transition: border 0.3s;
			}
			.style-4 input[type="password"]:focus,
			.style-4 input[type="password"].focus {
			  border-bottom: solid 2px #969696;
			}
			h3{
				margin: 0 auto;
				margin-bottom: 30px;
				color: #e67e22;
				text-align: center;
			}

		</style>


	<body class="">


<div class="container-fluid">
	<div class="row">
			
			
		<div class="col-md-12">
			
				<div class="col-md-6 col-md-offset-3 bd-panel">
				<h3>Please change your password to login</h3>
						<div class="panel panel-default">
							  
							  <div class="panel-body">
								  <div class="col-xs-12 col-md-3">
											<div class="thumbnail">
											  <img src="/dashboard/assets/img/avatars/{{Auth::user()->avatar}}" alt="...">
											</div>
										  </div>

										  <div class="col-xs-12 col-md-9">
												<div class="row">
														<h4>{{Auth::user()->name}}</h4>

														<h6> {{Auth::user()->email}}</h6>
												</div>

												<div class="row">
													
													<form class="form-inline" method="POST" action="{{ route('change_password') }}">
																{{ csrf_field() }}

																<input type="hidden" name="user_id" value="{{Auth::user()->id}}">


													
																	  <div class="form-group style-4">
																	   
																		<input type="password" class="form-control" placeholder="New Password" name="new_password" required>
																	  </div>
																	 &nbsp;
																	  <button type="submit" class="btn btn-default">Save</button>
																	</form>

												</div>

										  </div>

							  </div>
							</div>

							
				</div>

			
		</div>

	</div>
</div>



	<!-- BEGIN CORE PLUGINS -->
		<script src="/dashboard/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="/dashboard/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		
		<!-- END CORE PLUGINS -->
		
	</body>

</html>