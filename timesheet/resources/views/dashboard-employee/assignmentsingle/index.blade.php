@extends('layouts.main-app')

	 @section('content')



			<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
		 <!-- BEGIN CONTENT BODY -->
		 <div class="page-content">
			<!-- BEGIN PAGE HEADER-->
	
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
			 <ul class="page-breadcrumb">
				
				<li class="page-breadcrumb-deactive">
				 <a href="{{ url('/employee')}}"><span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
				 </a>
				</li>
				&nbsp;
				&nbsp;
				&nbsp;
				<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;My Asssignments</span>
				</li>

			 </ul>

			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title">
				
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS 1-->
	<div class="row">

<div class="col-md-6">

	<div class="col-lg-12 border-blobk">

				<div class="col-lg-6">
								

				</div>

				<div class="col-lg-3">

				<small class="f16 pull-right" style="text-align: center; margin-left: -50px;">
				
				<div class="circle" style=" margin-top: -5px;">
				 <span id="show_left">
				 
					<input type="text" value="{{ $countleft->count()}}" id="project_assignsingle_left" class="input_border_less">
					
				 </span>

				 </div>
				 <h5 class="f12 color-light" style="margin-top: 5px; margin-bottom: -10px; margin-left: 40px;">Assignments Left</h5>
				 

				</small>
				 
				</div>

				<div class="col-lg-3">

				<small class="f16 pull-left" style="text-align: center; margin-left: 20px;">
				
				<div class="circle-complet" style=" margin-top: -5px;">
				 <span id="show_completed">
				 
					<input type="text" value=" {{ $countCompleted->count() }}" id="project_assignsingle_complete" class="input_border_less">
					
				 </span>

				 </div>
				 <h5 class="f12 color-light" style="margin-top: 5px; margin-bottom: -10px; margin-left: 55px;">Completed</h5>
				 
				 <div class="circlecircle hidden">
				 <i class="fa fa-check f20" style="color:#2ecc71;"></i>
				 </div>

				

				</small>

				</div>


		 </div>
		 <!-- end col 12 -->


<div class="portlet light col-xs-12 col-sm-12 col-md-12 col-lg-12">
		 <!--  <p style="margin-top: -25px;">&nbsp;</p> -->


<table class="table table-borderless">
					
		<tbody>
			 @php
					 $i=1
			 @endphp

		@foreach($assignmentsingles as $assignmentsingle)
						<tr>
							
							<!-- <td width="2%">

					@if($assignmentsingle->completed == 0)

						<form method="POST" action="" id="assignsingle_complet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="completed" value="1">
					 
						<input type="checkbox" id="checksingle{{$i}}" data-id="{{ $assignmentsingle->id }}">
						</form>

						@else
								
						<form method="POST" action="" id="assignsingle_uncomplet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="uncompleted" value="0">
					 <input type="checkbox" id="unchecksingle{{$i}}" data-id="{{ $assignmentsingle->id }}" checked="checked">
						</form>


						@endif

			</td> -->

				<td>
				<a href="{{route('myassignment_employee.tasksingle', $assignmentsingle->id)}}">
				<span class="f12">{{ Str::limit($assignmentsingle->title, 80,'...') }}</span>
				</a>
				</td>

				</td>

				<td width="5%">
					<span class="badge pull-right" style="background-color: #2ecc71;">{{$assignmentsingle->tasks->count()}}</span>
				</td>

				<td width="3%">
					<button class="btn btn-link f11 popovers pull-right" data-container="body" data-trigger="hover" data-placement="top" data-content="New Task" aria-expanded="false" data-toggle="modal" data-target="#myTaskSingleModal" data-assign-id="{{$assignmentsingle->id}}" data-user-id="{{$assignmentsingle->user->id}}" data-ass-date="{{$assignmentsingle->duedate}}" id="add-task{{$i}}"><i class="fa fa-plus"></i></button>
				 </td>

			 <td width="10%">
			 <span class="f11 color-light">
							By&nbsp;
				</span>

			 <img class="user-avatar popovers" src="/dashboard/assets/img/avatars/{{ $assignmentsingle->creatBys->avatar }}" style="max-width: 18px; margin-top: -2px; border-radius: 100px;" data-container="body" data-trigger="hover" data-placement="top" data-content="{{ $assignmentsingle->creatBys->name }}">

			 </td>


				<td width="15%">
					<span class="f12 color-light pull-right">{{ Carbon\Carbon::parse($assignmentsingle->start_date)->format('d-M-Y') }}</span>
			 </td>

			 <td width="15%">
					<span class="f12 color-light pull-right">{{ Carbon\Carbon::parse($assignmentsingle->duedate)->format('d-M-Y') }}</span>
			 </td>


			 <td width="10%" id="completed{{$i}}">
				 <div>
					@if($assignmentsingle->completed == 1)
					 <span class="badge" style="background-color: #2ecc71;">
					 Completed
					 </span>

					 @else

					 <?php

						  $countTaskCompleted = App\Tasksingle::where('assignment_id', $assignmentsingle->id)->where('completed', 1)->get();
					 
							$current = 0;
										$percent=0;
										$tasks = $countTaskCompleted->count();
										$total = $assignmentsingle->tasks->count() * 1000;
									 
										$current = $tasks * 1000;
										if ($total!=0) {				 	
											$percent = ($current /	$total) *100;
											settype($percent, "integer");	
										}
				 
					?>


					<span class="badge pull-right" style="background-color: #2ecc71;">{{$percent}} %</span>
				

					 @endif
					</div>
			 </td>

			 <td width="5%">
					@if($assignmentsingle->create_by == Auth::user()->id)
						
					 <form class="form form-horizontal" role="form" method="POST" action="{{route('myassignment_employee.deleteassign')}}">
						{{ csrf_field() }}
					 <input type="hidden" name="assign_id" value="{{ $assignmentsingle->id }}">
					 <button class="btn btn-link" style="margin-top: -8px;"><i class="fa fa-close"></i></button>
					 
						</form>
					 @endif
				 </td>


						</tr>

							@php
								$i++
							@endphp

						@endforeach

 
					</tbody>
					

					
				</table>

 <div class="col-lg-12 block-project">
			 <div class="col-lg-4 pull-left">
			 <button class="btn btn-link f12 pull-left" data-toggle="modal" data-target="#myAssigSingleModal" style="margin-left: -25px; margin-top: 10px; margin-bottom: 10px;"><i class="fa fa-plus"></i> &nbsp; New Assignment</button>
				
			 </div>
 
		 </div>



<div class="pagination pull-right">
	{{ $assignmentsingles->links() }}
</div>


			 </div>


</div>


<div class="col-md-6 border-left">

	<div class="col-lg-12 border-blobk">

				<div class="col-lg-6">
							<h6 class="f16 color-light" style="margin-top: 28px;">Assignment of Project</h6>
			
				</div>

				<div class="col-lg-3">

				
				 
				</div>

				<div class="col-lg-3">

			

				</div>


		 </div>
		 <!-- end col 12 -->


<div class="portlet light col-xs-12 col-sm-12 col-md-12 col-lg-12">
		 <!--  <p style="margin-top: -25px;">&nbsp;</p> -->



<table class="table table-borderless">
	

		<tbody>

	@php
		$i=1
	@endphp


	@foreach($projects as $project)

		@foreach($project->user as $member)

		@if($member->id == Auth::user()->id )

	<tr style="background-color: #E1F5FE; color: #34495e;">
			<td colspan="4">{{ Str::limit($project->title, 30,'...') }}</td>
			<td>{{Carbon\Carbon::parse($project->start_date)->format('d-M-Y')}}</td>
			<td> {{ Carbon\Carbon::parse($project->duedate)->format('d-M-Y') }}</td>

			<td width="2%">
				<button class="btn btn-link f11 popovers pull-right" data-container="body" data-trigger="hover" data-placement="top" data-content="New Assignment" aria-expanded="false" data-toggle="modal" data-target="#myAssigProModal" value="{{$project->id}}" data-project-date="{{$project->duedate}}" id="add-assig-pro{{$i}}" style="margin-top: -5px;"><i class="fa fa-plus"></i></button>
			</td>


		</tr>


		@php 
		$assign_count =1
		@endphp

	@foreach($project->assignmentprojects->slice(0, 5) as $assignmentproject)

	@if($assignmentproject->users_id == Auth::user()->id )
			
		<tr>
		
		<td>
		<span class="color-light f12">{{ Str::limit($assignmentproject->title, 20,'...') }}</span>
		</td>

		<td></td>
		<td></td>


<td>
<span class="f11 color-light">
								Assign by
						</span>
						<img class="user-avatar popovers pull-right" data-container="body" data-trigger="hover" data-placement="top" data-content="{{ $assignmentproject->creatBys->name }}" aria-expanded="false" src="/dashboard/assets/img/avatars/{{ $assignmentproject->creatBys->avatar }}" style="max-width: 18px; border-radius: 100px;">

			</td>

			<td>
			<span class="f11 color-light pull-right">{{Carbon\Carbon::parse($assignmentproject->start_date)->format('d-M-Y') }}</span>
			</td>
			<td>
			<span class="f11 color-light pull-right">{{Carbon\Carbon::parse($assignmentproject->duedate)->format('d-M-Y') }}</span>
			</td>
			<td>
				<span class="badge" style="background-color: #2ecc71;">{{$assignmentproject->tasks->count()}}</span>
			</td>

		</tr>

		@php 
		$assign_count++;
		@endphp
		@endif
		@endforeach

		<tr>
			<td colspan="3">
				<a href="{{ route('project.get.assignmentsproject', $project->id )}}" class="btn btn-link f11" style="margin-left: -15px">
					<i class="fa fa-plus"></i> &nbsp; New Tasks
				</a>
			</td>

			<td>
					<a href="{{ route('project.get.assignmentsproject', $project->id )}}" class="btn btn-link f11" style="margin-left: -15px">
					<i class="fa fa-eye"></i> &nbsp; View More
				</a>
			</td>

			<td></td>
			<td></td>
			<td></td>
		</tr>

						
			@php
				$i++
			@endphp
											
		@endif
		@endforeach
	@endforeach




 
</tbody>
					

					
				</table>
							


</div>

@include('dashboard-employee.assignmentsingle.add-assig-pro')

@include('dashboard-employee.assignmentsingle.add-assig-single')

@include('dashboard-employee.assignmentsingle.add-task')


</div>


</div>


			<div class="clearfix"></div>
			<!-- END DASHBOARD STATS 1-->
			<div class="row">
			</div>
			
		 
		 </div>


		 <!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->  


@stop

@section('javascript')

<script type="text/javascript">


$(document).ready(function () {
				
	
	$(document).on('change', '#end_time', function () {

		fromDate = Date.parse($('#example1').val());

	toDate = Date.parse($('#example2').val());

	fromtime = parseInt($("#start_time").val());

	totime = parseInt($("#end_time").val());
				
				var timeDiff = (totime - fromtime);
				var dateDiff = (toDate - fromDate);

	var DD = Math.floor(dateDiff / 3600 / 24);
		var formatdate = DD / 1000;
				var formatted = ((timeDiff < 10) ? ("0" + timeDiff + " Hours") : timeDiff);

				if (toDate > fromDate) {

					$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

				}else{

						$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatted+"</div>" );

				}

					
				});

	});

$(document).ready(function () {
				
			$(document).on('change', '#example6', function () {


				fromDate = Date.parse($('#example5').val());

				toDate = Date.parse($('#example6').val());

				var dateDiff = (toDate - fromDate);

				var DD = Math.floor(dateDiff / 3600 / 24);
				var formatdate = DD / 1000;
			

					$( "#log" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

					
				});
	});

$(document).ready(function () {

	<?php


			foreach($projects as $project){

							$count = $project->count();
			}

	?>


<?php for($i=1;$i<=1000;$i++){ ?>
	
$(document).on('click', '#assig_user<?php echo $i ?>', function(){
	var id = $('#assig_user<?php echo $i ?>').attr("data-value");

	$( "input[name~='user_id']" ).val(id);

});


$(document).on('click', '#leader_id<?php echo $i ?>', function(){
	var id = $('#leader_id<?php echo $i ?>').attr("data-value");

	$( "input[name~='user_id']" ).val(id);

});



	<?php }?>

	});


	$(document).ready(function () {

			<?php
			$count = $projects->count();

			?>

<?php for($i=1;$i<=1000;$i++){ ?>



$(document).on('click', '#add-assig-pro<?php echo $i ?>', function(){

			var id = $('#add-assig-pro<?php echo $i ?>').val();
			
			var ass_project = $('#add-assig-pro<?php echo $i ?>').attr("data-project-date");

			$('#myAssigProModal').on('shown.bs.modal', function () {

				$( "input[name~='project_id']" ).val(id);

				$( "input[name~='project_duedate']" ).val(ass_project);


		 });



		});

	<?php }?>

	});



	/// Add Assignment Single

	$(document).ready(function () {
				
			$(document).on('change', '#example8', function () {


				fromDate = Date.parse($('#example7').val());

				toDate = Date.parse($('#example8').val());

				var dateDiff = (toDate - fromDate);

				var DD = Math.floor(dateDiff / 3600 / 24);
				var formatdate = DD / 1000;
			

					$( "#log-single" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

					
				});
	});


	///Tasks 	

$(document).ready(function () {
			
	
	$(document).on('change', '#end_time', function () {

		fromDate = Date.parse($('#example1').val());

	toDate = Date.parse($('#example2').val());

	fromtime = parseInt($("#start_time").val());

	totime = parseInt($("#end_time").val());
				
				var timeDiff = (totime - fromtime);
				var dateDiff = (toDate - fromDate);

	var DD = Math.floor(dateDiff / 3600 / 24);
		var formatdate = DD / 1000;
				var formatted = ((timeDiff < 10) ? ("0" + timeDiff + " Hours") : timeDiff);

				if (toDate > fromDate) {

					$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

				}else{

						$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatted+"</div>" );

				}

					
				});

	});


// Add Task

$(document).ready(function () {

			<?php
		

			foreach($assignmentsingles as $assignmentsingle){

							$count = $assignmentproject->count();
			}


	?>

<?php for($i=1;$i<=$count;$i++){ ?>



$(document).on('click', '#add-task<?php echo $i ?>', function(){

			var id = $('#add-task<?php echo $i ?>').attr("data-assign-id");

			var ass_date = $('#add-task<?php echo $i ?>').attr("data-ass-date");
			var user_id = $('#add-task<?php echo $i ?>').attr("data-user-id");

			$('#myTaskSingleModal').on('shown.bs.modal', function () {

				$( "input[name~='assignment_id']" ).val(id);
				$( "input[name~='user_id']" ).val(user_id);
				$( "input[name~='assignment_duedate']" ).val(ass_date);


		 });



		});

	<?php }?>

	});



</script>

@stop