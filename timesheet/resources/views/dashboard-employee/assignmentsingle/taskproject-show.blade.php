@extends('layouts.main-app')

	 @section('content')



			<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
		 <!-- BEGIN CONTENT BODY -->
		 <div class="page-content">
			<!-- BEGIN PAGE HEADER-->
	
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
			 <ul class="page-breadcrumb">
				
				<li class="page-breadcrumb-deactive">
				 <a href="{{ url('/teamleader')}}"><span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
				 </a>
				</li>
				&nbsp;
				&nbsp;
				&nbsp;
				<li class="page-breadcrumb-deactive">
				<a href="{{ url('/employee/myassignment_employee')}}">
					<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;My Assignment</span>
					</a>
				</li>

			
				&nbsp;
				&nbsp;
				&nbsp;
				<li class="">
				
					<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Tasks</span>
					
				</li>

			 </ul>

			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title">
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS 1-->
			<div class="row">

			<div class="portlet light col-xs-12 col-sm-12 col-md-12 col-lg-12">

					

					<p style="margin-top: -25px;">&nbsp;</p>

			
@php
$count =1
@endphp

	@foreach($assignprojects as $assignproject)

	 <div class="row block-row">

		 <div class="col-lg-12 border-blobk">

				<div class="col-lg-3">

				 <span class="f18 color-light block-project">
				 {{ $assignproject->title}}
				 </span>

				 <span class="f11 block-project color-light">
					Started &nbsp;{{ Carbon\Carbon::parse($assignproject->start_date)->format('d-M-Y') }}&nbsp; | &nbsp; 
				 Ends &nbsp;{{ Carbon\Carbon::parse($assignproject->duedate)->format('d-M-Y') }}
				 </span>

				 

				</div>

				<div class="col-md-3">

				<span class="f14 color-light block-project">
					Description
				 </span>

				 <span class="f11 block-project color-light">
					<p>{{ $assignproject->desc}}</p>
				 </span>
					
				</div>

				<div class="col-md-2">

				 <span class="f14 color-light block-project">
					
				 </span>

				 <?php
				 
	  $current = 0;
					$percent=0;
					$tasks = $countCompleted->count();
					$total = $assignproject->tasks->count() * 1000;
					$current = $tasks * 1000;
					if ($total!=0) {				 	
						$percent = ($current /	$total) *100;
						settype($percent, "integer");	
					}
				 
				 ?>

				 <div class="progress">
									
											<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" style="width:{{$percent}}%">
												<span class="sr-only"></span>
												{{$percent}} %
											</div>
								</div>

				
					
				</div>

				<div class="col-lg-2">

				<small class="f16 pull-right" style="text-align: center;">
				
				<div class="circle" style=" margin-top: -5px;">
				 <span id="show_left">
				 
					<input type="text" value="{{ $countleft->count()}}" id="project_assign_left" class="input_border_less">
					
				 </span>

				 </div>
				 <h5 class="f12 color-light" style="margin-top: 5px; margin-bottom: -10px; margin-left: 40px;">Assignments Left</h5>
				 

				</small>
				 
				</div>

				<div class="col-lg-2">

				<small class="f16 pull-right" style="text-align: center;">
				
				<div class="circle-complet" style=" margin-top: -5px;">
				 <span id="show_completed">
				 
					<input type="text" value=" {{ $countCompleted->count() }}" id="project_assign_complete" class="input_border_less">
					
				 </span>

				 </div>
				 <h5 class="f12 color-light" style="margin-top: 5px; margin-bottom: -10px; margin-left: 55px;">Completed</h5>
				 
				 <div class="circlecircle hidden">
				 <i class="fa fa-check f20" style="color:#2ecc71;"></i>
				 </div>

				

				</small>

				</div>


		 </div>
		 <!-- end col 12 -->

		 <div class="col-lg-12 member-block">
			<table class="table table-borderless">

			 <tbody>

			 @php
			 $i=1
			 @endphp

			@if(count($assignproject->tasks))

			 @foreach($assignproject->tasks as $task)


				<tr>
				<td width="2%">


					@if($task->completed == 0)

						<form method="POST" action="" id="tasks_empolyee_complet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="completed" value="1">
					 <input type="hidden" name="completed_by" value="{{Auth::user()->id }}">

						<input type="checkbox" id="checkemployee{{$i}}" data-id="{{ $task->id }}">
						</form>


						@else

						<form method="POST" action="" id="tasks_employee_uncomplet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="uncompleted" value="0">
					 

					 <input type="checkbox" id="uncheckemployee{{$i}}" data-id="{{ $task->id }}" checked="checked">
						</form>


						@endif


				</td>
				 
				 <td><span class="f12">{{ Str::limit($task->title, 80,'...') }}</span>

							

				 </td>

				 
				 <td width="2%"> <img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $task->user->avatar }}" style="max-width: 18px; border-radius: 100px;"></td>
				 <td width="10%">
				 <span class="f11 color-light">{{ $task->user->name }}</span>
				 </td>
				 <td width="6%">
				 <span class="f11 color-light">
								Assign by
						</span>
						</td>
				 <td width="2%"> <img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $task->creatBys->avatar }}" style="max-width: 18px; border-radius: 100px;"></td>

					 <td width="10%">
						 <span class="f11 color-light">
						 {{ $task->creatBys->name }}
						 </span>
					 </td>

				 <td width="10%">
					<span class="f12 color-light pull-left">{{ Carbon\Carbon::parse($task->start_date)->format('d-M-Y') }}</span>
				 </td>
				

				 <td width="10%">
					<span class="f12 color-light pull-left">{{ Carbon\Carbon::parse($task->duedate)->format('d-M-Y') }}</span>
				 </td>

				 <td width="15%" id="completed{{$i}}">
				 <div>
					@if($task->completed == 1)
					 <span class="badge" style="background-color: #2ecc71;">
					 Completed
					 </span>

					 @else

					@endif
									
				 </td>


					<td>

					<div class="btn-group" role="group">
					  <button type="button" class="btn btn-xs btn-default" aria-expanded="false" data-toggle="modal" data-target="#myNote"  value="{{ $task->id }}" id="note{{$i}}"><i class="fa fa-pencil"></i>&nbsp; Notes</button>
					  <button type="button" class="btn btn-xs btn-success" aria-expanded="false" data-toggle="modal" data-target="#showNote{{$i}}">{{$task->noteSingles->count()}}</button>
					  
					</div>

					@include('dashboard-employee.assignmentsingle.show-note')

				</td>
				

				

				 <td width="5%">

						@if($task->completed == 0)

					 <!-- Modal -->
										<div class="modal fade  bs-example-modal-sm" id="myModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
										  <div class="modal-dialog modal-sm" role="document">
											<div class="modal-content">
											  <div class="modal-header" style="background-color: #e74c3c; color: #FFFFFF; border-top-left-radius: 5px; border-top-right-radius: 5px;">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #FFFFFF;"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="mySmallModalLabel">Delete Confirmation</h4>
											  </div>
											  <div class="modal-body">

												   <form method="POST" action="{{route('myassignment_employee.deletetask')}}">
																{{ csrf_field() }}

																		<input type="hidden" name="task_id" value="{{$task->id}}">

																				
																	<button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 140px;">Close</button>
																	&nbsp;
																	<button class="btn btn-primary pull-right" type="submit">Yes</button>
															

														</form>

											  </div>
											  
											  
													</div>
										  </div>
										</div>

							<button class="btn btn-link" style="margin-top: -10px;" data-toggle="modal" data-target="#myModal{{$i}}"><i class="fa fa-close f11"></i></button>

							@else

					 @endif
				 </td>

				</tr> 

			 @php
				$i++
			 @endphp

			 @endforeach

			@endif

			 <tr>

				<td colspan="2"> 

				 <button class="btn btn-link f11" aria-expanded="false" data-toggle="modal" data-target="#myTaskSingleModal"  value="{{ $assignproject->id }}" data-user-id="{{ $assignproject->user->id }}" data-ass-date="{{$assignproject->duedate}}" id="add-task"><i class="fa fa-plus"></i> &nbsp; New Tasks</button>

				 </td>
			 </tr>



			 </tbody>
			 
			</table>
			
		 </div>

	 </div>
<!--  end row -->            


	 @php
		$count++
	 @endphp

 @endforeach


			 </div>

		@include('dashboard-employee.assignmentsingle.add-task')
	
		@include('dashboard-employee.assignmentsingle.add-note')

			</div>


			<div class="clearfix"></div>
			<!-- END DASHBOARD STATS 1-->
			<div class="row">
			</div>
			
		 
		 </div>
		 <!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->  


@stop

@section('javascript')

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 -->
<script type="text/javascript">
	

	$(document).on('change', '#end_time', function () {

				// var date = moment($("#duedate").val());

				// var momemt = moment(date).endOf('day').fromNow();
				// fromDate = parseInt(new Date($("#example1").val())*1000); 
				fromDate = Date.parse($('#example1').val());

	// toDate = parseInt(new Date($("#example2").val())*1000);

	toDate = Date.parse($('#example2').val());

				fromtime = parseInt($("#start_time").val()); 
	totime = parseInt($("#end_time").val());
				
				var timeDiff = (totime - fromtime);
				var dateDiff = (toDate - fromDate);

	var DD = Math.floor(dateDiff / 3600 / 24);
		var formatdate = DD / 1000;
				var formatted = ((timeDiff < 10) ? ("0" + timeDiff + " Hours") : timeDiff);

				if (toDate > fromDate) {

					$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

				}else{

						$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatted+"</div>" );

				}

					
				});






	$(document).ready(function(){


$(document).on('click', '#add-task', function(){

			var id = $('#add-task').val();

			var user_id = $('#add-task').attr("data-user-id");
			var ass_date = $('#add-task').attr("data-ass-date");

		$('#myTaskSingleModal').on('shown.bs.modal', function () {

				$( "input[name~='assignment_id']" ).val(id);

				$( "input[name~='user_id']" ).val(user_id);

				$( "input[name~='assignmentpro_duedate']" ).val(ass_date);
				

		 });



});



});



$(document).ready(function() {

	 $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}}); 
	 
	 $.ajaxSetup({cache: false });

});

	
	$(document).ready(function () {
	
	<?php
		foreach($assignprojects as $assignproject){

			$count = $assignproject->tasks->count();

			} 
	?>

	<?php for($i=1;$i<=$count;$i++){ ?>

	$(document).on('change', '#checkemployee<?php echo $i; ?>', function() {

		var id = $("#checkemployee<?php echo $i; ?>").attr("data-id");


		var completed =  parseFloat($("#project_assign_complete").val());

		var left = parseFloat($("#project_assign_left").val()); 

		var num = 1;

		var cal_completed = completed + num;

		var cal_left = left - num;



				$.ajax({
					
						url: "/employee/myassignment_employee/task/completed/"+id,
						type: 'POST',
						data: $('#tasks_empolyee_complet_form').serialize(), 
						dataType: 'json',
						cache: false,
						success: function(data){

							
							//window.location.reload(true);


							$("#show_completed").html("<input type='text' value="+cal_completed+" id='project_assign_complete' class='input_border_less'>");

							$("#show_left").html("<input type='text' value="+cal_left+" id='project_assign_left' class='input_border_less'>");

							$("#completed<?php echo $i; ?>").html("<span class='badge' style='background-color: #2ecc71;'>Completed</span>");
							
							window.location.reload(true);

						}

				});

				event.preventDefault();


	});

	<?php }?>

});


// Uncompleted assignment

$(document).ready(function () {
	<?php
		foreach($assignprojects as $assignproject){

			$count = $assignproject->tasks->count();

			} 
	?>

	<?php for($i=1;$i<=$count;$i++){ ?>

	$(document).on('change', '#uncheckemployee<?php echo $i; ?>', function() {

		var id = $("#uncheckemployee<?php echo $i; ?>").attr("data-id");
		
		var completed =  parseFloat($("#project_assign_complete").val());

		var left = parseFloat($("#project_assign_left").val()); 

		var num = 1;

		var cal_completed = completed - num;

		var cal_left = left + num;

		

				$.ajax({
					
						url: "/employee/myassignment_employee/task/uncompleted/"+id,
						type: 'POST',
						data: $('#tasks_employee_uncomplet_form').serialize(), 
						dataType: 'json',
						cache: false,
						success: function(data){
							
							//window.location.reload(true);


							$("#show_completed").html("<input type='text' value="+cal_completed+" id='project_assign_complete' class='input_border_less'>");

							$("#show_left").html("<input type='text' value="+cal_left+" id='project_assign_left' class='input_border_less'>");

							$("#completed<?php echo $i; ?>").html("<span class='badge' style='background-color: #2ecc71;'></span>");

							window.location.reload(true);

								 

						}
				});

				event.preventDefault();


	});

	<?php }?>



});



$(document).ready(function() {

	<?php
		foreach($assignprojects as $assignproject){

			$count = $assignproject->tasks->count();

			} 
	?>

	<?php for($i=1;$i<=$count;$i++){ ?>

		$('#note<?php echo $i;?>').on('click', function(){
				var task_id = $(this).val();
				
			$("input[name~='task_id']" ).val(task_id);


		});

	<?php }?>

	
});


</script>

@stop