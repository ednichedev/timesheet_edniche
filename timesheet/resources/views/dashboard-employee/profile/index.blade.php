@extends('layouts.main-app')

   @section('content')

					<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li>
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
								</li>
							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h5 class="f18"> 
				
						</h5>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
					<div class="row">
							<div class="col-md-12">
								<!-- BEGIN PROFILE SIDEBAR -->

								<!-- END BEGIN PROFILE SIDEBAR -->
								<!-- BEGIN PROFILE CONTENT -->
								<div class="profile-content">
									<div class="row">
										<div class="col-md-12">
											<div class="portlet light ">
												<div class="portlet-title tabbable-line">

													<div class="caption caption-md">
														<i class="icon-globe theme-font hide"></i>
														<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
													</div>
													<ul class="nav nav-tabs">
														<li class="active">
															<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
														</li>
														<li>
															<a href="#tab_1_2" data-toggle="tab">Change Profile Picture</a>
														</li>
														<li>
															<a href="#tab_1_3" data-toggle="tab">Change Password</a>
														</li>
														
													</ul>
												</div>
												<div class="portlet-body">
													<div class="tab-content">
														<!-- PERSONAL INFO TAB -->
														<div class="tab-pane active" id="tab_1_1">
																		
																		<div class="col-md-12">
																				 <div class="col-xs-6 col-md-2">
																			<span class="thumbnail" style=" border-radius: 100px; width:110px; height: 110px;">
																			  <img src="/dashboard/assets/img/avatars/{{ Auth::user()->avatar }}" style="width:100px; height: 100px; border-radius: 100px;">
																			</span>
																		  </div>

																		  <div class="col-md-10">
																				<h3 style="margin-top: 20px;">{{ Auth::user()->name }}</h3><br>

																				<h4 style=" margin-top: -20px;" class="color-light">{{ Auth::user()->email }}</h4>

																		  </div>

																		</div>

														</div>
														<!-- END PERSONAL INFO TAB -->
														<!-- CHANGE AVATAR TAB -->
														<div class="tab-pane" id="tab_1_2">
															
															<form action="{{route('employee.profile.post', Auth::user()->id)}}" role="form" method="POST" enctype="multipart/form-data">
															{{ csrf_field() }}

																<div class="form-group">
																	<div class="fileinput fileinput-new" data-provides="fileinput">
																		<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																			<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
																		<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
																		<div>
																			<span class="btn default btn-file">
																				<span class="fileinput-new"> Select image </span>
																				<span class="fileinput-exists"> Change </span>
																				<input type="file" name="avatars"> </span>
																			<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
																		</div>
																	</div>
																	<div class="clearfix margin-top-10">
																		
																	</div>
																</div>
																<div class="margin-top-10">
																	
																	<button type="submit" class="btn green" >Submit</button>
																	
																</div>
															</form>
														</div>
														<!-- END CHANGE AVATAR TAB -->
														<!-- CHANGE PASSWORD TAB -->
														<div class="tab-pane col-md-6" id="tab_1_3">

															<form action="{{ route('employee.profile.changepassword', Auth::user()->id) }}" method="POST" role="form">

															{{ csrf_field() }}

																<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
																	<label class="control-label">New Password</label>
																	<input type="password" class="form-control" name="password" required> 

																		@if ($errors->has('password'))
																			<span class="help-block">
																				<strong>{{ $errors->first('password') }}</strong>
																			</span>
																		@endif

																	</div>

																<div class="form-group">
																	<label class="control-label">Re-type New Password</label>
																	<input type="password" class="form-control" name="password_confirmation" required> 

																	

																	</div>
																<div class="margin-top-10">
									
																	<button type="submit" class="btn green">Change Password</button>
																	
																</div>
															</form>

														</div>
														<!-- END CHANGE PASSWORD TAB -->

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- END PROFILE CONTENT -->
							</div>
						</div>
						
						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		

@stop

