<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>PMS | Erorr 404</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for 404 page option 3" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/dashboard/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="/dashboard/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- END THEME GLOBAL STYLES -->

        <link rel="shortcut icon" href="favicon.ico" />
        <style type="text/css">
            
            #fof{display:block; width:100%; margin:100px 0; line-height:1.6em; text-align:center;}
            #fof .hgroup{text-transform:uppercase;}
            #fof .hgroup h1{margin-bottom:25px; font-size:80px; color: #bdc3c7;}
            #fof .hgroup h1 span{display:inline-block; margin-left:5px; padding:2px; border:1px solid #ecf0f1; overflow:hidden;}
            #fof .hgroup h1 span strong{display:inline-block; padding:0 20px 20px; border:1px solid #ecf0f1; font-weight:normal;}
            #fof .hgroup h2{font-size:60px; color: #bdc3c7;}
            #fof .hgroup h2 span{display:block; font-size:30px; color: #bdc3c7;}
            #fof p{margin:25px 0 0 0; padding:0; font-size:16px;}
            #fof p:first-child{margin-top:0;}
            #fof p a{color: #e67e22;}

        </style>

         </head>
    <!-- END HEAD -->

    <body class="">
        
        <div class="wrapper row2">
          <div id="container" class="clear">

            <section id="fof" class="clear">
             
              <div class="hgroup">
                <h1><span><strong style="color: #e67e22;">4</strong></span><span><strong>0</strong></span><span><strong style="color: #e67e22;">4</strong></span></h1>
                <h2>Error ! <span>Page Not Found</span></h2>
              </div>
              
              <p><a href="javascript:history.go(-1)">&laquo; Go Back</a> / <a href="{{url('/')}}">Go Home &raquo;</a></p>
             
            </section>
           
          </div>
        </div>


        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/dashboard/assets/global/scripts/app.min.js" type="text/javascript"></script>

    </body>

</html>