			<div class="page-wrapper">
			<!-- BEGIN HEADER -->
			<div class="page-header navbar navbar-fixed-top">
				<!-- BEGIN HEADER INNER -->
				<div class="page-header-inner">
					<!-- BEGIN LOGO -->
					<div class="page-logo">
						<a href="http://school.edniche.com/">
							<img src="/dashboard/assets/img/logo/logoedniche.png" alt="logo" class="logo-default"/ style="width: 120px; margin-top: 5px;"> </a>
						<div class="menu-toggler sidebar-toggler">
							<span></span>
						</div>
					</div>
					<!-- END LOGO -->
					<!-- BEGIN RESPONSIVE MENU TOGGLER -->
					<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
						<span></span>
					</a>
					<!-- END RESPONSIVE MENU TOGGLER -->
					<!-- BEGIN TOP NAVIGATION MENU -->
					<div class="top-menu">
						<ul class="nav navbar-nav pull-right">
							<!-- BEGIN NOTIFICATION DROPDOWN -->
							
	<!-- 						<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									<i class="icon-bell"></i>
									<span class="badge badge-danger"> 7 </span>
								</a>
								<ul class="dropdown-menu">
									<li class="external">
										<h3>
											<span class="bold">12 pending</span> notifications</h3>
										<a href="page_user_profile_1.html">view all</a>
									</li>
									<li>
										<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
											<li>
												<a href="javascript:;">
													<span class="time">just now</span>
													<span class="details">
														<span class="label label-sm label-icon label-success">
															<i class="fa fa-plus"></i>
														</span> New user registered. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">3 mins</span>
													<span class="details">
														<span class="label label-sm label-icon label-danger">
															<i class="fa fa-bolt"></i>
														</span> Server #12 overloaded. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">10 mins</span>
													<span class="details">
														<span class="label label-sm label-icon label-warning">
															<i class="fa fa-bell-o"></i>
														</span> Server #2 not responding. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">14 hrs</span>
													<span class="details">
														<span class="label label-sm label-icon label-info">
															<i class="fa fa-bullhorn"></i>
														</span> Application error. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">2 days</span>
													<span class="details">
														<span class="label label-sm label-icon label-danger">
															<i class="fa fa-bolt"></i>
														</span> Database overloaded 68%. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">3 days</span>
													<span class="details">
														<span class="label label-sm label-icon label-danger">
															<i class="fa fa-bolt"></i>
														</span> A user IP blocked. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">4 days</span>
													<span class="details">
														<span class="label label-sm label-icon label-warning">
															<i class="fa fa-bell-o"></i>
														</span> Storage Server #4 not responding dfdfdfd. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">5 days</span>
													<span class="details">
														<span class="label label-sm label-icon label-info">
															<i class="fa fa-bullhorn"></i>
														</span> System Error. </span>
												</a>
											</li>
											<li>
												<a href="javascript:;">
													<span class="time">9 days</span>
													<span class="details">
														<span class="label label-sm label-icon label-danger">
															<i class="fa fa-bolt"></i>
														</span> Storage server failed. </span>
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li> -->
							<!-- END NOTIFICATION DROPDOWN -->
							<!-- BEGIN INBOX DROPDOWN -->
							<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
<!-- 							<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									<i class="icon-envelope-open"></i>
									<span class="badge badge-danger"> 4 </span>
								</a>
								<ul class="dropdown-menu">
									<li class="external">
										<h3>You have
											<span class="bold">7 New</span> Messages</h3>
										<a href="app_inbox.html">view all</a>
									</li>
									<li>
										<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
											<li>
												<a href="#">
													<span class="photo">
														<img src="/dashboard/assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
													<span class="subject">
														<span class="from"> Lisa Wong </span>
														<span class="time">Just Now </span>
													</span>
													<span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
												</a>
											</li>
											<li>
												<a href="#">
													<span class="photo">
														<img src="/dashboard/assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
													<span class="subject">
														<span class="from"> Richard Doe </span>
														<span class="time">16 mins </span>
													</span>
													<span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
												</a>
											</li>
											<li>
												<a href="#">
													<span class="photo">
														<img src="/dashboard/assets/layouts/layout3/img/avatar1.jpg" class="img-circle" alt=""> </span>
													<span class="subject">
														<span class="from"> Bob Nilson </span>
														<span class="time">2 hrs </span>
													</span>
													<span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
												</a>
											</li>
											<li>
												<a href="#">
													<span class="photo">
														<img src="/dashboard/assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
													<span class="subject">
														<span class="from"> Lisa Wong </span>
														<span class="time">40 mins </span>
													</span>
													<span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>
												</a>
											</li>
											<li>
												<a href="#">
													<span class="photo">
														<img src="/dashboard/assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
													<span class="subject">
														<span class="from"> Richard Doe </span>
														<span class="time">46 mins </span>
													</span>
													<span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li> -->
							<!-- END INBOX DROPDOWN -->
							<!-- BEGIN TODO DROPDOWN -->
						
							<!-- END TODO DROPDOWN -->
							<!-- BEGIN USER LOGIN DROPDOWN -->
							<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
							<li class="dropdown dropdown-user">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									<img alt="" class="img-circle" src="/dashboard/assets/img/avatars/{{ Auth::user()->avatar }}"  style="width: 30px; height: 30px; border-radius: 50px;">
									<span class="username username-hide-on-mobile">{{ Auth::user()->name }}</span>
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu dropdown-menu-default">
									<li>
										@if(Auth::user()->role == 1 || Auth::user()->role == 2)
												<a href="{{route('admin.profile', Auth::user()->id)}}">
												<i class="icon-user"></i> My Profile </a>
												@elseif(Auth::user()->role == 3)
													<a href="{{route('team.profile', Auth::user()->id)}}">
													<i class="icon-user"></i> My Profile </a>

													@elseif(Auth::user()->role == 4)
													<a href="{{route('employee.profile', Auth::user()->id)}}">
													<i class="icon-user"></i> My Profile </a>


												@endif
									</li>
									<li>
									@if(Auth::user()->role == 1 || Auth::user()->role == 2)
										<a href="{{route('admin.dashboard')}}">
											<i class="icon-calendar"></i> My Calendar </a>
												@elseif(Auth::user()->role == 3)

												<a href="{{route('team.dashboard')}}">
												<i class="icon-calendar"></i> My Calendar </a>

													@elseif(Auth::user()->role == 4)

													<a href="{{route('employee.dashboard')}}">
												<i class="icon-calendar"></i> My Calendar </a>

									@endif
									</li>

									<li>
										@if(Auth::user()->role == 3)

										<a href="{{url('/teamleader/project')}}">
											<i class="icon-envelope-open"></i> My Project
										
											<span class="badge badge-danger"></span>
											
										</a>

											@elseif(Auth::user()->role == 4)
												
												<a href="{{url('/employee/project')}}">
												<i class="icon-envelope-open"></i> My Project
												<span class="badge badge-danger"> </span>
											</a>


												@endif

									</li>
									<li>
										@if(Auth::user()->role == 3)

										<a href="{{url('/teamleader/myassignment')}}">
											<i class="icon-rocket"></i> My Assignment
											<span class="badge badge-success"> 7 </span>
										</a>
										@elseif(Auth::user()->role == 4)
											<a href="{{url('/employee/myassignment_employee')}}">
											<i class="icon-rocket"></i> My Assignment
											<span class="badge badge-success"> </span>
										</a>

											@endif

									</li>
									<li class="divider"> </li>

									<li>
										<a href="{{ url('/logout') }}" onclick="event.preventDefault();
													 document.getElementById('logout-form').submit();">
											<i class="icon-key"></i> Log Out </a>

											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
													{{ csrf_field() }}
												</form>

									</li>
								</ul>
							</li>
							<!-- END USER LOGIN DROPDOWN -->
							<!-- BEGIN QUICK SIDEBAR TOGGLER -->
							<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
							
							<!-- END QUICK SIDEBAR TOGGLER -->
						</ul>
					</div>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END HEADER INNER -->
			</div>
			<!-- END HEADER -->
			<div class="clearfix"> </div>

			<div class="page-container">