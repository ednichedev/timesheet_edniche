				<!-- BEGIN SIDEBAR -->
				<div class="page-sidebar-wrapper">
					<!-- BEGIN SIDEBAR -->
					<div class="page-sidebar navbar-collapse collapse">
						
						<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
						
							<li class="sidebar-toggler-wrapper hide">
								<div class="sidebar-toggler">
									<span></span>
								</div>
							</li>
							<!-- END SIDEBAR TOGGLER BUTTON -->
						
						
								<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
							<section style="margin-top: -20px; background-color: #2980b9; height: 29px; opacity: 0.9 ;
	filter: alpha(opacity=90) ;">&nbsp;</section> 
								<!-- END RESPONSIVE QUICK SEARCH FORM -->
				
							<li class="nav-item start active open">
								<a href="javascript:;" class="nav-link nav-toggle">
									<i class="icon-home"></i>
									<span class="title">Dashboard</span>
									
									
								</a>

							</li>
							<li class="heading">
								<h3 class="uppercase"></h3>
							</li>

							@if(Auth::user()->role != 3 && Auth::user()->role != 4)

							<li class="nav-item {{ url()->current() == url('admin/')?'active':''}}">
								<a href="{{route('admin.dashboard')}}" class="nav-link nav-toggle">
									<i class="icon-pie-chart"></i>
									<span class="title">My Calender</span>
									
								</a>

							</li>


							<li class="nav-item {{ url()->current() == url('admin/users')?'active':''}}">
								<a href="{{ url('/admin/users')}}" class="nav-link nav-toggle">
									<i class="icon-users"></i>
									<span class="title">Users</span>
									
								</a>

							</li>
						
							<li class="nav-item {{ url()->current() == url('admin/department')?'active':''}}">
								<a href="{{ url('admin/department')}}" class="nav-link nav-toggle">
									<i class="icon-briefcase"></i>
									<span class="title">Departments</span>
									
								</a>

							</li>
								@endif


							@if(Auth::user()->role == 3)

								<li class="nav-item {{ url()->current() == url('teamleader/myassignment')?'active':''}}">
								<a href="{{url('teamleader/myassignment')}}" class="nav-link nav-toggle">
									<i class="icon-pie-chart"></i>
									<span class="title">My Assignments</span>
									
								</a>

							</li>

							<li class="nav-item {{ url()->current() == url('teamleader/team')?'active':''}}">
								<a href="{{url('teamleader/team')}}" class="nav-link nav-toggle">
									<i class="icon-fire"></i>
									<span class="title">Teams</span>
									
								</a>

							</li>
					
							<li class="nav-item {{ url()->current() == url('teamleader/project')?'active':''}}">
								<a href="{{url('teamleader/project')}}" class="nav-link nav-toggle">
									<i class=" icon-rocket"></i>
									<span class="title">Projects</span>
									
								</a>

							</li>

							@endif

							@if(Auth::user()->role == 4)

								<li class="nav-item {{ url()->current() == url('employee/myassignment_employee')?'active':''}}">
								<a href="{{url('employee/myassignment_employee')}}" class="nav-link nav-toggle">
									<i class="icon-pie-chart"></i>
									<span class="title">My Assignments</span>
									
								</a>

							</li>

							<li class="nav-item {{ url()->current() == url('employee/team')?'active':''}}">
								<a href="{{url('employee/team')}}" class="nav-link nav-toggle">
									<i class="icon-fire"></i>
									<span class="title">Teams</span>
									
								</a>

							</li>
					
							<li class="nav-item {{ url()->current() == url('employee/project')?'active':''}}">
								<a href="{{url('employee/project')}}" class="nav-link nav-toggle">
									<i class=" icon-rocket"></i>
									<span class="title">Projects</span>
									
								</a>

							</li>

							@endif

							@if(Auth::user()->role != 3 && Auth::user()->role != 4)
							<li class="nav-item {{ url()->current() == url('admin/team')?'active':''}}">
								<a href="{{ url('admin/team') }}" class="nav-link nav-toggle">
									<i class="icon-fire"></i>
									<span class="title">Teams</span>
									
								</a>

							</li>
					
							<li class="nav-item {{ url()->current() == url('admin/project')?'active':''}}">
								<a href="{{ url('admin/project') }}" class="nav-link nav-toggle">
									<i class=" icon-rocket"></i>
									<span class="title">Projects</span>
									
								</a>

							</li>

							<li class="nav-item {{ url()->current() == url('admin/assignment')?'active':''}}">
								<a href="{{ url('admin/assignment') }}" class="nav-link nav-toggle">
									<i class="icon-puzzle"></i>
									<span class="title">Assignments</span>
									
								</a>

							</li>

							<li class="nav-item {{ url()->current() == url('admin/report')?'active':''}}">
								<a href="{{url('/admin/report')}}" class="nav-link nav-toggle">
									<i class="icon-shield"></i>
									<span class="title">Report</span>
									
								</a>

							</li>
							

						@endif
						</ul>
						<!-- END SIDEBAR MENU -->
						<!-- END SIDEBAR MENU -->
					</div>
					<!-- END SIDEBAR -->
				</div>
				<!-- END SIDEBAR -->