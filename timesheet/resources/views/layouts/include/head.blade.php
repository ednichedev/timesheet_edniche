<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title>Edniche</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
		<meta name="_token" content="{{ csrf_token() }}"/>

		<meta content="" name="author" />
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	  rel="stylesheet">
	  
		<link href="/dashboard/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="/dashboard/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

		<link href="/dashboard/assets/plugins/dropdown/components/dropdown.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/plugins/dropdown/components/modal.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/plugins/dropdown/semantic.css" rel="stylesheet" type="text/css" />
		
		<link href="/dashboard/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

		<link href="/dashboard/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />

		<!-- BEGIN PAGE LEVEL PLUGINS -->
       <!--  <link href="/dashboard/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/dashboard/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" /> -->
        <link href="/dashboard/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/dashboard/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/dashboard/assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->


		<link href="/dashboard/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />

		<link href="/dashboard/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="/dashboard/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="/dashboard/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="/dashboard/assets/layouts/layout/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="/dashboard/assets/layouts/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="/dashboard/assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
		<!-- END THEME LAYOUT STYLES -->

		<!-- Calender Style -->

		<link href='/dashboard/vendor/fullcalendar/fullcalendar.min.css' rel='stylesheet' />

		<!-- End Calender Style -->


		<!-- Datetimepicker -->

		<link rel="stylesheet" href="/dashboard/vendor/datimepicker/css/datimepicker.css">

		<link rel="shortcut icon" href="/dashboard/assets/favicon.png" /> 


		<script>
		window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
		]); ?>



		</script>


		</head>
	<!-- END HEAD -->
	
