@include('layouts.include.head')

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

	@include('layouts.include.header')
	
	@include('layouts.include.sidebar')

	@yield('content')



</body>

@include('layouts.include.end-page')

@yield('javascript')
