<div class="modal fade" id="showNote{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Show Notes</h4>
			</div>
			<div class="modal-body">
				
					@if($task->noteSingles->count())
						<ul class="list-group">
							@foreach($task->noteSingles as $note)
					  <li class="list-group-item"><img class="user-avatar popovers" src="/dashboard/assets/img/avatars/{{$note->creatBys->avatar}}" style="max-width: 18px; border-radius: 100px;" data-toggle="tooltip" data-placement="left" data-content="{{$note->creatBys->name}}" data-original-title="">&nbsp; {{ $note->note }}</li>
					  @endforeach
						</ul>
						@else

						<ul class="list-group">
							
					  <li class="list-group-item list-group-item-warning"><i class="fa fa-meh-o" aria-hidden="true"></i></li>
					 
						</ul>

						@endif

							<div class="form-group"></div>
							<!-- End .form-group  -->

								<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																	
								</div>
								<!-- End .form-group  -->
			</div>
			
		</div>
	</div>
</div>