<div class="modal fade" id="myAssigSingleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">New Assignment</h4>
			</div>
			<div class="modal-body">
				
			<form class="form form-horizontal" role="form" method="POST" action="{{ route('myassignment.add' )}}">

									{{ csrf_field() }}

									<div class="form-group">
									<input type="hidden" name="auth_id" value="{{Auth::user()->id}}">

									 @foreach($assignmentsingles as $assignmentsingle) 
										<input type="hidden" name="project_duedate" value="{{ $assignmentsingle->duedate }}">
										@endforeach

									</div>

									<div class="form-group">
											<div class="col-lg-12">

												<input type="text" name="title" class="form-control f12" required autofocus placeholder="Title..." id="title">
											</div>
									</div>

									<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
											<textarea class="form-control col-md-12 f12" name="desc" placeholder="Description..." id="desc"></textarea>
											</div>
										</div>


											<div class="form-group">

												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
												<input  type="text"  class="form-control" placeholder="Start Date..."  id="example7" name="start_date">
												</div>
												</div>

													

											</div>


										<div class="form-group">

													
												<div class="col-md-6">
												<div class="input-icon">
												<i class="fa fa-calendar"></i>
													<input  type="text" class="form-control" placeholder="End Date..."  id="example8" name="duedate">
													</div>
													</div>


													<div id="log-single" class="col-md-3 f18" style="position: absolute; margin-top: -40px; margin-left: 440px; text-align: center; color: #e67e22;"></div>  
														
									</div>

										

											@if ($errors->has('duedate'))
													<span class="help-block alert alert-danger alert-dismissible" role="alert">
																	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																	</button>
														<strong>{{ $errors->first('duedate') }}</strong>
														</span>
												@endif


									<div class="form-group"></div>
										<!-- End .form-group  -->
									 

										<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																	<button class="btn blue">Save</button>
																</div>

										<!-- End .form-group  -->
									</form>

			</div>
			
		</div>
	</div>
</div>