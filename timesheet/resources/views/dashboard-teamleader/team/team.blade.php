@extends('layouts.main-app')

   @section('content')

						<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content">
						<!-- BEGIN PAGE HEADER-->
		
						<!-- BEGIN PAGE BAR -->
						<div class="page-bar">
							<ul class="page-breadcrumb">
								
								<li>
								<a href="{{route('team.dashboard')}}">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>

									</a>
								</li>

									&nbsp;
									&nbsp;
									&nbsp;

								<li class="page-breadcrumb-deactive">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Teams</span>
								</li>

							</ul>

						</div>
						<!-- END PAGE BAR -->
						<!-- BEGIN PAGE TITLE-->
						<h1 class="page-title">
						</h1>
						<!-- END PAGE TITLE-->
						<!-- END PAGE HEADER-->
						<!-- BEGIN DASHBOARD STATS 1-->
						<div class="row">

						<div class="portlet light col-xs-12 col-sm-12 col-md-12 col-lg-12">

											
														<p style="margin-top: -25px;">&nbsp;</p>
												
												<table class="table">

													<tbody>
													@foreach($teams as $team)
														<tr>

																<td width="5%">
																<img class="user-avatar" src="/dashboard/assets/img/teams/avatar/{{ $team->avatar }}" style="max-width: 30px; border-radius:50px;">
																</td>

																<td>{{ $team->title }}
																<br>
																	<span class="f12 color-light">{{ $team->departments->title }}</span>
																</td>
																
																<td>
																@foreach($team->members->slice(0, 10) as $member)
																	
																<img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $member->avatar }}" style="max-width: 20px; border-radius:50px;">
																	{{ Str::limit($member->name, 5,'') }}
																
																		&nbsp;
																@endforeach

																	@if($team->members->count() > 10)
																	<div class="label label-info"><span class="f12">More ( {{ $team->members->count() }} )</span></div>
																				
																			@else
																		
																@endif
																</td>
																
														</tr>
														@endforeach
													</tbody>
													
												</table>


							</div>

						</div>


						<div class="clearfix"></div>
						<!-- END DASHBOARD STATS 1-->
						<div class="row">
						</div>
						
						
						
					</div>
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->		
	



@stop