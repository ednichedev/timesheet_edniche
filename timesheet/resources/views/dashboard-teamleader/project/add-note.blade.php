<div class="modal fade" id="myNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Notes</h4>
			</div>
			<div class="modal-body">
				
			<form class="form form-horizontal" role="form" method="POST" action="{{ route('teamleader.project.task.note.create')}}">

									{{ csrf_field() }}

									<div class="form-group">
									<input type="hidden" name="task_id">
									<input type="hidden" name="create_by" value="{{Auth::user()->id}}">
									
									</div>

									<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
											
											<div class="col-lg-12">
											<textarea class="form-control col-md-12 f12" name="note" placeholder="Note..." id="note" rows="6"></textarea>
											</div>
										</div>


									<div class="form-group"></div>
										<!-- End .form-group  -->

										<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																	<button class="btn blue">Save</button>
																</div>

										<!-- End .form-group  -->
									</form>

			</div>
			
		</div>
	</div>
</div>