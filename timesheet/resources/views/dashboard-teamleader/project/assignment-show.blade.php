@extends('layouts.main-app')

	 @section('content')



			<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
		 <!-- BEGIN CONTENT BODY -->
		 <div class="page-content">
			<!-- BEGIN PAGE HEADER-->
	
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
			 <ul class="page-breadcrumb">
				
				<li class="page-breadcrumb-deactive">
				 <a href="{{ url('/teamleader')}}"><span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
				 </a>
				</li>
				&nbsp;
				&nbsp;
				&nbsp;
				<li class="page-breadcrumb-deactive">
				<a href="{{ url('/teamleader/project')}}">
					<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Project</span>
					</a>
				</li>

				&nbsp;
				&nbsp;
				&nbsp;
				<li class="">
				
					<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Project details</span>
					
				</li>

			 </ul>

			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title">
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS 1-->
			<div class="row">

			<div class="portlet light col-xs-12 col-sm-12 col-md-12 col-lg-12">

					

					<p style="margin-top: -25px;">&nbsp;</p>



							
@php
$count =1
@endphp

	@foreach($projects as $project)

	 <div class="row block-row">

		 <div class="col-lg-12 border-blobk">

				<div class="col-lg-3">
					
				 <span class="f18 color-light block-project">
				 {{ $project->title}}
				 </span>

				 <span class="f11 block-project color-light">
					Started &nbsp;{{ Carbon\Carbon::parse($project->start_date)->format('d-M-Y') }}&nbsp; | &nbsp; 
				 Ends &nbsp;{{ Carbon\Carbon::parse($project->duedate)->format('d-M-Y') }}
				 </span>

				 

				</div>

				<div class="col-md-3">

				<span class="f14 color-light block-project">
					Description
				 </span>

				 <span class="f11 block-project color-light">
				{{ $project->desc}}
				 </span>
					
				</div>

				<div class="col-md-2">

				 <span class="f14 color-light block-project">
					
				 </span>

				 <?php
				 
					$current = 0;
					$percent=0;
					$assignment = $countCompleted->count();
					$total = $project->assignmentprojects->count() * 1000;
					$current = $assignment * 1000;
					if ($total!=0) {				 	
						$percent = ($current /	$total) *100;
						settype($percent, "integer");	
					}
				 
				 ?>



				 <div class="progress">
									
											<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" style="width:{{	$percent }}%">
												<span class="sr-only"></span>
												{{	$percent }} %
											</div>
								</div>

				
					
				</div>



				<div class="col-lg-2">

				<small class="f16 pull-right" style="text-align: center;">
				
				<div class="circle" style=" margin-top: -5px;">
				 <span id="show_left">
				 
					<input type="text" value="{{ $countleft->count()}}" id="project_assign_left" class="input_border_less">
					
				 </span>

				 </div>
				 <h5 class="f12 color-light" style="margin-top: 5px; margin-bottom: -10px; margin-left: 40px;">Assignments Left</h5>
				 

				</small>
				 
				</div>

				<div class="col-lg-2">

				<small class="f16 pull-right" style="text-align: center;">
				
				<div class="circle-complet" style=" margin-top: -5px;">
				 <span id="show_completed">
				 
					<input type="text" value=" {{ $countCompleted->count() }}" id="project_assign_complete" class="input_border_less">
					
				 </span>

				 </div>
				 <h5 class="f12 color-light" style="margin-top: 5px; margin-bottom: -10px; margin-left: 55px;">Completed</h5>
				 
				 <div class="circlecircle hidden">
				 <i class="fa fa-check f20" style="color:#2ecc71;"></i>
				 </div>

				

				</small>

				</div>


		 </div>
		 <!-- end col 12 -->

		 <div class="col-lg-12 member-block">
			<table class="table table-borderless">

			 <tbody>

			 @php
			 $i=1
			 @endphp

			 @foreach($project->assignmentprojects as $assignmentproject)
				<tr>
				<td width="2%">


					@if($assignmentproject->completed == 0)

						<form method="POST" action="" id="assign_complet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="completed" value="1">
					 <input type="hidden" name="completed_by" value="{{Auth::user()->id }}">

						<input type="checkbox" id="check{{$i}}" data-id="{{ $assignmentproject->id }}">
						</form>


						@else

						<form method="POST" action="" id="assign_uncomplet_form" class="">
						{{ csrf_field() }}
					 <input type="hidden" name="uncompleted" value="0">
					 

					 <input type="checkbox" id="uncheck{{$i}}" data-id="{{ $assignmentproject->id }}" checked="checked">
						</form>


						@endif


				</td>
				 
				 <td>
				 <a href="{{route('assignmentproject.get.taskproject', $assignmentproject->id)}}">
				 <span class="f12">{{ Str::limit($assignmentproject->title, 80,'...') }}</span>
				 </a>
				 <br>
					<span class="f11" style="color: #e67e22;">Tasks({{$assignmentproject->tasks->count()}})</span>
				 </td>

				 <td>
					<button class="btn btn-link f11 popovers pull-right" data-container="body" data-trigger="hover" data-placement="top" data-content="New Task" aria-expanded="false" data-toggle="modal" data-target="#myTaskModal" data-assign-id="{{$assignmentproject->id}}" data-user-id="{{$assignmentproject->user->id}}" data-ass-date="{{$assignmentproject->duedate}}" id="add-task{{$i}}"><i class="fa fa-plus"></i></button>
				 </td>

				 <td width="2%"> <img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $assignmentproject->user->avatar }}" style="max-width: 18px; border-radius: 100px;"></td>
				 <td width="10%">
				 <span class="f11 color-light">{{ $assignmentproject->user->name }}</span>
				 </td>
				 <td width="6%">
				 <span class="f11 color-light">
								Assign by
						</span>
						</td>
				 <td width="2%"> <img class="user-avatar" src="/dashboard/assets/img/avatars/{{ $assignmentproject->creatBys->avatar }}" style="max-width: 18px; border-radius: 100px;"></td>

					 <td width="10%">
						 <span class="f11 color-light">
						 {{ $assignmentproject->creatBys->name }}
						 </span>
					 </td>

				 <td width="10%">
					<span class="f12 color-light pull-left">{{ Carbon\Carbon::parse($assignmentproject->start_date)->format('d-M-Y') }}</span>
				 </td>

				 <td width="10%">
					<span class="f12 color-light pull-left">{{ Carbon\Carbon::parse($assignmentproject->duedate)->format('d-M-Y') }}</span>
				 </td>

				 <td width="10%" id="completed{{$i}}">
				 <div>
					@if($assignmentproject->completed == 1)
					 <span class="badge" style="background-color: #2ecc71;">
					 Completed
					 </span>

					 @else

					 <?php


	  $countTaskCompleted = App\TaskProject::where('assignment_id', $assignmentproject->id)->where('completed', 1)->get();
 
			$current = 0;
					$percent=0;
					$tasks = $countTaskCompleted->count();
					$total = $assignmentproject->tasks->count() * 1000;
				 
					$current = $tasks * 1000;
					if ($total!=0) {				 	
						$percent = ($current /	$total) *100;
						settype($percent, "integer");	
					}
				 
				 ?>

					 <div class="progress">
									
											<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" style="width:{{	$percent }}%">
												<span class="sr-only"></span>
											{{	$percent }} %
											</div>
								</div>

					 @endif
					</div>
				 </td>


				 <td width="5%">
					@if($assignmentproject->create_by == Auth::user()->id)
						
					 <form class="form form-horizontal" role="form" method="POST" action="{{route('projec.deleteassign')}}">
						{{ csrf_field() }}
					 <input type="hidden" name="assign_id" value="{{ $assignmentproject->id }}">
					 <button class="btn btn-link" style="margin-top: -8px;"><i class="fa fa-close"></i></button>
					 
						</form>
					 @endif
				 </td>

				</tr> 

			 @php
				$i++
			 @endphp

			 @endforeach


			 </tbody>
			 
			</table>
			
		 </div>


		 <div class="col-lg-12">
			 <div class="col-lg-4 pull-left">
			<button class="btn btn-link f11 popovers pull-left" data-container="body" data-trigger="hover" data-placement="top" data-content="New Assignment" aria-expanded="false" data-toggle="modal" data-target="#myAssigProModal" value="{{$project->id}}" data-project-date="{{$project->duedate}}" id="add-assig-pro" style="margin-top: -5px;">
			<i class="fa fa-plus"></i> &nbsp; New Assignment</button>
				
			 </div>
			 <div class="col-lg-8">&nbsp;</div>
 
		 </div>

	 </div>
<!--  end row -->            


	 @php
		$count++
	 @endphp

 @endforeach

			 </div>

@include('dashboard-teamleader.project.add-assig-pro')

@include('dashboard-teamleader.project.add-task')


			</div>


			<div class="clearfix"></div>
			<!-- END DASHBOARD STATS 1-->
			<div class="row">
			</div>
			
		 
		 </div>
		 <!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->  


@stop

@section('javascript')

<script type="text/javascript">

//Assignment

	$(document).ready(function () {
				
			$(document).on('change', '#example6', function () {


				fromDate = Date.parse($('#example5').val());

				toDate = Date.parse($('#example6').val());

				var dateDiff = (toDate - fromDate);

				var DD = Math.floor(dateDiff / 3600 / 24);
				var formatdate = DD / 1000;
			

					$( "#log" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

					
				});
	});


	$(document).ready(function () {

	

$(document).on('click', '#add-assig-pro', function(){

			var id = $('#add-assig-pro').val();
			
			var ass_project = $('#add-assig-pro').attr("data-project-date");


			$('#myAssigProModal').on('shown.bs.modal', function () {

				$( "input[name~='project_id']" ).val(id);

				$( "input[name~='project_duedate']" ).val(ass_project);


		 });



		});


	});




	///Tasks 	

$(document).ready(function () {
			
	
	$(document).on('change', '#end_time', function () {

		fromDate = Date.parse($('#example1').val());

	toDate = Date.parse($('#example2').val());

	fromtime = parseInt($("#start_time").val());

	totime = parseInt($("#end_time").val());
				
				var timeDiff = (totime - fromtime);
				var dateDiff = (toDate - fromDate);

	var DD = Math.floor(dateDiff / 3600 / 24);
		var formatdate = DD / 1000;
				var formatted = ((timeDiff < 10) ? ("0" + timeDiff + " Hours") : timeDiff);

				if (toDate > fromDate) {

					$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatdate+' Days'+"</div>" );

				}else{

						$( "#log_task" ).html( "<div class='well' id='datetime_show'>"+formatted+"</div>" );

				}

					
				});

	});


// Add Task

$(document).ready(function () {

			<?php
		

			foreach($projects as $project){

							$count = $project->assignmentprojects->count();
			}


	?>

<?php for($i=1;$i<=$count;$i++){ ?>



$(document).on('click', '#add-task<?php echo $i ?>', function(){

			var id = $('#add-task<?php echo $i ?>').attr("data-assign-id");

			var ass_date = $('#add-task<?php echo $i ?>').attr("data-ass-date");
			var user_id = $('#add-task<?php echo $i ?>').attr("data-user-id");

			$('#myTaskModal').on('shown.bs.modal', function () {

				$( "input[name~='assignment_id']" ).val(id);
				$( "input[name~='user_id']" ).val(user_id);
				$( "input[name~='assignmentpro_duedate']" ).val(ass_date);


		 });



		});

	<?php }?>

	});


//Complete Assignment 

$(document).ready(function() {

	 $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}}); 
	 
	 $.ajaxSetup({cache: false });

});

	
	$(document).ready(function () {


			<?php
		

			foreach($projects as $project){

							$count = $project->assignmentprojects->count();
			}


	?>
	
		<?php for($i=1;$i<=$count;$i++){ ?>

	$(document).on('change', '#check<?php echo $i; ?>', function() {

		var id = $("#check<?php echo $i; ?>").attr("data-id");

		var completed =  parseFloat($("#project_assign_complete").val());

		var left = parseFloat($("#project_assign_left").val()); 

		var num = 1;

		var cal_completed = completed + num;

		var cal_left = left - num;



				$.ajax({
					
						url: "/teamleader/project/completed/"+id,
						type: 'POST',
						data: $('#assign_complet_form').serialize(), 
						dataType: 'json',
						cache: false,
						success: function(data){

							
							//window.location.reload(true);


							$("#show_completed").html("<input type='text' value="+cal_completed+" id='project_assign_complete' class='input_border_less'>");

							$("#show_left").html("<input type='text' value="+cal_left+" id='project_assign_left' class='input_border_less'>");

							$("#completed<?php echo $i; ?>").html("<span class='badge' style='background-color: #2ecc71;'>Completed</span>");
							
							window.location.reload(true);

						}

				});

				event.preventDefault();


	});

	<?php }?>

});


	// Uncompleted Assignment Project

$(document).ready(function () {
	
		<?php
		foreach($projects as $project){

							$count = $project->assignmentprojects->count();
			}

	?>
	
		<?php for($i=1;$i<=$count;$i++){ ?>

	$(document).on('change', '#uncheck<?php echo $i; ?>', function() {

		var id = $("#uncheck<?php echo $i; ?>").attr("data-id");
		
		var completed =  parseFloat($("#project_assign_complete").val());

		var left = parseFloat($("#project_assign_left").val()); 

		var num = 1;

		var cal_completed = completed - num;

		var cal_left = left + num;

		

				$.ajax({
					
						url: "/teamleader/project/uncompleted/"+id,
						type: 'POST',
						data: $('#assign_uncomplet_form').serialize(), 
						dataType: 'json',
						cache: false,
						success: function(data){
							
							//window.location.reload(true);


							$("#show_completed").html("<input type='text' value="+cal_completed+" id='project_assign_complete' class='input_border_less'>");

							$("#show_left").html("<input type='text' value="+cal_left+" id='project_assign_left' class='input_border_less'>");

							$("#completed<?php echo $i; ?>").html("<span class='badge' style='background-color: #2ecc71;'></span>");

							window.location.reload(true);

								 

						}
				});

				event.preventDefault();


	});

	<?php }?>



});

</script>

@stop