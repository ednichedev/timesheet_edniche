@extends('layouts.main-app')

	 @section('content')



			<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
		 <!-- BEGIN CONTENT BODY -->
		 <div class="page-content">
			<!-- BEGIN PAGE HEADER-->
	
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
			 <ul class="page-breadcrumb">
				
				<li class="page-breadcrumb-deactive">
				 <a href="{{ url('/teamleader')}}"><span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Overview</span>
				 </a>
				</li>
				&nbsp;
				&nbsp;
				&nbsp;
				<li class="">
									<span><i class="material-icons f16 absolute">more_vert</i>&nbsp;Project</span>
				</li>

			 </ul>

			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title">
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS 1-->
			<div class="row">

			<div class="portlet light col-xs-12 col-sm-12 col-md-12 col-lg-12 morebox">
		 <!--  <p style="margin-top: -25px;">&nbsp;</p> -->


				<table class="table">
				
					@foreach($teams as $team)

					@if($team->leaders_id == Auth::user()->id)

					@foreach($projects as $project)

					@if($project->teams_id == $team->id)

					@if($project->completed == 1)

							<tr bgcolor="#E1F5FE">
										@else
							<tr>
						@endif
						<td>
						<span class="f18 block-project">
						<a href="{{ route('project.get.assignments', $project->id )}}" class="color-light">{{ $project->title}}</a>
						</span>
						<span class="f11 block-project color-light">
						Assignments &nbsp;
									<span class="badge" style="background-color: #2ecc71;">
		   {{ $project->assignmentprojects->count()}}
		  </span>
						</span>
						</td>
						<td>
						<span class="project_image">
		   <img class="user-avatar" src="/dashboard/assets/img/teams/avatar/{{ $project->teams->avatar }}" style="max-width: 20px; border-radius:50px;">
		   <span class="f12 color-light">{{ $project->teams->title}}
		 </span>
		 </td>

			<td width="25%">Started &nbsp;{{ Carbon\Carbon::parse($project->start_date)->format('d-M-Y') }}&nbsp; | &nbsp; 
		 Ends &nbsp;{{ Carbon\Carbon::parse($project->duedate)->format('d-M-Y') }}
	  </td>

	  <td>

	   @if($project->completed == 1)
				
				<span class="badge" style="background-color: #2ecc71;">
					 Completed
					 </span>

	  @else
		
		<?php

		 $countAssignCompleted = App\Assignmentproject::where('projects_id', $project->id)->where('completed', 1)->get();
				 
					$current = 0;
					$percent=0;
					$assignment = $countAssignCompleted->count();
					$total = $project->assignmentprojects->count() * 1000;
					$current = $assignment * 1000;
					if ($total!=0) {				 	
						$percent = ($current /	$total) *100;
						settype($percent, "integer");	
					}
				 
				 ?>


				<div class="col-md-12">
				 <div class="progress">
									
											<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="30" style="width:{{	$percent }}%">
												<span class="sr-only"></span>
												{{	$percent }} %
											</div>
								</div>
					</div>

					@endif

	  </td>

		</tr>

						@endif

				@endforeach

			@endif
	@endforeach
					
	</table>

<div class="pagination pull-right">
	{{ $projects->links() }}
</div>



			 </div>




</div>

<!-- @foreach($teams as $team)
@if($team->projects->count()>5)
<div class="col-md-6">
		<button class="btn btn-sm btn-default pull-right btn_show_more">View More</button>

		<button class="btn btn-sm btn-default pull-right btn_show_less hidden">View Less</button>

</div>
@endif
@endforeach -->


			<div class="clearfix"></div>
			<!-- END DASHBOARD STATS 1-->
			<div class="row">
			</div>


			
		 
		 </div>
		 <!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->

			<div class="loading">
				
			</div>
			<div class="loader"></div> 


@stop

@section('javascript')
 
 <script type="text/javascript">
	$(document).ready(function () {
			
			$(document).on('click', '.btn_show_more', function(){

					$('.loading').fadeIn().delay(500).fadeOut();
					$('.loader').fadeIn().delay(500).fadeOut();

					setTimeout(function() {
				
					$('.morebox').css({"height":"auto"});

					$('.btn_show_more').addClass('hidden');
					$('.btn_show_less').removeClass('hidden');

						}, 1000);


				});

			$(document).on('click', '.btn_show_less', function(){

				 $('.loading').fadeIn().delay(500).fadeOut();
					$('.loader').fadeIn().delay(500).fadeOut();

						setTimeout(function() {

					$('.morebox').css({"height":"400px"});
					$('.btn_show_less').addClass('hidden');
					$('.btn_show_more').removeClass('hidden');

							}, 1000);

				
			});


	});
 </script>


@stop