$(document).ready(function() {

	 $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}}); 
});


 $(document).ready( function(){

			$('#submit').on('click', function(event) {

				event.preventDefault();

				function ValidateEmail(email) {
				var expr = /^([\w-\.]+)@edniche.com/;
				return expr.test(email);
			};
   
		if (!ValidateEmail($("#email").val())) {
						$('#email').css("border-color", "#e74c3c");
							$('#email').attr("placeholder", "example@edniche.com").placeholder();

			$('#error_email').fadeTo(200, 0).animate(
																	{ bottom: 30, opacity: 1 }, {
																	duration: 1000,
																	easing: 'easeOutQuint'
																	}).delay(3000)
																	.animate(
																	{ bottom: -60, opacity: 0}, {
																			duration: 1000,
																			easing: 'easeOutQuint',
																		 
																	});

		}
		else {


			$.ajax({
													url: $('#user_form').attr('action'),
													type: 'POST',
													//headers: {'X-CSRF-TOKEN': _token},
													//async: false,
													data: $('#user_form').serialize(), // Remember that you need to have your csrf token included
													//dataType: 'json',

													success: function(data){


														//console.log(data);
														//alert(data.msg);
														
														 $('#success').fadeTo(500, 0).animate(
																	{ bottom: 30, opacity: 1 }, {
																	duration: 1000,
																	easing: 'easeOutQuint'
																	}).delay(3000)
																	.animate(
																	{ bottom: -60, opacity: 0}, {
																			duration: 1000,
																			easing: 'easeOutQuint',
																		 
																	});

														$("#user_form").trigger("reset");

														getUser();

													},error:function(){

															$('#error').fadeTo(200, 0).animate(
																	{ bottom: 30, opacity: 1 }, {
																	duration: 1000,
																	easing: 'easeOutQuint'
																	}).delay(3000)
																	.animate(
																	{ bottom: -60, opacity: 0}, {
																			duration: 1000,
																			easing: 'easeOutQuint',
																		 
																	});
															
													}

											});

		}

	
				});

		 });
			

				$(document).ready( function(){

						$(document).on('click', '.block-btn', function(e){

								var id = $(this).attr("data-value");

								$.ajax({
										type: 'post',
										url: "/admin/users/block/",
										data: {id:id},
										dataType: 'json',
									 success:function(data){
					 
												// window.location.reload(true);
												 $('#success').fadeTo(500, 0).animate(
														{ bottom: 30, opacity: 1 }, {
														duration: 1000,
														easing: 'easeOutQuint'
														}).delay(3000)
														.animate(
														{ bottom: -60, opacity: 0}, {
																duration: 1000,
																easing: 'easeOutQuint',
															 
														});

															getUser();

										}
								});
						});

						$(document).on('click', '.btn-enable', function(e){

								var id = $(this).attr("data-value");

								$.ajax({
										type: 'post',
										url: "/admin/users/enable/",
										data: {id:id},
										dataType: 'json',
									 success:function(data){

												// window.location.reload(true);

												 $('#success').fadeTo(500, 0).animate(
														{ bottom: 30, opacity: 1 }, {
														duration: 1000,
														easing: 'easeOutQuint'
														}).delay(3000)
														.animate(
														{ bottom: -60, opacity: 0}, {
																duration: 1000,
																easing: 'easeOutQuint',
															 
														});

															getUser();
											 
										}
								});
						});
		 


				 });

 $(document).ready(function(){

		$(document).on('click', '#search', function(){

		 $("#search_users").animate({width:'200'},200); 
		 $("#search_users").focus();
		 $("#search").addClass('hidden');
		 $("#search-1").removeClass('hidden');
		 // $("#search_users").focusout(function () {

				//$("#search_users").animate({width:'30'},200);
		 // });

		});

		 $(document).on('click', '#search-1', function(){

				$("#search_users").animate({width:'30'},200);
				$("#search_users").val("");
				$("#search_users").trigger("reset");
				$("#search-1").addClass('hidden');
				$("#search").removeClass('hidden');
		 // });

		});

		
 });


	$(document).on('change', '#show_user', function(e){

				$value = $(this).val();
				
				 $.ajax({

								type : 'get',
								url : "/admin/users/show_user",
								data : {'show_user': $value},
								success:function(data){
									//console.log(data);
										//$('#book').html(data);

									$('.table-display').html(data);

								}

						});


		});

 $(document).ready(function(){

				$(document).on('keyup', '#search_users', function(e){

				// On click or whatever
					 $value = $(this).val();
					 //alert($value);                           
						$.ajax({

								type : 'get',
								url : "/admin/users/search",
								data : {'search_users': $value},
								success:function(data){
									//console.log(data);
										//$('#book').html(data);
									 
									$('.table-display').html(data);
									 
								 

								}

						});
				});
	});


				// declare function

				getUser();
				// declare function
		
				function getUser(){

						$.ajax({

								type: 'get',
								url: "/admin/users/getuser",
								dataType: 'html',
								success:function(data){
										$('.table-display').html(data);
										//console.log(data);
								}

						})
				}

		

			$(document).ready(function(e){

				$(document).on('click', '.add-new', function(e){

						$('.show_form_pass').addClass('hidden');
						$('.show_form_create').removeClass('hidden');

				});

				$(document).on('click', '#back-user', function () {
					 $('.show_form_pass').addClass('hidden');
						$('.show_form_create').removeClass('hidden');
				});

		});
		// Reset Password
		$(document).ready(function(e){

				$(document).on('click', '.resetpass', function(e){

					 var id = $(this).attr("data-value");

						var name = $(this).attr("data-name");
						var image = $(this).attr("data-image");

						$('.show_form_create').addClass('hidden');
						$('.show_form_account').addClass('hidden');
						$('.show_form_pass').removeClass('hidden');
						$('#user_name').removeClass('hidden');
						$('#user_name').html(name);
						$('#user_avatar').removeClass('hidden');
						$('#user_avatar').html("<img src=" +image+" style='max-width: 30px;'/>");
			 

				$('#user_resetpass').on('submit', function(event) {

					event.preventDefault();
		

						$.ajax({
										type: 'post',
										url: "/admin/users/resetpass/"+id,
						
										data: $('#user_resetpass').serialize(),
										dataType: 'json',
									 success:function(data){

												//window.location.reload(true);
												$('#resetpass').fadeTo(500, 0).animate(
														{ bottom: 30, opacity: 1 }, {
														duration: 1000,
														easing: 'easeOutQuint'
														}).delay(3000)
														.animate(
														{ bottom: -60, opacity: 0}, {
																duration: 1000,
																easing: 'easeOutQuint',
															 
														});

											$("#user_resetpass").trigger("reset");
											$("#btn-reset").fadeTo(200, 0);
											$("#btn-clear").fadeTo(200, 0);

											$("#btn-back-1").fadeTo(500, 1);

										 $("#reset-password").on('mousedown' ,function(e){
												$("#btn-clear").fadeTo(300, 1);
												$("#btn-reset").fadeTo(300, 1);
												$("#btn-back-1").fadeTo(300, 0).css({ "display": "none"});

											}); 

										$(document).on('click', '#btn-back-1', function(e){

												$('.show_form_create').fadeTo(300, 1).removeClass('hidden');
												$('.show_form_pass').fadeTo(100, 1).addClass('hidden');
										});
											
										 
											 
										},error:function(){

												$('#resetpass_notmacth').fadeTo(200, 0).animate(
														{ bottom: 30, opacity: 1 }, {
														duration: 1000,
														easing: 'easeOutQuint'
														}).delay(3000)
														.animate(
														{ bottom: -60, opacity: 0}, {
																duration: 1000,
																easing: 'easeOutQuint',
															 
														});
												
										}
										
								});

						 });

				});

		});


