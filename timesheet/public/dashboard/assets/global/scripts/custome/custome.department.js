$(document).ready(function() {

	 $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}}); 
});


$(document).ready( function(){

			$('#department_submit').on('click', function(event) {

				event.preventDefault();
				//var _token = $('#token').val();

				$.ajax({
						url: $('#department_form').attr('action'),
						type: 'POST',
						data: $('#department_form').serialize(), // Remember that you need to have your csrf token included

						success: function(data){

							console.log(data);
							//alert(data.msg);
							
							 $('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});

							$("#department_form").trigger("reset");

							getDepartment();

						},error:function(){

								$('#error').fadeTo(200, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});
								
						}

				});
	
				});

		 });
			

				$(document).ready( function(){

						$(document).on('click', '.btn-block-depart', function(e){

								var id = $(this).attr("data-value");

								$.ajax({
										type: 'post',
										url: "/admin/department/block/",
										data: {id:id},
										dataType: 'json',
									 success:function(data){
					 
												$('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});

						
										getDepartment();

										}
								});
						});

						$(document).on('click', '.btn-enable-depart', function(e){

							var id = $(this).attr("data-value");

								$.ajax({
										type: 'post',
										url: "/admin/department/enable/",
										data: {id:id},
										dataType: 'json',
									 success:function(data){

												$('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});

						

								getDepartment();
											 
										}
								});
						});
		 


				 });

 $(document).ready(function(){


		
		$(document).on('click', '#search', function(){

		 $("#search_depart").animate({width:'200'},200); 
		 $("#search_depart").focus();
		 $("#search").addClass('hidden');
		 $("#search-1").removeClass('hidden');
		 // $("#search_users").focusout(function () {

				//$("#search_users").animate({width:'30'},200);
		 // });

		});

		 $(document).on('click', '#search-1', function(){

		 // $("#search_users").animate({width:'200'},200); 
		 // $("#search_users").focus();
		 
		 // $("#search_users").focusout(function () {

				$("#search_depart").animate({width:'30'},200);
				$("#search_depart").val("");
				$("#search_depart").trigger("reset");
				$("#search-1").addClass('hidden');
				$("#search").removeClass('hidden');
		 // });

		});

		
 });


	$(document).on('change', '#show_depart', function(e){

				$value = $(this).val();
				
				 $.ajax({

								type : 'get',
								url : "/admin/department/show_depart",
								data : {'show_depart': $value},
								success:function(data){
									// console.log(data);
										//$('#book').html(data);

									$('.table-depart-display').html(data);

								}

						});


		});

 $(document).ready(function(){

				$(document).on('keyup', '#search_depart', function(e){

				// On click or whatever
					 $value = $(this).val();
					 //alert($value);                           
						$.ajax({

								type : 'get',
								url : "/admin/department/search",
								data : {'search_depart': $value},
								success:function(data){
									//console.log(data);
										//$('#book').html(data);
									 
									$('.table-depart-display').html(data);
									 
								}

						});
				});
	});


				// declare function

				getDepartment();
				// declare function
		
				function getDepartment(){

						$.ajax({

								type: 'get',
								url: "/admin/department/getdepartment",
								dataType: 'html',
								success:function(data){
										$('.table-depart-display').html(data);
										//console.log(data);
								}

						})
				}

		removeForm();
				
		function removeForm() {
					
				$('.department_form').removeClass('hidden');
				$('.department_form_edit').addClass('hidden');
		}

			$(document).ready(function(e){

				$(document).on('click', '.add-new', function(e){
						removeForm();

				});

					$(document).on('click', '#back-depart', function(e){

						removeForm();

				});

		});

			
		// Edit
		$(document).ready(function(e){

				$(document).on('click', '.edit', function(e){
					//alert('OK');
					var id = $(this).attr("data-value");
			
					var depart_name = $(this).attr("data-name");
					var depart_desc = $(this).attr("data-desc");
					var manager_name = $(this).attr("data-manager");
					var manager_id = $(this).attr("data-id");
					var image = $(this).attr("data-image");

						$('.department_form').addClass('hidden');
						$('.department_form_edit').removeClass('hidden');
						$('#depart_name').removeClass('hidden');
						$('#depart_name').html(depart_name);
						// $('#user_avatar').removeClass('hidden');
						$('.text-title').html("<input type='text' class='form-control col-md-12 f12' name='title' id='title' placeholder='Title...' required autofocus value="+depart_name+"> ");

						$('.text-desc').html("<textarea class='form-control col-md-12 f12' name='desc' placeholder='Description...'>"+depart_desc+"</textarea>");
					
					// $('.menu').html("<div class='item' data-value="+manager_id+"><img class='ui mini avatar image' src="+image+">"+manager_name+"</div>");
					$('.managers_id').html("<input type='hidden' name='managers_id' id='managers_id' class='att_name_default' value="+manager_id+"><div class='item'><img class='ui mini avatar image' src="+image+">&nbsp;<span class='f14'>"+manager_name+"</span>		<a href='#' class='btn btn-md btn-link' id='btn-rev-manage'><i class='icon-close'></i></a><hr></div>");


			$('.managers_id').on('click', function(event) {
			
				$('.managers_id').addClass('hidden');
				$('.selection').removeClass('hidden');
				$('.att_name').attr('name', 'managers_id');
				$('.att_name_default').attr('name', '');		
						
			});


	$(document).on('click', '#department_update', function(event) {

					event.preventDefault();
		
						$.ajax({
										type: 'post',
										url: "/admin/department/update/"+id,
										data: $('#department_form_edit').serialize(),
										dataType: 'json',
									 success:function(data){
				
										window.location.reload(true);

										},error:function(){

													$('#error').fadeTo(200, 0).animate(
															{ bottom: 30, opacity: 1 }, {
															duration: 1000,
															easing: 'easeOutQuint'
															}).delay(3000)
															.animate(
															{ bottom: -60, opacity: 0}, {
																	duration: 1000,
																	easing: 'easeOutQuint',
																 
															});
													
											}

								});

					

						 });

				});

		});


