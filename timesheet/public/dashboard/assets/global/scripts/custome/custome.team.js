$(document).ready(function() {

	 $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

});



$(document).ready( function(){

			$('#team_submit').on('click', function(event) {

				event.preventDefault();
				//var _token = $('#token').val();

				$.ajax({
						url: $('#team_form').attr('action'),
						type: 'POST',
						data: $('#team_form').serialize(), // Remember that you need to have your csrf token included

						success: function(data){

							//console.log(data);
							//alert(data.msg);
							
							 $('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});

							$("#team_form").trigger("reset");

							  getTeams();


						},error:function(){

								$('#error').fadeTo(200, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});
								
						}

				});
	
				});

		 });
			

				$(document).on('click', '.btn-block-team', function(event){
							
							event.preventDefault();

								var id = $(this).attr("data-value");
								$.ajax({
										type: 'post',
										url: "/admin/team/block/",
										data: {id:id},
										dataType: 'json',
									 success:function(data){
										
								$('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});

										 getTeams();
												//window.location.reload(true);

										}
								});
						});

						$(document).on('click', '.btn-enable-team', function(event){
							event.preventDefault();
							var id = $(this).attr("data-value");
								$.ajax({
										type: 'post',
										url: "/admin/team/enable/",
										data: {id:id},
										dataType: 'json',
									 success:function(data){

								$('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});

										 getTeams();

												//window.location.reload(true);
											 
										}
								});
						});
		 


		$(document).on('click', '#search', function(){

		 $("#search_team").animate({width:'200'},200); 
		 $("#search_team").focus();
		 $("#search").addClass('hidden');
		 $("#search-1").removeClass('hidden');
		 // $("#search_users").focusout(function () {

				//$("#search_users").animate({width:'30'},200);
		 // });

		});

		 $(document).on('click', '#search-1', function(){

				$("#search_team").animate({width:'30'},200);
				$("#search_team").val("");
				$("#search_team").trigger("reset");
				$("#search-1").addClass('hidden');
				$("#search").removeClass('hidden');


		});



	$(document).on('change', "#show_team",function(e){

				$value = $(this).val();
				
				 $.ajax({

								type : 'get',
								url : "/admin/team/show_team",
								data : {'show_team': $value},
								success:function(data){
									// console.log(data);
										//$('#book').html(data);

									$('.table-team-display').html(data);

								}

						});


		});

 $(document).ready(function(){

				 
				$(document).on('keyup', '#search_team',function(e){

				// On click or whatever
					 $value = $(this).val();
					 //alert($value);                           
						$.ajax({
								type : 'get',
								url : "/admin/team/search",
								data : {'search_team': $value},
								success:function(data){

									$('.table-team-display').html(data);


								}

						});

				});

	});


//Add members 
$(document).ready(function(){

			$('.btn-add-member').on('click', function(e) {

					var id = $(this).attr("data-value");
					var image = $(this).attr("data-image");
					var name = $(this).attr("data-name");
					var leader_name = $(this).attr("data-leader-name");
					var leader_image = $(this).attr("data-leader-image");
					var members_name = $(this).attr("data-member");

					$('.add_member_form').removeClass('hidden');
					$('.team_form').addClass('hidden');
					$("#team_name").html(name);
					$(".leader_name").html(leader_name);
					$('.team_id').html("<input type='hidden' value="+id+" name='team_id'>");
				$('.team_avatar').html("<img src=" +image+" style='max-width: 30px; border-radius:50px;'/>");
				$('.leader_avatar').html("<img src=" +leader_image+" style='max-width: 30px; border-radius:50px;'/>");
				$('.member_name').html(members_name);
		
		});


});


				// declare function

				getTeams();
				// declare function
		
				function getTeams(){

						$.ajax({
								type: 'get',
								url: "/admin/team/getteam",
								dataType: 'html',
								success:function(data){

										$('.table-team-display').html(data);
										
										// var monkeyList = new List('test-list', {
										//   valueNames: ['title'],
										//   page: 3,
										//   pagination: true
										// });
								}

						});
				}



			$(document).ready(function(e){

				$(document).on('click', '.add-new', function(e){
						$('.team_form_edit').addClass('hidden');
						$('.team_form').removeClass('hidden');

				});
		});

		// Edit
		$(document).ready(function(e){

				$(document).on('click', '#back' , function(){

						$('.team_form_edit').addClass('hidden');
						$('.team_form').removeClass('hidden');

				});

				$(document).on('click', '.edit', function(e){
					//alert('OK');
					var id = $(this).attr("data-value");
					var team_name = $(this).attr("data-name");
					var team_desc = $(this).attr("data-desc");
					var team_image =$(this).attr("data-team-image");
					var leader_name = $(this).attr("data-leader");
					var leader_id = $(this).attr("data-id");
					var image = $(this).attr("data-image");

						$('.team_form').addClass('hidden');
						$('.team_form_edit').removeClass('hidden');
						$('#team_name').removeClass('hidden');
						$('#team_name').html(team_name);
						// $('#user_avatar').removeClass('hidden');
						$('.text-title').html("<input type='text' class='form-control col-md-12 f12' name='title' id='title' placeholder='Title...' required autofocus value="+team_name+"> ");

						$('.text-desc').html("<textarea class='form-control col-md-12 f12' name='desc' placeholder='Description...'>"+team_desc+"</textarea>");
					
					$('#team_avatar').html("<img class='ui mini avatar image' src="+team_image+" style='max-width: 30px; border-radius:50px;'>");

					$('.leaders_id').html("<input type='hidden' name='leaders_id' id='leaders_id' class='att_name_default' value="+leader_id+"><div class='item'><img class='ui mini avatar image' src="+image+" style='max-width: 30px; border-radius:50px;'>&nbsp;<span class='f14'>"+leader_name+"</span>		<a href='#' class='btn btn-md btn-link' id='btn-rev-manage'><i class='icon-close'></i></a><hr></div>");


			$('.leaders_id').on('click', function(event) {
			
				$('.leaders_id').addClass('hidden');
				$('.selection').removeClass('hidden');
				$('.att_name').attr('name', 'leaders_id');
				$('.att_name_default').attr('name', '');		
						
			});


	$('#team_update').on('click', function(event) {

				event.preventDefault();
		
						$.ajax({
										type: 'post',
										url: "/admin/team/update/"+id,
										data: $('#team_form_edit').serialize(),
										dataType: 'json',
									 success:function(data){
												window.location.reload(true);

										},error:function(){

													$('#error').fadeTo(200, 0).animate(
															{ bottom: 30, opacity: 1 }, {
															duration: 1000,
															easing: 'easeOutQuint'
															}).delay(3000)
															.animate(
															{ bottom: -60, opacity: 0}, {
																	duration: 1000,
																	easing: 'easeOutQuint',
																 
															});
													
											}

								});

					

						 });

				});

		});


