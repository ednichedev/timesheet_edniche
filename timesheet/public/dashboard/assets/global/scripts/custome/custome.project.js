$(document).ready(function() {

		$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}}); 
});


$(document).ready( function(){

			// $('.item').click(function () {
			// 	var id = $(this).attr("data-value");
			// 	alert(id);
			// });
			$('#project_submit').on('click', function(event) {

				event.preventDefault();
			
				$.ajax({
					
						url: $('#project_form').attr('action'),
						type: 'POST',
						data: $('#project_form').serialize(), 

						success: function(data){

							//console.log(data);
							//alert(data.msg);
							
								$('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
												
										});

							$("#project_form").trigger("reset");
							$("#datetime_show").hide();
								getProject();

						},error:function(){

								$('#error').fadeTo(200, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
												
										});
								
						}

				});
	
				});

			});
			

				$(document).ready( function(){

						$(document).on('click', '.btn-block-project', function(e){

								var id = $(this).attr("data-value");

								$.ajax({
										type: 'post',
										url: "/admin/project/block/",
										data: {id:id},
										dataType: 'json',
										success:function(data){
						
												$('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
												
										});

						
											getProject();

										}
								});
						});

						$(document).on('click', '.btn-enable-project', function(e){

							var id = $(this).attr("data-value");

								$.ajax({
										type: 'post',
										url: "/admin/project/enable/",
										data: {id:id},
										dataType: 'json',
										success:function(data){

												$('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
												
										});

						

								getProject();
												
										}
								});
						});
			


					});

	$(document).ready(function(){


		
		$(document).on('click', '#search', function(){

			$("#search_project").animate({width:'200'},200); 
			$("#search_project").focus();
			$("#search").addClass('hidden');
			$("#search-1").removeClass('hidden');
			// $("#search_users").focusout(function () {

				//$("#search_users").animate({width:'30'},200);
			// });

		});

			$(document).on('click', '#search-1', function(){

			// $("#search_users").animate({width:'200'},200); 
			// $("#search_users").focus();
			
			// $("#search_users").focusout(function () {

				$("#search_project").animate({width:'30'},200);
				$("#search_project").val("");
				$("#search_project").trigger("reset");
				$("#search-1").addClass('hidden');
				$("#search").removeClass('hidden');
			// });

		});

		
	});


	$(document).on('change', '#show_project', function(e){

				$value = $(this).val();
				
					$.ajax({

								type : 'get',
								url : "/admin/project/show_project",
								data : {'show_project': $value},
								success:function(data){

									$('.table-project-display').html(data);

								}

						});


		});

	$(document).ready(function(){

				$(document).on('keyup', '#search_project', function(e){

				// On click or whatever
						$value = $(this).val();
						//alert($value);                           
						$.ajax({

								type : 'get',
								url : "/admin/project/search",
								data : {'search_project': $value},
								success:function(data){
									//console.log(data);
										//$('#book').html(data);
										
									$('.table-project-display').html(data);
										
								}

						});
				});
	});


				// declare function

				getProject();
				// declare function
		
				function getProject(){
						$.ajax({
								type: 'get',
								url: "/admin/project/getproject",
								dataType: 'html',
								success:function(data){
										$('.table-project-display').html(data);
										//console.log(data);
								}

						});
				}

// getAssign();

// function getAssign() {
	
// 						$.ajax({
// 								type: 'get',
// 								url: "/admin/project/getassign/",
// 								dataType: 'html',
// 								success:function(data){
// 										$('.table-assign-display').html(data);
									
// 								}

// 						});

// 	}

			$(document).on('click', '.assign', function () {
						var id = $(this).attr("data-value");
						var project_name = $(this).attr("data-project");
						var project_date = $(this).attr("data-date");
						var project_start_date = $(this).attr("data-start-date");
						var project_assign_count = $(this).attr("data-count-assign");
						
							$('.project_form').addClass('hidden');
							$('.add_assignt_form').addClass('hidden');
							$('.project_form_edit').addClass('hidden');
							$('.add_member_form').addClass('hidden');
							$('.remove_member_form').addClass('hidden');
							
							$('.assignments').removeClass('hidden');
							$('#project_start_date').html(project_start_date);
							$('#project_date').html(project_date);
							$('#project_name').html(project_name);
							$('#project_assign_count').html(project_assign_count);
							$('.assign_left').html(project_assign_count);
							

						$.ajax({
								type: 'get',
								url: "/admin/project/getassign/",
								data: {id:id},
								dataType: 'html',
								success:function(data){
										$('.table-assign-display').html(data);
									
								}

						});


			});

$(document).on('click', '.btn-close-assign', function () {
							$('.project_form').removeClass('hidden');
							$('.add_assignt_form').removeClass('hidden');
							$('.add_member_form').removeClass('hidden');
							$('.remove_member_form').removeClass('hidden');
							$('.assignments').addClass('hidden');

});

$(document).on('click', '.btn-close-edit', function () {
							$('.project_form').removeClass('hidden');
							$('.add_assignt_form').removeClass('hidden');
							$('.add_member_form').removeClass('hidden');
							$('.remove_member_form').removeClass('hidden');
							$('.project_form_edit').addClass('hidden');

});

// Edit
		$(document).ready(function(){


				$(document).on('click', '.project_edit', function(){

					var id = $(this).attr("data-value");
				
					var project_name = $(this).attr("data-name");
					var project_desc = $(this).attr("data-desc");
					var team_name = $(this).attr("data-team");
					var team_id = $(this).attr("data-id");
					var image = $(this).attr("data-image");
					var start_date = $(this).attr("data-start-date");
					var data_duedate = $(this).attr("data-duedate");


						$('.project_form').addClass('hidden');
						$('.project_form_edit').removeClass('hidden');
						// $('.add_assignt_form').addClass('hidden');
						// $('.add_member_form').addClass('hidden');
						// $('.remove_member_form').addClass('hidden');
						// $('.assignments').addClass('hidden');
					

						$('.project_name').html(project_name);
						// $('#user_avatar').removeClass('hidden');

						$( "input[name~='title']" ).val(project_name);

						$('.text-desc').html("<textarea class='form-control col-md-12 f12' name='desc' placeholder='Description...'>"+project_desc+"</textarea>");

						$('.teams_id').html("<input type='hidden' name='teams_id' id='teams_id' class='att_name_default' value="+team_id+"><div class='item'><img class='ui mini avatar image' src="+image+" style='max-width: 30px; border-radius:50px;'>&nbsp;<span class='f14'>"+team_name+"</span>		<a href='#' class='btn btn-md btn-link' id='btn-rev-project'><i class='icon-close'></i></a><hr></div>");

						$('#example3').val(start_date);
						$('#example4').val(data_duedate);

								$('.teams_id').on('click', function() {
			
								$('.teams_id').addClass('hidden');
								$('.selection').removeClass('hidden');
								$('.att_name').attr('name', 'teams_id');
								$('.att_name_default').attr('name', '');
								});

						$('#project_update').on('click', function(event) {

							event.preventDefault();
					
									$.ajax({
													type: 'post',
													url: "/admin/project/update/"+id,
													data: $('#project_form_edit').serialize(),
													dataType: 'json',
													success:function(data){
															window.location.reload(true);

													},error:function(){

																$('#error').fadeTo(200, 0).animate(
																		{ bottom: 30, opacity: 1 }, {
																		duration: 1000,
																		easing: 'easeOutQuint'
																		}).delay(3000)
																		.animate(
																		{ bottom: -60, opacity: 0}, {
																				duration: 1000,
																				easing: 'easeOutQuint',
																				
																		});
																
														}

											});

					

							});

					
				});

		});


