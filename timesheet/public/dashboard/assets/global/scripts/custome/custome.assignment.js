$(document).ready(function() {

	 $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}}); 
});


$(document).ready( function(){

			$('#assign_submit').on('click', function(event) {

				event.preventDefault();

				$.ajax({
					
						url: $('#submit_assignt_form').attr('action'),
						type: 'POST',
						data: $('#submit_assignt_form').serialize(), 

						success: function(data){

							
							 $('#success').fadeTo(500, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});

							$("#submit_assignt_form").trigger("reset");
							$("#datetime_show").hide();

								getAssignment();

						},error:function(){

								$('#error').fadeTo(200, 0).animate(
										{ bottom: 30, opacity: 1 }, {
										duration: 1000,
										easing: 'easeOutQuint'
										}).delay(3000)
										.animate(
										{ bottom: -60, opacity: 0}, {
												duration: 1000,
												easing: 'easeOutQuint',
											 
										});
								
						}

				});
	
				});

		 });
			

				

 $(document).ready(function(){


		
		$(document).on('click', '#search', function(){

		 $("#search_assignmentsingle").animate({width:'200'},200); 
		 $("#search_assignmentsingle").focus();
		 $("#search").addClass('hidden');
		 $("#search-1").removeClass('hidden');
		 // $("#search_users").focusout(function () {

				//$("#search_users").animate({width:'30'},200);
		 // });

		});

		 $(document).on('click', '#search-1', function(){

		 // $("#search_users").animate({width:'200'},200); 
		 // $("#search_users").focus();
		 
		 // $("#search_users").focusout(function () {

				$("#search_assignmentsingle").animate({width:'30'},200);
				$("#search_assignmentsingle").val("");
				$("#search_assignmentsingle").trigger("reset");
				$("#search-1").addClass('hidden');
				$("#search").removeClass('hidden');
		 // });

		});

		
 });


	$(document).on('change', '#show_assignmentsingle', function(e){

				$value = $(this).val();
				
				 $.ajax({

								type : 'get',
								url : "/admin/assignment/show_assignmentsingle",
								data : {'show_assignmentsingle': $value},
								success:function(data){

									$('.table-assignmentsingle-display').html(data);

								}

						});


		});

 $(document).ready(function(){

				$(document).on('keyup', '#search_assignmentsingle', function(e){

				// On click or whatever
					 $value = $(this).val();
					 //alert($value);                           
						$.ajax({

								type : 'get',
								url : "/admin/assignment/search",
								data : {'search_assignmentsingle': $value},
								success:function(data){
									//console.log(data);
										//$('#book').html(data);
									 
									$('.table-assignmentsingle-display').html(data);
									 
								}

						});
				});
	});


				// declare function

				getAssignment();
				// declare function
		
				function getAssignment(){
						$.ajax({
								type: 'get',
								url: "/admin/assignment/getassignment",
								dataType: 'html',
								success:function(data){
										$('.table-assignmentsingle-display').html(data);
										//console.log(data);
								}

						});
				}

// getAssign();

// function getAssign() {
	
// 						$.ajax({
// 								type: 'get',
// 								url: "/admin/project/getassign/",
// 								dataType: 'html',
// 								success:function(data){
// 										$('.table-assign-display').html(data);
									
// 								}

// 						});

// 	}

			$(document).on('click', '.assign', function () {
						var id = $(this).attr("data-value");
						var project_name = $(this).attr("data-project");
						var project_date = $(this).attr("data-date");
						var project_start_date = $(this).attr("data-start-date");
						var project_assign_count = $(this).attr("data-count-assign");
						
							$('.project_form').addClass('hidden');
							$('.add_assignt_form').addClass('hidden');
							$('.project_form_edit').addClass('hidden');
							$('.add_member_form').addClass('hidden');
							$('.remove_member_form').addClass('hidden');
							
							$('.assignments').removeClass('hidden');
							$('#project_start_date').html(project_start_date);
							$('#project_date').html(project_date);
							$('#project_name').html(project_name);
							$('#project_assign_count').html(project_assign_count);
							$('.assign_left').html(project_assign_count);
							

						$.ajax({
								type: 'get',
								url: "/admin/project/getassign/",
								data: {id:id},
								dataType: 'html',
								success:function(data){
										$('.table-assign-display').html(data);
									
								}

						});


			});

$(document).on('click', '.btn-close-assign', function () {
							$('.project_form').removeClass('hidden');
							$('.add_assignt_form').removeClass('hidden');
							$('.add_member_form').removeClass('hidden');
							$('.remove_member_form').removeClass('hidden');
							$('.assignments').addClass('hidden');

});

$(document).on('click', '.btn-close-edit', function () {
							$('.project_form').removeClass('hidden');
							$('.add_assignt_form').removeClass('hidden');
							$('.add_member_form').removeClass('hidden');
							$('.remove_member_form').removeClass('hidden');
							$('.project_form_edit').addClass('hidden');

});

// Edit
		$(document).ready(function(){


				$(document).on('click', '.project_edit', function(){

					var id = $(this).attr("data-value");
					var project_name = $(this).attr("data-name");
					var project_desc = $(this).attr("data-desc");
					var team_name = $(this).attr("data-team");
					var team_id = $(this).attr("data-id");
					var image = $(this).attr("data-image");
					var start_date = $(this).attr("data-start-date");
					var data_duedate = $(this).attr("data-duedate");

						$('.project_form').addClass('hidden');

						$('.add_assignt_form').addClass('hidden');
						$('.add_member_form').addClass('hidden');
						$('.remove_member_form').addClass('hidden');
						$('.assignments').addClass('hidden');
						$('.project_form_edit').removeClass('hidden');

						$('.project_name').html(project_name);
						// $('#user_avatar').removeClass('hidden');
						$('.text-title').html("<input type='text' class='form-control col-md-12 f12' name='title' id='title' placeholder='Title...' required autofocus value="+project_name+"> ");

						$('.text-desc').html("<textarea class='form-control col-md-12 f12' name='desc' placeholder='Description...'>"+project_desc+"</textarea>");

						$('.datline').html("<input type='date' class='form-control f12' name='start_date' placeholder='Start Date...' id='start_date' value="+start_date+"><span class='input-group-addon'> To </span><input type='date' class='form-control f12' name='duedate' placeholder='Duedate...' id='duedate' value="+data_duedate+">");

					$('.teams_id').html("<input type='hidden' name='teams_id' id='teams_id' class='att_name_default' value="+team_id+"><div class='item'><img class='ui mini avatar image' src="+image+" style='max-width: 30px; border-radius:50px;'>&nbsp;<span class='f14'>"+team_name+"</span>		<a href='#' class='btn btn-md btn-link' id='btn-rev-project'><i class='icon-close'></i></a><hr></div>");

								$('.teams_id').on('click', function() {
			
								$('.teams_id').addClass('hidden');
								$('.selection').removeClass('hidden');
								$('.att_name').attr('name', 'teams_id');
								$('.att_name_default').attr('name', '');
								});

						$('#project_update').on('click', function(event) {

							event.preventDefault();
					
									$.ajax({
													type: 'post',
													url: "/admin/project/update/"+id,
													data: $('#project_form_edit').serialize(),
													dataType: 'json',
												 success:function(data){
															window.location.reload(true);

													},error:function(){

																$('#error').fadeTo(200, 0).animate(
																		{ bottom: 30, opacity: 1 }, {
																		duration: 1000,
																		easing: 'easeOutQuint'
																		}).delay(3000)
																		.animate(
																		{ bottom: -60, opacity: 0}, {
																				duration: 1000,
																				easing: 'easeOutQuint',
																			 
																		});
																
														}

											});

					

						 });

					
				});

		});


