<script>

	$(document).ready(function() {

		<?php 

		$now = new DateTime();
		$current = $now->format('Y-m-d');

		?>

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay,listWeek,listDay'
			},

			views: {
				listDay: { buttonText: 'list day' },
				listWeek: { buttonText: 'list week' }
			},

			defaultDate: '<?php echo $current; ?>',
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
			<?php foreach ($data as $datas) {?>
				{
					title: '<?php echo ' | '.$datas->title ?>',
					start: '<?php echo $datas->start_date ?>',
					end: '<?php echo $datas->duedate ?>',
					<?php if(count($datas)==1){?>
					color: '#2ecc71',
					<?php }else{?>
					color: '#1abc9c',
					<?php } ?>		
				},
				<?php }?>
				],
				eventColor: '#378006'

		});
		
	});

</script>