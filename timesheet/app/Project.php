<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $table = 'projects';

     //protected $date = ['deleted_at'];

    protected $fillable = [
        'title',  
        'slug',
        'desc', 
        'teams_id',
        'start_date',
        'duedate',
        'completed',
        'is_active', 
    	];

    public function teams()
    {
    	return $this->belongsTo('App\Team', 'teams_id');
    }


    public function user()
    {
    	return $this->belongsToMany('App\User', 'project_users', 'projects_id', 'users_id')->withTimestamps();
    }

    public function assignmentprojects()
    {
    	return $this->hasMany('App\Assignmentproject', 'projects_id');
    }



    public function getRouteKeyName()
    {
    return 'slug';
    }


}
