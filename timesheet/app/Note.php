<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table ='notes';

    protected $fillable = [
        'note',  
        'task_id',
        'create_by',
    	];

    public function tasksingles()
    {
    	return $this->belongsTo('App\Tasksingle', 'task_id');
    }

    public function creatBys()
    {
        return $this->belongsTo('App\User', 'create_by');
    }
}
