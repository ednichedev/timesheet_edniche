<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteProject extends Model
{
    protected $table ='note_projects';

    protected $fillable = [
        'note',  
        'task_id',
        'create_by',
    	];

    public function taskprojects()
    {
    	return $this->belongsTo('App\TaskProject', 'task_id');
    }

    public function creatBys()
    {
        return $this->belongsTo('App\User', 'create_by');
    }


}
