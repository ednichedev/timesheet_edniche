<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignmentproject extends Model
{
    protected $table = 'assignmentprojects';

     //protected $date = ['deleted_at'];

    protected $fillable = [
        'title',  
        'slug',
        'desc', 
        'projects_id',        
        'start_date',
        'duedate',
        'completed',
        'users_id',
        'create_by',
        'is_active', 
    	];

    public function protectes()
    {
    	return $this->belongsTo('App\Project', 'projects_id');
    }

    public function tasks()
    {
        return $this->hasMany('App\TaskProject', 'assignment_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'users_id');
    }

    
    public function creatBys()
    {
        return $this->belongsTo('App\User', 'create_by');
    }

    public function getRouteKeyName()
    {
    return 'slug';
    }
}
