<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';

     //protected $date = ['deleted_at'];

    protected $fillable = [
        'title',  
        'desc', 
        'managers_id',
        'is_active', 
    	];

    public function managers()
    {
    	return $this->belongsTo('App\User', 'managers_id');
    }

    public function teams()
    {
        return $this-> hasMany('App\Team', 'departments_id');
    }

}
