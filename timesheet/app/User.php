<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

   

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'add_members','avatar', 'is_active', 'is_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function departments()
    {
        return $this-> hasMany('App\Department', 'managers_id');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team', 'team_users', 'teams_id', 'member_id')->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project', 'project_users', 'projects_id', 'users_id')->withTimestamps();
    }

    public function assignmentprojects()
    {
        return $this-> hasManyThrough('App\Assignmentproject', 'App\Project', 'users_id', 'create_by');
    }

    public function assignmentsingles()
    {
        return $this-> hasMany('App\Assignmentsngle', 'users_id', 'create_by');
    }

    public function noteprojects()
    {
        return $this-> hasMany('App\NoteProject', 'create_by');
    }

    public function noteassigs()
    {
        return $this-> hasMany('App\Note', 'create_by');
    }



}
