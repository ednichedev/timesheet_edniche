<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignmentsingle extends Model
{
    protected $table = 'assignmentsingles';

     //protected $date = ['deleted_at'];

    protected $fillable = [
        'title',  
        'slug',
        'desc',       
        'start_date',
        'duedate',
        'completed',
        'users_id',
        'create_by',
    	];

    public function user()
    {
    	return $this->belongsTo('App\User', 'users_id');
    }

    public function tasks()
    {
        return $this->hasMany('App\Tasksingle', 'assignment_id');
    }

    public function creatBys()
    {
        return $this->belongsTo('App\User', 'create_by');
    }


    public function getRouteKeyName()
    {
    return 'slug';
    }

}
