<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class AuthLoginController extends Controller
{

	public function login(Request $request)
	{
		//dd($request->all());

		if (Auth::attempt([
			'email' => $request->email,
			'password' => $request->password
			])) {
			
			$user = User::where('email', $request->email)->first();

				if ($user->role == 1) {
					
					return redirect()->route('admin.dashboard');

				}elseif ($user->role == 2) {

					return redirect()->route('team.dashboard');

				}elseif ($user->role == 3) {
					return redirect()->route('employee.dashboard');
				}

						

		}

		return redirect()->back();



	}
    
}
