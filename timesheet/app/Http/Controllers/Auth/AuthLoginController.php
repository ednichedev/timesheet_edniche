<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthLoginController extends Controller
{


	public function login(Request $request)
	{
		//dd($request->all());

		if (Auth::attempt([
			'email' => $request->email,
			'password' => $request->password
			])) {

			$user = User::where('email', $request->email)->first();

			if ($user->role == 1) {
					
					return redirect()->route('admin.dashboard');

				}elseif ($user->role == 2) {

					return redirect()->route('admin.dashboard');

				}elseif ($user->role == 3) {

					if($user->is_login == 0){

					return view('dashboard-teamleader.change-password');

					}elseif($user->is_login == 1){

						return redirect()->route('team.dashboard');

					}


				
				}elseif ($user->role == 4) {


					if($user->is_login == 0){

					return view('dashboard-employee.change-password');

					}elseif($user->is_login == 1){

						return redirect()->route('employee.dashboard');

					}
					
					
				}
		}

		return redirect()->back();



	}


	public function change_password(Request $request)
	{
		  
			$is_login = 1;
			$user = User::find($request->user_id);
			$user->password = bcrypt($request->new_password);
			$user->is_login = $is_login;
			$user->update();

			if($user->role == 3){

				return redirect()->route('team.dashboard');	

			}elseif($user->role == 4){

				return redirect()->route('employee.dashboard');
			}
			
				

		}

	
}
