<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Department;
class DepartmentController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$users = User::where('role', 1)->get();
		return view('dashboard-admin.department.index')->withUsers($users);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if($request->ajax())
		  {

			$this->validate($request, [
				'title' => 'required',
				'desc' => 'required',
				'managers_id' => 'required',

			]);
				
			$departments = New Department;
			$departments->title = $request->title;
			$departments->desc = $request->desc;
			$departments->managers_id = $request->managers_id;
			if ($departments->save()) {
			   
				return response(['msg'=>'Successfuly']);
			}

		 }
	}

public function getdepartment()
{
	 $departments = Department::with('managers')->orderby('created_at', 'desc')->paginate(5);
		  
		return view('dashboard-admin.department.table', compact('departments', 'managers'));
}

	public function search(Request $request){

		 if($request->ajax())
		  {

			// $user = Request::get('keyword');
			$output="";

			$departments = Department::with('managers')->where('title', 'LIKE', '%'.$request->search_depart.'%')->orWhere('id', 'LIKE', '%'.$request->search_depart.'%')->orderby('created_at', 'desc')->paginate(5);

			if ($departments) {

			  $output = view('dashboard-admin.department.table', compact('departments', 'managers'));
			}

			return Response($output); 
			//return view('dashboard-admin.users.table', compact('users'));
		   }
	}


public function show_depart(Request $request){

		 if($request->ajax())
		  {

			// $user = Request::get('keyword');
			$show="";

			$show_depart = $request->show_depart;

		   if ($show_depart == 5) {

			  $departments = Department::with('managers')->orderby('created_at', 'desc')->paginate(5);
		   }elseif ($show_depart == 10) {
			   $departments = Department::with('managers')->orderby('created_at', 'desc')->paginate(10);
		   }elseif ($show_depart == 30) {
			   $departments = Department::with('managers')->orderby('created_at', 'desc')->paginate(30);
		   }elseif ($show_depart == 100) {
			   $departments = Department::with('managers')->orderby('created_at', 'desc')->get();
		   }

			if ($departments) {

			   $show = view('dashboard-admin.department.table', compact('departments', 'managers'));
			}

			return Response($show); 
			//return view('dashboard-admin.users.table', compact('users'));
		   }
	}



public function block(Request $request)
	{
		
		if ($request->ajax()) {
			
			$is_active =1;

			$blocks = Department::find($request->id);

			$blocks->update([
				'is_active'=>$is_active,
				]);

			if ($blocks) {
			   
			   return response(['msg'=>'Successfuly']);
			}

		}
	}

public function enable(Request $request)
	{
		
		if ($request->ajax()) {
			
			$is_active =0;

			$blocks = Department::find($request->id);

			$blocks->update([
				'is_active'=>$is_active,
				]);

			if ($blocks) {
			   
			   return response(['msg'=>'Successfuly']);
			}

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		if ($request->ajax()) {
			
			$departments = Department::find($request->id);

			$departments->title = $request->title;
			$departments->desc = $request->desc;
			$departments->managers_id = $request->managers_id;

			$departments->update();
			
			if ($departments) {
			   
			   return response(['msg'=>'Successfuly']);
			}

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
