<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Project;
use Auth;
use App\Team;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $auth_id = Auth::id();

        $teams = Team::with('leaders', 'departments', 'members', 'projects')->where('leaders_id', $auth_id)->where('is_active', 0)->orderby('created_at', 'desc')->get();

      

            $data = Project::with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->get();
       

        return view('dashboard-teamleader.index', compact('data', 'teams'));

        
    }

    
    public function Profile($id)
    {
        $users = User::find($id);
        return view('dashboard-teamleader.profile.index');
    }

    public function ProfilePost(Request $request, $id)
    {
        
        $this->validate($request, [
            'avatars' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);


        $imageName = time().'.'.$request->avatars->getClientOriginalExtension();
        $request->avatars->move(public_path('/dashboard/assets/img/avatars'), $imageName);

        $user = User::find($id);

        $user->update(['avatar'=>$imageName]);

        return redirect()->back();



    }

    public function ChangePassword(Request $request, $id)
    {
            $this->validate($request, [
            'password' => 'required|min:6|confirmed',
            ]);

            $user_pass = User::find($id);

            $user_pass->password  = bcrypt($request->password);

            $user_pass->update();
           
            return redirect()->back();
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
