<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\User;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Project::get(['title', 'start_date', 'duedate']);
        return view('dashboard-admin.index', compact('data'));
    }

    
    public function projectCalender()
    {

        $data = Project::get(['title', 'start_date', 'duedate']);
        
        return Response()->json($data);

    }


    public function Profile($id)
    {
        $users = User::find($id);
        return view('dashboard-admin.profile.index');
    }

    public function ProfilePost(Request $request, $id)
    {
        
        $this->validate($request, [
            'avatars' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);


        $imageName = time().'.'.$request->avatars->getClientOriginalExtension();
        $request->avatars->move(public_path('/dashboard/assets/img/avatars'), $imageName);

        $user = User::find($id);

        $user->update(['avatar'=>$imageName]);

        return redirect()->back();



    }

    public function ChangePassword(Request $request, $id)
    {
            $this->validate($request, [
            'password' => 'required|min:6|confirmed',
            ]);

            $user_pass = User::find($id);

            $user_pass->password  = bcrypt($request->password);

            $user_pass->update();
           
            return redirect()->back();
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
