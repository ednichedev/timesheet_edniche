<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Team;
use App\User;
use App\Project;
use App\Assignmentproject;
use Auth;
use App\TaskProject;
use App\NoteProject;
class AdminProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_active', 1)->orderby('created_at', 'desc')->get();

        $teams = Team::where('is_active', 0 )->orderby('created_at', 'desc')->get();
         return view('dashboard-admin.project.index', compact('users', 'teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->ajax())
          {

            $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'teams_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date|after:start_date',

            ]);

            $teams_id = $request->teams_id;

            $teams = Team::with('leaders')->where('id', $teams_id)->orderby('created_at', 'desc')->get();

            foreach ($teams as $team) {

                $leader_id = $team->leaders->id;

            }
            

            

            $projects = New Project;
            $projects->title = $request->title;
            $projects->slug = strtolower(str_slug($request->title, '-'));
            $projects->desc = $request->desc;
            $projects->teams_id = $request->teams_id;
            //$projects->leadprojects_id = $leader_id;
            $projects->start_date = $request->start_date;
            $projects->duedate = $request->duedate;
            //$projects->save();

            if ($projects->save()) {
               
                return response(['msg'=>'Successfuly']);
            }

         }
    }

    public function getProject()
    {
        $projects = Project::with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->get();
              
        return view('dashboard-admin.project.table', compact('projects', 'teams', 'user', 'assignmentprojects'));

    }

    public function projectComplete(Request $request, $id)
    {

        if ($request->ajax()) {

            $projectComplete = Project::find($id);
            $projectComplete->completed = $request->completed;

            $projectComplete->update();

            if ($projectComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }

        
    }


    public function projectunComplete(Request $request, $id)
    {

        if ($request->ajax()) {

            $projectunComplete = Project::find($id);
            $projectunComplete->completed = $request->uncompleted;

            $projectunComplete->update();

            if ($projectunComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }

        
    }

    public function search(Request $request){

         if($request->ajax())
          {

            // $user = Request::get('keyword');
            $output="";

            $projects = Project::with('teams', 'leadprojects', 'user', 'assignmentprojects')->where('title', 'LIKE', '%'.$request->search_project.'%')->orWhere('id', 'LIKE', '%'.$request->search_project.'%')->orderby('created_at', 'desc')->paginate(5);

            if ($projects) {

              $output = view('dashboard-admin.project.table', compact('projects', 'teams', 'leadprojects', 'user', 'assignmentprojects'));
            }

            return Response($output); 
            //return view('dashboard-admin.users.table', compact('users'));
           }
    }


    public function addmember($id)
    {
            //$team_id = $request->get('data-team-id');

            //dd($team_id);

            $projects = Project::where('id', $id)->with('teams', 'user')->orderby('created_at', 'desc')->get();

            foreach ($projects as $project) {
               
                   $team_id = $project->teams->id;

            }

           $teams = Team::where('id', $team_id)->with('members', 'leaders')->orderby('created_at', 'desc')->get();

           
            // $members_p = User::where('role', 3)->where('add_members', 1)->orderby('created_at', 'desc')->get();  
            
        
            return view('dashboard-admin.project.add-members', compact('projects', 'leaders', 'members', 'teams', 'user'));
    }

    public function addmemberPost(Request $request)
    {
        $members_id = $request->members;

        foreach ($members_id as $m_id) {
           //$quantity = 1;
           $membercounts = User::where('id', $m_id)->orderby('created_at', 'desc')->get();

           // foreach ($membercounts as $membercount) {
           //  //$quantity = $bookcount->quantity;
           //  //foreach ($quantity as $qty) {
           //      //dd($bookcount->quantity);
           //  //}
           //  $members = User::where('id', $m_id)->update(['add_members'=>$add_members]);
           //  }

        }

           $projects = Project::find($request->project_id);

           $projects->user()->sync($request->members, false);


        
        // if($projects == null){

        //    return redirect()->back()->with('message', 'This User already in this project ');
            
        // }
        
        return redirect()->back();      


    }


    public function removemember($id)
    {
        
         $projects = Project::where('id', $id)->with('teams', 'user')->orderby('created_at', 'desc')->get();

         foreach ($projects as $project) {
               
                   $team_id = $project->teams->id;

            }

        $teams = Team::where('id', $team_id)->with('members', 'leaders')->orderby('created_at', 'desc')->get();


        return view('dashboard-admin.project.remove-members', compact('projects', 'leaders', 'members', 'teams', 'user'));
    }


    public function removememberPost(Request $request)
    {

            $members_id = $request->members;

            $project_id = $request->project_id;
            $projects = Project::find($project_id);

            $projects->update();

            $projects->user()->detach($request->members);

            return redirect()->back();    
    }


    public function addAssignment($id)
    {
        $projects = Project::where('id', $id)->with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->get();

            foreach ($projects as $project) {
               
                   $team_id = $project->teams->id;

            }

        $teams = Team::where('id', $team_id)->with('members', 'leaders')->orderby('created_at', 'desc')->get();

            return view('dashboard-admin.project.add-assignment', compact('projects', 'leaders', 'members', 'teams', 'user'));
    }


    public function addAssignmentPost(Request $request)
    {
        
    
         $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'project_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date|after:start_date',
                'duedate' => 'required|date|before:project_duedate',

            ]);


           $project_id = $request->project_id;

           $assignments = new Assignmentproject;
           $assignments->title = $request->title;
           $assignments->slug = strtolower(str_slug($request->title, '-'));
           $assignments->projects_id = $project_id;
           $assignments->desc = $request->desc;
           $assignments->start_date = $request->start_date;
           $assignments->duedate = $request->duedate;
           $assignments->users_id = $request->user_id;

           $assignments->create_by = $request->auth_id;

           $assignments->save();
            
           return redirect()->back()->withAssignments($assignments);
    }

    public function addTaskAssignmentPost(Request $request)
    {
        
    
         $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'assignment_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date|after:start_date',
                // 'duedate' => 'required|date|before:assignmentpro_duedate',

            ]);


           $assignment_id = $request->assignment_id;

           $taskproject = new TaskProject;
           $taskproject->title = $request->title;
           $taskproject->slug = strtolower(str_slug($request->title, '-'));
           $taskproject->assignment_id = $assignment_id;
           $taskproject->desc = $request->desc;
           $taskproject->start_date = $request->start_date;
           $taskproject->start_time = $request->start_time;
           $taskproject->duedate = $request->duedate;
           $taskproject->end_time = $request->end_time;
           $taskproject->users_id = $request->user_id;

           $taskproject->create_by = $request->create_by;

           $taskproject->save();
            
           return redirect()->back();
    }




    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            
            $projects = Project::find($request->id);

            $teams_id = $request->teams_id;

            $teams = Team::with('leaders')->where('id', $teams_id)->orderby('created_at', 'desc')->get();

            foreach ($teams as $team) {

                $leader_id = $team->leaders->id;

            }
            

            $projects->title = $request->title;
            $projects->slug = strtolower(str_slug($request->title, '-'));
            $projects->desc = $request->desc;
            $projects->teams_id = $request->teams_id;
            // $projects->leadprojects_id = $leader_id;
            $projects->start_date = $request->start_date;
            $projects->duedate = $request->duedate;

  
            if ($projects->update()) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAssignment($id)
    {

        $projects = Project::with('teams', 'user', 'assignmentprojects')->where('id', $id)->orderby('created_at', 'desc')->get();

        $projects_id = Project::find($id);


        foreach ($projects as $project) {
          
            $countCompleted = Assignmentproject::where('projects_id', $project->id)->where('completed', 1)->get();

            $countleft = Assignmentproject::where('projects_id', $project->id)->where('completed', 0)->get();    
          
        }


        return view('dashboard-admin.project.assignment-show', compact('assignmentprojects', 'projects', 'user', 'countCompleted', 'countleft'));
        
    }


    public function getTask($id)
    {

        $assignprojects = Assignmentproject::with('tasks', 'user')->where('id', $id)->orderby('created_at', 'desc')->get();

        foreach ($assignprojects as $assignproject) {
          
            $countCompleted = TaskProject::where('assignment_id', $assignproject->id)->where('completed', 1)->get();

            $countleft = TaskProject::where('assignment_id', $assignproject->id)->where('completed', 0)->get();    
          
        }

        return view('dashboard-admin.project.taskproject-show', compact('assignprojects', 'tasks', 'user', 'countCompleted', 'countleft')); 
    }


    public function getassign(Request $request)
    {
        if ($request->ajax()) {

        $show="";

        $projects = Project::with('teams', 'user', 'assignmentprojects')->where('id', $request->id)->orderby('created_at', 'desc')->get();

        if ($projects) {

            $show = view('dashboard-admin.project.assign-table', compact('projects', 'teams', 'user', 'assignmentprojects'));
            }

        return Response($show); 
              
        // return view('dashboard-admin.project.assignments.table', compact('projects', 'teams', 'leadprojects', 'user', 'assignmentprojects'));

        }
    }

    public function removeassignPost(Request $request)
    {
      
            

            $assign_id = $request->assign;
            $assignments = Assignmentproject::find($assign_id);
            $assignments->delete();

            return redirect()->back();

    }

    public function block(Request $request)
    {
        
        if ($request->ajax()) {
            
            $is_active =1;

            $blocks = Project::find($request->id);

            $blocks->update([
                'is_active'=>$is_active,
                ]);

            if ($blocks) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

    public function enable(Request $request)
    {
        
        if ($request->ajax()) {
            
            $is_active =0;

            $enables = Project::find($request->id);

            $enables->update([
                'is_active'=>$is_active,
                ]);

            if ($enables) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }


    public function show_project(Request $request){

         if($request->ajax())
          {

            // $user = Request::get('keyword');
            $show="";

            $show_project = $request->show_project;

           if ($show_project == 5) {

              $projects = Project::with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->paginate(5);
           }elseif ($show_project == 10) {
               $projects = Project::with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->paginate(10);
           }elseif ($show_project == 30) {
               $projects = Project::with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->paginate(30);
           }elseif ($show_project == 100) {
               $projects = Project::with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->get();
           }

            if ($projects) {

               $show = view('dashboard-admin.project.table', compact('projects', 'teams', 'user', 'assignmentprojects'));
            }

            return Response($show); 
            
           }
    }


    public function assignComplete(Request $request, $id)
    {
        if ($request->ajax()) {

            $assignComplete = Assignmentproject::find($request->id);
            $assignComplete->completed = $request->completed;

            $assignComplete->completed_by = $request->completed_by;

            $assignComplete->update();

            if ($assignComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function assignunComplete(Request $request, $id)
    {
        $null = null;

        if ($request->ajax()) {

            $assignunComplete = Assignmentproject::find($request->id);
            $assignunComplete->completed = $request->uncompleted;

            $assignunComplete->completed_by = $request->$null;

            $assignunComplete->update();

            if ($assignunComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }



    public function taskComplete(Request $request, $id)
    {
        if ($request->ajax()) {

            $taskComplete = TaskProject::find($request->id);
            $taskComplete->completed = $request->completed;

            $taskComplete->completed_by = $request->completed_by;

            $taskComplete->update();

            if ($taskComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function taskunComplete(Request $request, $id)
    {
        $null = null;

        if ($request->ajax()) {

            $taskComplete = TaskProject::find($request->id);
            $taskComplete->completed = $request->uncompleted;

            $taskComplete->completed_by = $request->$null;

            $taskComplete->update();

            if ($taskComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function addNote(Request $request)
    {
        $this->validate($request, [
                'note' => 'required',
                'task_id' => 'required',

            ]);

        $notes = new NoteProject;
        
        $notes->create([
            'note'=>$request->note,
            'task_id'=>$request->task_id,
            'create_by'=>$request->create_by,
        ]);

        return redirect()->back();

    }




}
