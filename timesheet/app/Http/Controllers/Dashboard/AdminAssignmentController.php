<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\User;
use App\Assignmentsingle;
use App\Team;
use App\Tasksingle;
use App\Note;
class AdminAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_active', 1)->orderby('created_at', 'desc')->get();

        
         return view('dashboard-admin.assignment.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){

            $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'users_id' => 'required',
                'start_date' => 'required|date',
                // 'duedate' => 'required|date|after:start_date',
            ]);

           $assignmentsSignle = new Assignmentsingle;

           $assignmentsSignle->title = $request->title;
           $assignmentsSignle->slug = strtolower(str_slug($request->title, '-'));
           $assignmentsSignle->desc = $request->desc;
           $assignmentsSignle->start_date = $request->start_date;

            // $assignmentsSignle->start_time = $request->start_time;

           $assignmentsSignle->duedate = $request->duedate;

        // $assignmentsSignle->end_time = $request->end_time;

           $assignmentsSignle->users_id = $request->users_id;
           
           $assignmentsSignle->create_by = $request->create_by;


           if ($assignmentsSignle->save()) {
               
               return response(['msg'=>'Successfuly']);
            }


        }
    }


    public function getassignment()
    {
        $assignmentsingles = Assignmentsingle::with('user')->orderby('created_at', 'desc')->get();
              
        return view('dashboard-admin.assignment.assignmentsingle-table', compact('assignmentsingles', 'user'));

    }


    public function remove(Request $request)
    {
      

            $assign_id = $request->assignmentsingle;
            $assignmentsingles = Assignmentsingle::find($assign_id);
            $assignmentsingles->delete();

            return redirect()->back();

    }


    public function search(Request $request){

         if($request->ajax())
          {

            // $user = Request::get('keyword');
            $output="";

            $assignmentsingles = Assignmentsingle::with('user')->where('title', 'LIKE', '%'.$request->search_assignmentsingle.'%')->orWhere('id', 'LIKE', '%'.$request->search_assignmentsingle.'%')->orderby('created_at', 'desc')->get();

            if ($assignmentsingles) {

              $output = view('dashboard-admin.assignment.assignmentsingle-table', compact('assignmentsingles', 'user'));
            }

            return Response($output); 
            //return view('dashboard-admin.users.table', compact('users'));
           }
    }


    public function show_assignmentsingle(Request $request){

         if($request->ajax())
          {

            // $user = Request::get('keyword');
            $show="";

            $show_assignmentsingle = $request->show_assignmentsingle;

           if ($show_assignmentsingle == 5) {

              $assignmentsingles = Assignmentsingle::with('user')->orderby('created_at', 'desc')->paginate(5);
           }elseif ($show_assignmentsingle == 10) {
               $assignmentsingles = Assignmentsingle::with('user')->orderby('created_at', 'desc')->paginate(10);
           }elseif ($show_assignmentsingle == 30) {
               $assignmentsingles = Assignmentsingle::with('user')->orderby('created_at', 'desc')->paginate(30);
           }elseif ($show_assignmentsingle == 100) {
               $assignmentsingles = Assignmentsingle::with('user')->orderby('created_at', 'desc')->get();
           }

            if ($assignmentsingles) {

               $show = view('dashboard-admin.assignment.assignmentsingle-table', compact('assignmentsingles', 'user'));
            }

            return Response($show); 
            
           }
    }


    public function getTask($id)
    {

        $assignprojects = Assignmentsingle::with('tasks', 'user')->where('id', $id)->orderby('created_at', 'desc')->get();


        foreach ($assignprojects as $assignproject) {
          
            $countCompleted = Tasksingle::where('assignment_id', $assignproject->id)->where('completed', 1)->get();

            $countleft = Tasksingle::where('assignment_id', $assignproject->id)->where('completed', 0)->get();    
          
        }

        return view('dashboard-admin.assignment.taskproject-show', compact('assignprojects', 'tasks', 'user', 'countCompleted', 'countleft')); 
    }


    public function addTaskAssignmentPost(Request $request)
    {
        
    
         $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'assignment_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date',
                'duedate' => 'required|date|before_or_equal:assignmentpro_duedate',

            ]);


           $assignment_id = $request->assignment_id;

           $tasksingle = new Tasksingle;
           $tasksingle->title = $request->title;
           $tasksingle->slug = strtolower(str_slug($request->title, '-'));
           $tasksingle->assignment_id = $assignment_id;
           $tasksingle->desc = $request->desc;
           $tasksingle->start_date = $request->start_date;
           $tasksingle->start_time = $request->start_time;
           $tasksingle->duedate = $request->duedate;
           $tasksingle->end_time = $request->end_time;
           $tasksingle->users_id = $request->user_id;

           $tasksingle->create_by = $request->create_by;

           $tasksingle->save();
            
           return redirect()->back();
    }



    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


   public function assignComplete(Request $request, $id)
    {
        if ($request->ajax()) {

            $assignComplete = Assignmentsingle::find($request->id);
            $assignComplete->completed = $request->completed;

            $assignComplete->update();

            if ($assignComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function assignunComplete(Request $request, $id)
    {
       

        if ($request->ajax()) {

            $assignunComplete = Assignmentsingle::find($request->id);
            $assignunComplete->completed = $request->uncompleted;


            $assignunComplete->update();

            if ($assignunComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function taskComplete(Request $request, $id)
    {

      if ($request->ajax()) {

       $tasksingle = Tasksingle::find($id);
       $tasksingle->completed = $request->completed;
       $tasksingle->update();

       if ($tasksingle) {
               
               return response(['msg'=>'Successfuly']);
            }

        }

    }

    public function taskunComplete(Request $request, $id)
    {
      if ($request->ajax()) {

       $tasksingle = Tasksingle::find($id);
       $tasksingle->completed = $request->uncompleted;
       $tasksingle->update();

        if ($tasksingle) {
               
               return response(['msg'=>'Successfuly']);
            }

        }

    }

    public function addNote(Request $request)
    {
        $this->validate($request, [
                'note' => 'required',
                'task_id' => 'required',

            ]);

        $notes = new Note;

        $notes->create([
            'note'=>$request->note,
            'task_id'=>$request->task_id,
            'create_by'=>$request->create_by,
        ]);

        return redirect()->back();

    }


}
