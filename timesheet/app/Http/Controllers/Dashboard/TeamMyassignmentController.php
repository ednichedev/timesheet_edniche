<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\User;
use App\Project;
use App\Assignmentproject;
use App\Assignmentsingle;
use App\Tasksingle;
use App\Note;
use Auth;
class TeamMyassignmentController extends Controller
{

		public function index()
		{
				$auth_id = Auth::id();

				$assignmentsingles = Assignmentsingle::with('user', 'creatBys', 'tasks')->where('users_id', $auth_id)->orderby('created_at', 'desc')->paginate(10);

				$countCompleted = Assignmentsingle::where('users_id', $auth_id)->where('completed', 1)->get();

				$countleft = Assignmentsingle::where('users_id', $auth_id)->where('completed', 0)->get();


				$teams = Team::with('leaders', 'departments', 'members', 'projects')->where('leaders_id', $auth_id)->where('is_active', 0)->orderby('created_at', 'desc')->get();

				return view('dashboard-teamleader.assignmentsingle.index', compact('assignmentsingles', 'user', 'creatBys', 'countCompleted', 'countleft', 'projects', 'assignmentprojects', 'teams', 'tasks'));
		}

	
		public function create()
		{
				//
		}

	 
		public function store(Request $request)
		{
				$this->validate($request, [
								'title' => 'required',
								'desc' => 'required',
								'start_date' => 'required|date',
								'duedate' => 'required|date|after:start_date',
						]);

					 $assignments = new Assignmentsingle;
					 $assignments->title = $request->title;
					 $assignments->slug = strtolower(str_slug($request->title, '-'));
					 
					 $assignments->desc = $request->desc;
					 $assignments->start_date = $request->start_date;
					 $assignments->duedate = $request->duedate;
					 $assignments->users_id = $request->auth_id;
					 $assignments->create_by = $request->auth_id;

					 $assignments->save();
						
					 return redirect()->back();
		}

		public function deleteassign(Request $request)
		{
						$assign_id = $request->assign_id;
						$assignments = Assignmentsingle::find($assign_id);
						$assignments->delete();

						return redirect()->back();

		}


		public function addTaskAssignmentPost(Request $request)
		{
				
		
				 $this->validate($request, [
								'title' => 'required',
								'desc' => 'required',
								'assignment_id' => 'required',
								'start_date' => 'required|date',
								'duedate' => 'required|date',
								'duedate' => 'required|date|before_or_equal:assignmentpro_duedate',

						]);


					 $assignment_id = $request->assignment_id;

					 $tasksingle = new Tasksingle;
					 $tasksingle->title = $request->title;
					 $tasksingle->slug = strtolower(str_slug($request->title, '-'));
					 $tasksingle->assignment_id = $assignment_id;
					 $tasksingle->desc = $request->desc;
					 $tasksingle->start_date = $request->start_date;
					 $tasksingle->start_time = $request->start_time;
					 $tasksingle->duedate = $request->duedate;
					 $tasksingle->end_time = $request->end_time;
					 $tasksingle->users_id = $request->user_id;

					 $tasksingle->create_by = $request->create_by;

					 $tasksingle->save();
						
					 return redirect()->back()->withAssignments($tasksingle);
		}

	 
		public function show($id)
		{
				//
		}

	 
		public function edit($id)
		{
				//
		}

	 
		public function update(Request $request, $id)
		{
				//
		}

		
		public function destroy($id)
		{
				//
		}


		 public function assignComplete(Request $request, $id)
		{
				if ($request->ajax()) {

						$assignComplete = Assignmentsingle::find($request->id);
						$assignComplete->completed = $request->completed;

						$assignComplete->update();

						if ($assignComplete) {
							 
							 return response(['msg'=>'Successfuly']);
						}

				}
				
		}


		public function assignunComplete(Request $request, $id)
		{
			 

				if ($request->ajax()) {

						$assignunComplete = Assignmentsingle::find($request->id);
						$assignunComplete->completed = $request->uncompleted;


						$assignunComplete->update();

						if ($assignunComplete) {
							 
							 return response(['msg'=>'Successfuly']);
						}

				}
				
		}

		public function getTask($id)
		{

				$assignmentsingles = Assignmentsingle::with('tasks', 'user')->where('id', $id)->orderby('created_at', 'desc')->get();


				foreach ($assignmentsingles as $assignmentsingle) {
					
						$countCompleted = Tasksingle::where('assignment_id', $assignmentsingle->id)->where('completed', 1)->get();

						$countleft = Tasksingle::where('assignment_id', $assignmentsingle->id)->where('completed', 0)->get();    
					
				}

				return view('dashboard-teamleader.assignmentsingle.taskproject-show', compact('assignmentsingles', 'tasks', 'user', 'countCompleted', 'countleft')); 
		}


		public function taskComplete(Request $request, $id)
	{
		if ($request->ajax()) {

			$taskComplete = Tasksingle::find($id);
			$taskComplete->completed = $request->completed;

			$taskComplete->completed_by = $request->completed_by;

			$taskComplete->update();

			if ($taskComplete) {
			   
			   return response(['msg'=>'Successfuly']);
			}

		}
		
	}


	public function tasknunComplete(Request $request, $id)
	{
		$null = null;

		if ($request->ajax()) {

			$taskunComplete = Tasksingle::find($id);
			$taskunComplete->completed = $request->uncompleted;

			$taskunComplete->completed_by = $request->$null;

			$taskunComplete->update();

			if ($taskunComplete) {
			   
			   return response(['msg'=>'Successfuly']);
			}

		}
		
	}



	 public function addNote(Request $request)
    {
        $this->validate($request, [
                'note' => 'required',
                'task_id' => 'required',

            ]);

        $notes = new Note;
        
        $notes->create([
            'note'=>$request->note,
            'task_id'=>$request->task_id,
            'create_by'=>$request->create_by,
        ]);

        return redirect()->back();

    }



}
