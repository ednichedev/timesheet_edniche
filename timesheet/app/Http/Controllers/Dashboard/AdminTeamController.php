<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Department;
use App\Team;
class AdminTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role', 3 )->where('is_active', 1)->orderby('created_at', 'desc')->get();

        $members = User::with('teams')->where('role', 4 )->orWhere('role', 2)->where('is_active', 1)->orderby('created_at', 'desc')->get();
        
        $teams = Team::with('members')->orderby('created_at', 'desc')->paginate(5);

        $departments = Department::where('is_active', 0)->orderby('created_at', 'desc')->get();

        return view('dashboard-admin.team.index', compact('departments', 'users', 'members', 'teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
          {

            $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'departments_id' => 'required',
                'leaders_id' => 'required',

            ]);
                
            $teams = New Team;
            $teams->title = $request->title;
            $teams->desc = $request->desc;
            $teams->departments_id = $request->departments_id;
            $teams->leaders_id = $request->leaders_id;

            if ($teams->save()) {
                
                return response(['msg'=>'Successfuly']);
            }

         }
    }

    public function getTeams()
    {
         $teams = Team::with('leaders', 'departments', 'members')->orderby('created_at', 'desc')->paginate(5);
              
        return view('dashboard-admin.team.table', compact('teams', 'leaders', 'departments', 'members'));

    }


    public function search(Request $request){

         if($request->ajax())
          {

            // $user = Request::get('keyword');
            $output="";

            $teams = Team::with('leaders', 'departments', 'members')->where('title', 'LIKE', '%'.$request->search_team.'%')->orWhere('id', 'LIKE', '%'.$request->search_team.'%')->orderby('created_at', 'desc')->paginate(5);

            if ($teams) {

              $output = view('dashboard-admin.team.table', compact('teams', 'leaders', 'departments', 'members'));
            }

            return Response($output); 
            //return view('dashboard-admin.users.table', compact('users'));
           }
    }

public function show_team(Request $request){

		 if($request->ajax())
		  {

			// $user = Request::get('keyword');
			$show="";

			$show_team = $request->show_team;

		   if ($show_team == 5) {

			  $teams = Team::with('leaders', 'departments', 'members')->orderby('created_at', 'desc')->paginate(5);
		   }elseif ($show_team == 10) {
			   $teams = Team::with('leaders', 'departments', 'members')->orderby('created_at', 'desc')->paginate(10);
		   }elseif ($show_team == 30) {
			   $teams = Team::with('leaders', 'departments', 'members')->orderby('created_at', 'desc')->paginate(30);
		   }elseif ($show_team == 100) {
			   $teams = Team::with('leaders', 'departments', 'members')->orderby('created_at', 'desc')->get();
		   }

			if ($teams) {

			   $show = view('dashboard-admin.team.table', compact('teams', 'leaders', 'departments', 'members'));
			}

			return Response($show); 
			
		   }
	}





    public function addmember($id)
    {
            // $team_id = Team::findOrFail($id);
            
            $teams = Team::where('id', $id)->with('members', 'leaders')->orderby('created_at', 'desc')->get();

            $members_t = User::where('role', 4)->where('add_members', 0)->orderby('created_at', 'desc')->get();  
             
              

            return view('dashboard-admin.team.add-members', compact('leaders', 'members', 'teams', 'members_t'));
    }

    public function addmemberPost(Request $request)
    {
        $members_id = $request->members;

        $add_members = 1;

        foreach ($members_id as $m_id) {
           
           $membercounts = User::where('id', $m_id)->orderby('created_at', 'desc')->get();

           foreach ($membercounts as $membercount) {
   
            $members = User::where('id', $m_id)->update(['add_members'=>$add_members]);
            }

        }

           $teams = Team::find($request->team_id);

           $teams->members()->sync($request->members, false);

            return redirect()->back();    


    }




    public function removemember($id)
    {
         $teams = Team::where('id', $id)->with('members', 'leaders')->orderby('created_at', 'desc')->get();

        return view('dashboard-admin.team.remove-members', compact('members', 'teams', 'leaders'));
    }

    public function removememberPost(Request $request)
    {

            $members_id = $request->members;

            $add_members = 0;

            
                
            $members = User::where('id', $members_id)->update(['add_members'=>$add_members]);
     


            $team_id = $request->team_id;
            $teams = Team::find($team_id);

            $teams->update();

            $teams->members()->detach($request->members);

            return redirect()->back();    
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->ajax()) {
            
            $teams = Team::find($request->id);

            $teams->title = $request->title;
            $teams->desc = $request->desc;
            $teams->leaders_id = $request->leaders_id;

            $teams->update();
            
            if ($teams) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function block(Request $request)
    {
        
        if ($request->ajax()) {
            
            $is_active =1;

            $blocks = Team::find($request->id);

            $blocks->update([
                'is_active'=>$is_active,
                ]);

            if ($blocks) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

public function enable(Request $request)
    {
        
        if ($request->ajax()) {
            
            $is_active =0;

            $enables = Team::find($request->id);

            $enables->update([
                'is_active'=>$is_active,
                ]);

            if ($enables) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

}
