<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Response;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      
         
    $users = User::where('is_active', 1)->orderby('created_at', 'desc')->get();
            
    return view('dashboard-admin.users.index', compact('users'))->render();
      

            
         
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    

      if($request->ajax())
      {

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'role' => 'required',

        ]);
        
        $is_login = 1;
        
        $users = New User;
        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = bcrypt($request->password);
        $users->role = $request->role;

        if ($request->role == 1) {

           $users->is_login = $is_login;  
            
        }
        //$users->save();
        //return redirect()->back();
        
            if ($users->save()) {
               
                return response(['msg'=>'Successfuly']);
            }

         }


    }

    public function search(Request $request){

         if($request->ajax())
          {

            // $user = Request::get('keyword');
            $output="";

            $users = User::where('name', 'LIKE', '%'.$request->search_users.'%')->orWhere('email', 'LIKE', '%'.$request->search_users.'%')->orWhere('id', 'LIKE', '%'.$request->search_users.'%')->orderby('created_at', 'desc')->paginate(5);

            if ($users) {

               $output = view('dashboard-admin.users.table', compact('users'));
            }

            return Response($output); 
            //return view('dashboard-admin.users.table', compact('users'));
           }
    }



    public function show_user(Request $request){

         if($request->ajax())
          {

            // $user = Request::get('keyword');
            $show="";

            $show_user = $request->show_user;

           if ($show_user == 5) {

              $users = User::orderBy('created_at', 'desc')->paginate(5);
           }elseif ($show_user == 10) {
               $users = User::orderBy('created_at', 'desc')->paginate(10);
           }elseif ($show_user == 30) {
               $users = User::orderBy('created_at', 'desc')->paginate(30);
           }elseif ($show_user == 100) {
               $users = User::orderBy('created_at', 'desc')->get();
           }

            if ($users) {

               $show = view('dashboard-admin.users.table', compact('users'));
            }

            return Response($show); 
            //return view('dashboard-admin.users.table', compact('users'));
           }
    }




    public function getUser()
    {

         $users = User::orderBy('created_at', 'desc')->get();
          
        return view('dashboard-admin.users.table', compact('users'));
    }

    public function block(Request $request)
    {
        
        if ($request->ajax()) {
            
            $is_active =0;

            $blocks = User::find($request->id);

            $blocks->update([
                'is_active'=>$is_active,
                ]);

            if ($blocks) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

    public function enable(Request $request)
    {
        
        if ($request->ajax()) {
            
            $is_active =1;

            $blocks = User::find($request->id);

            $blocks->update([
                'is_active'=>$is_active,
                ]);
            
            if ($blocks) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

    public function resetpass(Request $request)
    {
            
        if ($request->ajax()) {

            $this->validate($request, [
            'password' => 'required|min:6|confirmed',
            ]);

            
            $resetpass = User::find($request->id);

            $resetpass->password = bcrypt($request->password);

            $resetpass->update();
            
            if ($resetpass) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }

    public function account(Request $request)
    {
        
        if ($request->ajax()) {

            $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',

            ]);

            $accounts = User::find($request->id);

            $accounts->name = $request->name;
            $accounts->email = $request->email;
            $accounts->role = $request->role;

            $accounts->update();
            
            if ($accounts) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
    }
 
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('dashboard-admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',

            ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->update();

        return redirect()->back();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
