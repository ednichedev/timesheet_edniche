<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\Team;
use App\User;
use App\Assignmentproject;
use Carbon\Carbon;
class AdminReportController extends Controller
{

    public function index()
    {

        $now = Carbon::now()->format('Y-m-d');

        $addweek = Carbon::now()->addWeek()->format('Y-m-d');


        $projects = Project::with('teams', 'user', 'assignmentprojects')->where('duedate', $now)->get();

        return view('dashboard-admin.report.index', compact('projects', 'teams', 'user', 'assignmentprojects'));
    }


    public function projectReport(Request $request)
    {
       

        $start_date = Carbon::parse($request->start_date)->format('Y-m-d');

        $duedate = Carbon::parse($request->duedate)->format('Y-m-d');

        $completed = $request->complete;



        $projects = Project::with('teams', 'user', 'assignmentprojects')->where('completed', $completed)->whereBetween('start_date', [$start_date, $duedate])->get();

      

        return view('dashboard-admin.report.index', compact('projects', 'teams', 'user', 'assignmentprojects'));
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
