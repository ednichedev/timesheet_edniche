<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\User;
use App\Project;
use App\Assignmentproject;
use App\Assignmentsingle;
use App\TaskProject;
use App\NoteProject;
use Auth;

class EmployeeProjectController extends Controller
{
  
    public function index()
    {
        
        $projects = Project::with('user')->orderby('created_at', 'desc')->paginate(5);

      
       return view('dashboard-employee.project.project', compact('user', 'projects'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'project_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date|after:start_date',
                'duedate' => 'required|date|before_or_equal:project_duedate',

            ]);

           $project_id = $request->project_id;

           $assignments = new Assignmentproject;
           $assignments->title = $request->title;
           $assignments->slug = strtolower(str_slug($request->title, '-'));
           $assignments->projects_id = $project_id;
           $assignments->desc = $request->desc;
           $assignments->start_date = $request->start_date;
           $assignments->duedate = $request->duedate;
           $assignments->users_id = $request->auth_id;

           $assignments->create_by = $request->auth_id;

           $assignments->save();
            
           return redirect()->back();
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function getAssignment($id)
    {

        $auth_id = Auth::id();

        $projects = Project::where('id', $id)->get();

        $projects_id = Project::find($id);



        foreach ($projects as $project) {
          
           $assignmentprojects = Assignmentproject::where('projects_id', $project->id)->where('users_id', '=',$auth_id)->get();

            $countCompleted = Assignmentproject::where('projects_id', $project->id)->where('completed', 1)->get();

            $countleft = Assignmentproject::where('projects_id', $project->id)->where('completed', 0)->get();    
          
        }


        return view('dashboard-employee.project.assignment-show', compact('assignmentprojects', 'projects', 'user', 'countCompleted', 'countleft'));
        
    }


    public function deleteassign(Request $request)
    {
            $assign_id = $request->assign_id;
            $assignments = Assignmentproject::find($assign_id);
            $assignments->delete();

            return redirect()->back();

    }

     public function addTaskAssignmentPost(Request $request)
    {
        
    
         $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'assignment_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date',
                'duedate' => 'required|date|before_or_equal:assignmentpro_duedate',

            ]);


           $assignment_id = $request->assignment_id;

           $taskproject = new TaskProject;
           $taskproject->title = $request->title;
           $taskproject->slug = strtolower(str_slug($request->title, '-'));
           $taskproject->assignment_id = $assignment_id;
           $taskproject->desc = $request->desc;
           $taskproject->start_date = $request->start_date;
           $taskproject->start_time = $request->start_time;
           $taskproject->duedate = $request->duedate;
           $taskproject->end_time = $request->end_time;
           $taskproject->users_id = $request->user_id;

           $taskproject->create_by = $request->create_by;

           $taskproject->save();
            
           return redirect()->back()->withAssignments($taskproject);
    }


    public function getTask($id)
    {
        $user_id = Auth::id();

        $assignprojects = Assignmentproject::with('tasks', 'user')->where('id', $id)->orderby('created_at', 'desc')->get();

        foreach ($assignprojects as $assignproject) {
          
            $countCompleted = TaskProject::where('assignment_id', $assignproject->id)->where('users_id', $user_id)->where('completed', 1)->get();

            $countleft = TaskProject::where('assignment_id', $assignproject->id)->where('users_id', $user_id)->where('completed', 0)->get();    
          
        }

        return view('dashboard-employee.project.taskproject-show', compact('assignprojects', 'tasks', 'user', 'countCompleted', 'countleft')); 
    }


    public function taskComplete(Request $request, $id)
    {
        if ($request->ajax()) {

            $taskComplete = TaskProject::find($id);
            $taskComplete->completed = $request->completed;

            $taskComplete->completed_by = $request->completed_by;

            $taskComplete->update();

            if ($taskComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function tasknunComplete(Request $request, $id)
    {
        $null = null;

        if ($request->ajax()) {

            $taskunComplete = TaskProject::find($id);
            $taskunComplete->completed = $request->uncompleted;

            $taskunComplete->completed_by = $request->$null;

            $taskunComplete->update();

            if ($taskunComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }

    public function addNote(Request $request)
    {
        $this->validate($request, [
                'note' => 'required',
                'task_id' => 'required',

            ]);

        $notes = new NoteProject;
        
        $notes->create([
            'note'=>$request->note,
            'task_id'=>$request->task_id,
            'create_by'=>$request->create_by,
        ]);

        return redirect()->back();

    }
    
}
