<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Team;
use App\User;
use App\Project;
use App\Assignmentproject;
use App\Assignmentsingle;
use Auth;
use App\TaskProject;
use App\NoteProject;
class TeamTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $auth_id = Auth::id();

       $teams = Team::with('leaders', 'departments', 'members', 'projects')->where('leaders_id', $auth_id)->orderby('created_at', 'desc')->get();

      

       return view('dashboard-teamleader.team.team', compact('teams', 'leaders', 'departments', 'members', 'projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'project_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date',
                'duedate' => 'required|date|before_or_equal:project_duedate',

            ]);

           $project_id = $request->project_id;

           $assignments = new Assignmentproject;
           $assignments->title = $request->title;
           $assignments->slug = strtolower(str_slug($request->title, '-'));
           $assignments->projects_id = $project_id;
           $assignments->desc = $request->desc;
           $assignments->start_date = $request->start_date;
           $assignments->duedate = $request->duedate;
           $assignments->users_id = $request->user_id;

           $assignments->create_by = $request->auth_id;

           $assignments->save();
            
           return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function team()
    {
        $auth_id = Auth::id();

       $teams = Team::with('leaders', 'departments', 'members', 'projects')->where('leaders_id', $auth_id)->where('is_active', 0)->orderby('created_at', 'desc')->get();

       return view('dashboard-teamleader.team.team', compact('teams', 'leaders', 'departments', 'members', 'projects'));
    }

    public function project()
    {
        $auth_id = Auth::id();

        $teams = Team::with('leaders', 'departments', 'members', 'projects')->where('leaders_id', $auth_id)->where('is_active', 0)->orderby('created_at', 'desc')->get();

    
        $projects = Project::with('teams', 'user', 'assignmentprojects')->orderby('created_at', 'desc')->paginate(10);
       
       
       return view('dashboard-teamleader.project.project', compact('teams', 'leaders', 'departments', 'members', 'projects', 'user', 'assignmentprojects'));
    }


    public function getAssignment($id)
    {

        $projects = Project::with('teams', 'user', 'assignmentprojects')->where('id', $id)->get();

        $projects_id = Project::find($id);


        foreach ($projects as $project) {
          
            $countCompleted = Assignmentproject::where('projects_id', $project->id)->where('completed', 1)->get();

            $countleft = Assignmentproject::where('projects_id', $project->id)->where('completed', 0)->get();    
          
        }


        return view('dashboard-teamleader.project.assignment-show', compact('assignmentprojects', 'projects', 'user', 'countCompleted', 'countleft'));
        
    }


    public function addTaskAssignmentPost(Request $request)
    {
        
    
         $this->validate($request, [
                'title' => 'required',
                'desc' => 'required',
                'assignment_id' => 'required',
                'start_date' => 'required|date',
                'duedate' => 'required|date',
                'duedate' => 'required|date|before_or_equal:assignmentpro_duedate',

            ]);


           $assignment_id = $request->assignment_id;

           $taskproject = new TaskProject;
           $taskproject->title = $request->title;
           $taskproject->slug = strtolower(str_slug($request->title, '-'));
           $taskproject->assignment_id = $assignment_id;
           $taskproject->desc = $request->desc;
           $taskproject->start_date = $request->start_date;
           $taskproject->start_time = $request->start_time;
           $taskproject->duedate = $request->duedate;
           $taskproject->end_time = $request->end_time;
           $taskproject->users_id = $request->user_id;

           $taskproject->create_by = $request->create_by;

           $taskproject->save();
            
           return redirect()->back()->withAssignments($taskproject);
    }


    public function deleteassign(Request $request)
    {
            $assign_id = $request->assign_id;
            $assignments = Assignmentproject::find($assign_id);
            $assignments->delete();

            return redirect()->back();

    }

    public function assignComplete(Request $request, $id)
    {
        if ($request->ajax()) {

            $assignComplete = Assignmentproject::find($request->id);
            $assignComplete->completed = $request->completed;

            $assignComplete->completed_by = $request->completed_by;

            $assignComplete->update();

            if ($assignComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function assignunComplete(Request $request, $id)
    {
        $null = null;

        if ($request->ajax()) {

            $assignunComplete = Assignmentproject::find($request->id);
            $assignunComplete->completed = $request->uncompleted;

            $assignunComplete->completed_by = $request->$null;

            $assignunComplete->update();

            if ($assignunComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function getTask($id)
    {

        $assignprojects = Assignmentproject::with('tasks', 'user')->where('id', $id)->orderby('created_at', 'desc')->get();

        foreach ($assignprojects as $assignproject) {
          
            $countCompleted = TaskProject::where('assignment_id', $assignproject->id)->where('completed', 1)->get();

            $countleft = TaskProject::where('assignment_id', $assignproject->id)->where('completed', 0)->get();    
          
        }

        return view('dashboard-teamleader.project.taskproject-show', compact('assignprojects', 'tasks', 'user', 'countCompleted', 'countleft')); 
    }


    public function taskComplete(Request $request, $id)
    {
        if ($request->ajax()) {

            $taskComplete = TaskProject::find($id);
            $taskComplete->completed = $request->completed;

            $taskComplete->completed_by = $request->completed_by;

            $taskComplete->update();

            if ($taskComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function tasknunComplete(Request $request, $id)
    {
        $null = null;

        if ($request->ajax()) {

            $assignunComplete = TaskProject::find($request->id);
            $assignunComplete->completed = $request->uncompleted;

            $assignunComplete->completed_by = $request->$null;

            $assignunComplete->update();

            if ($assignunComplete) {
               
               return response(['msg'=>'Successfuly']);
            }

        }
        
    }


    public function addNote(Request $request)
    {
        $this->validate($request, [
                'note' => 'required',
                'task_id' => 'required',

            ]);

        $notes = new NoteProject;
        
        $notes->create([
            'note'=>$request->note,
            'task_id'=>$request->task_id,
            'create_by'=>$request->create_by,
        ]);

        return redirect()->back();

    }



}
