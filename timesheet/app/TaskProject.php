<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskProject extends Model
{
    protected $table = 'task_projects';

     //protected $date = ['deleted_at'];

    protected $fillable = [
        'title',  
        'slug',
        'desc', 
        'assignment_id',        
        'start_date',
        'start_time',
        'duedate',
        'end_time',
        'completed',
        'users_id',
        'create_by',
        'is_active', 
    	];

   	// public function protectes()
    // {
    // 	return $this->belongsTo('App\Project', 'projects_id');
    // }

    public function assignmentproject()
    {
    	return $this->belongsTo('App\Assignmentproject', 'assignment_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'users_id');
    }

    
    public function creatBys()
    {
        return $this->belongsTo('App\User', 'create_by');
    }


    public function noteProjects()
    {
        return $this->hasMany('App\NoteProject', 'task_id');
    }

    public function getRouteKeyName()
    {
    return 'slug';
    }

}
