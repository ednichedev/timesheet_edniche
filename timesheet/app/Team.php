<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    

    protected $table = 'teams';

     //protected $date = ['deleted_at'];

    protected $fillable = [
        'title',  
        'desc', 
        'departments_id',
        'leaders_id',
        'avatar',
        'is_active', 
    	];

    public function departments()
    {
    	return $this->belongsTo('App\Department', 'departments_id');
    }

    public function leaders()
    {
        return $this-> belongsTo('App\User', 'leaders_id');
    }

    public function members()
    {
    	return $this->belongsToMany('App\User', 'team_users', 'teams_id', 'member_id')->withTimestamps();
    }

    public function projects()
    {
        return $this-> hasMany('App\Project', 'teams_id');
    }

}
